
# Roadmap for v1.1

See the [draft roadmap](https://pad.castalia.camp/p/roadmap-v2-uxk0nsc) and the [minutes of the vote](https://pad.castalia.camp/p/meetings-2022-05-31-1w30p0y).

## Handbook

* Update to v1.1 (for late october/beginning of november, before OS Experience 8-9 Nov)
  - Update references and resources, check links for 404s.
  - Add relationships between activities? (would help suggest new activities when completing one / when starting)
  - Keep up to date and improve the Resource Centre
* Update to v2.0
  - Improve method first chapters
  - Add specific advice for contexts (administrations, not-for-profit, etc.)
  - New activities, if we get some feedback suggesting so? No hard requirement here, but new feedback could change that.


## Translations

* Set up a dedicated page or system (Translation Management System) to help with translations.
* Set the path for translations, with no hard commitment for this iteration:
  - Portuguese with Gerardo Lisboa.
  - French?
  - Spanish?
  - German?


## GGI Deployment

We need to provide a way for people to discover and experiment with the GGI approach.

Two approaches have been discussed:

* A set of scripts, working on a GitLab / GitHub project, that:
  - creates the activities,
  - publishes a static website with the updated current status (based on activities),
* A QMS-like system that provides a better user experience and more advanced features:
  - provide a way to define where you are, then propose actions, then record the output/proofs.

As voted during our last meeting of May, this iteration will focus on the first option: Issue-based deployment.

## Use cases / Communication

We need to gather market feedbacks thru customer uses cases and community of interest
* Establish and agree on action plan to gather customer use cases (industry, organisations of different sizes)
* Extend community reach & awareness thru communication plan
  - Events: France: OSXP, OW2con, …
  - Organisations: Numeum, Pole Systematic, CRIP, TOSIT, CIGREF
  - Communication drum beat
    - content: roadmap, feature poll, uses cases
    - channel: linkedin, twitter, …
  - OW2con
    - Justin Colaninno Keynote
    - Roundtable ()
    - OSPO agenda topics but ot GGI specifics
    - 15th anniversary Roundtable (TBC)
  - OSXP
    - CFP
    - OpenCIO Summit


## Infra / Support

* We will need to regularly update the resource centre if we are to use it and communicate on it.
* There are a bunch of interesting resources in the handbook + the resource centre, it would be nice to have a script that extracts links and resources from these two sources (handbook + resource centre) and publish them somewhere. (on the ospo.zone website?)

