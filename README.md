# Good Governance Initiative

> The GGI handbook has been moved to <https://gitlab.ow2.org/ggi/ggi>, please update your bookmarks!

The Good Governance Initiative proposes a methodological framework to assess and improve open-source awareness, compliance and governance within organisations.

Organisations -- may they be small or big companies, city councils, or associations -- maximise the benefits derived from open-source when people, processes and technology are aligned and share the same vision.

This initiative helps organisations to achieve these goals with:
* An easy-to-understand **assessment program** to identify pain points and evaluate the organisation's current practices.
* A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm.
* **Guidance** and a breadth of **curated references** about open-source licencing, practices, training, and ecosystems, to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership.
* A **certifying process** to help you select the right partners and show your commitment to the world.


## Content

In the course of this program you will learn:
* How to **properly and safely use open source software** within the company to improve re-use, maintainability and velocity of development teams.
* About the **legal and technical risks** associated with external code and collaboration, and **how to mitigate them**.
* About the required **training for teams**, from developers to team leaders and managers, so everybody shares the same, unified vision.
* How to **build up your own strategy**, define goals and setup the required processes within the organisation, with real-world use cases and examples, from top experts of the domain.
* How to **properly communicate** within the company and to the external world so as to make the most of your strategy.
* How to **improve competitiveness and attractiveness** for your organisation through open-source usage and evangelism.


## Activities

The framework uses **Activities** to identify practices relevant to OSS Good Governance.

Please go to [the Level Board](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/432), or to the [Roles Board](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/433).


## Assessment

Each activity has an "Assessment" section, with one or more questions attached to it. These allow you to know if your practices regarding the activity's topic is in a "not compliant", "working", or "compliant" status.

When one starts their journey through the GGI they will have all activities marked as "not compliant". Answering assessment questions will mark all acquired or in-progress practices as so, and will establish a landscape of actions to complete (for the in-progress activities) or to work on (for the not-compliant activities).

From there, you can start by securing in-progress activities achievements or tackle new challenges by selecting a not-compliant activity and addressing its activities.

## Translations

The GGI handbook is translated using [Weblate](https://hosted.weblate.org/), who offers free hosting for open source projects. We appreciate the commitment to the open source community. 
