---
title: The Good Governance initiative
layout: default
---

### The Good Governance Initiative (GGI) proposes a methodological framework to assess and improve open-source awareness, compliance and governance within organisations.

The GGI is an integral part of the [OSPO Alliance initiative](https://ospo.zone).

This place is where contributors collaborate on the content and resources provided by the GGI. Everything we do happens in the open, on the [GitLab page of the GGI](https://gitlab.ow2.org/ggi/ggi). Our [meetings are public](contributing/#discussions-and-meetings), and you are welcome to join.

**For the latest official version of the GGI Handbook, please see the [OSPO.Zone website](https://ospo.zone/ggi).**

<a href="https://gitlab.ow2.org/ggi/ggi"><img src="https://gitlab.ow2.org/uploads/-/system/project/avatar/1353/G2i_sigle_gitlab.png?width=256" /></a>


The major goals of the initiative are the following:
* A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm.
* **Guidance** and a breadth of **curated references** about open-source licencing, practices, training, and ecosystems, to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership.
* An easy-to-understand **assessment program** to identify pain points and evaluate the organisation's current practices.
* A **certifying process** to help you select the right partners and show your commitment to the world.


## Content

In the course of this program you will learn:
* How to **properly and safely use open source software** within the company to improve re-use, maintainability and velocity of development teams.
* About the **legal and technical risks** associated with external code and collaboration, and **how to mitigate them**.
* About the required **training for teams**, from developers to team leaders and managers, so everybody shares the same, unified vision.
* How to **build up your own strategy**, define goals and setup the required processes within the organisation, with real-world use cases and examples, from top experts of the domain.
* How to **properly communicate** within the company and to the external world so as to make the most of your strategy.
* How to **improve competitiveness and attractiveness** for your organisation through open-source usage and evangelism.


## Activities

The framework [uses **activities** to identify practices relevant to OSS Good Governance](https://ospo.zone/ggi/). Activities are organised into **goals** and provide a roadmap and guidance to define and develop an OSPO.

There is an online visualisation of the activities developed for the GGI Handbook v1.0, see [the Level Board](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/449)
