---
title: GGI - Contributing
layout: default
---


# Contributing to the GGI

The Good Governance Initiative is a sub-working group of the [OSPO Alliance](https://ospo.zone/) and is hosted on the [OW2 forge](https://gitlab.ow2.org/ggi).  

We are open to contributions, and all our activity is open and public. No registration, no fees are required, and write access to the repositories is granted according to merit and participation.
Everything developed here is licensed under the [CC-BY licence 4.0](https://creativecommons.org/licenses/by/4.0/).

## Discussions and meetings

To join the conversation, the best way is to **introduce yourself on our mailing list**: <https://mail.ow2.org/wws/info/ossgovernance>.  
This is where public discussions take place - you can register freely at [this link](https://mail.ow2.org/wws/subscribe/ossgovernance).

We hold **regular meetings** open to everyone, to synchronise and report on our progress.
- They usually take place on Tuesday at 5pm CEST on [Jitsi](https://jitsi.hivane.net/OW2OSSGoodGovernance). No registration is required, just connect and join the discussion.
- Minutes are publicly available in this [Pad folder](https://pad.castalia.camp/mypads/?/mypads/group/ggi-minutes-zz1b0nmd/view).
- Previous meeting minutes are stored in the [OW2 Nextcloud instance](https://nextcloud.ow2.org).

## Collaboration tools

* Current work takes place on OW2 GitLab [GGI project](https://gitlab.ow2.org/ggi/ggi).
* **Current tasks** are tracked as issues in [ggi/ggi](https://gitlab.ow2.org/ggi/ggi/-/issues/).
* We use [Pads](https://pad.castalia.camp/mypads/?/mypads/group/ggi-ri40n2d/view) for collaborative work.
* Public weekly meetings.
* Weekly calls take place on this Jitsi room: https://jitsi.hivane.net/OW2OSSGoodGovernance

## Contributing to the GGI Handbook

The handbook is stored on [Git](https://gitlab.ow2.org/ggi/ggi), as [Markdown](https://about.gitlab.com/handbook/markdown-guide/) text.  

### Using Markdown
Using Markdown makes it simple to integrate basic formatting while letting us focus on contents. It also means we can easily profit from the possibilities and automation services provided by GitLab because everyhing is stored as lightweight text files.

You can read more about Markdown here: https://about.gitlab.com/handbook/markdown-guide/  
But mostly, you can start by editing existing files and will quickly get the basics !

### Using GitLab
Using Git and GitLab is a great way to collaborate transparently, making peer review, commenting, automatic PDF and Website creation possible.

Our workflow:
* The `main` [branch](https://gitlab.ow2.org/ggi/ggi/-/tree/main) contains the current text, as progressively reviewed and validated by peers.
* Updates and preparation of the next version happen in the `dev` [branch](https://gitlab.ow2.org/ggi/ggi/-/tree/dev) of the repository. These changes are regularly integrated into the `main` branch
* Previous stable releases of the Handbook can be found in [tags](https://gitlab.ow2.org/ggi/ggi/-/tags) (like snapshots)
* Any user can also create their own branch to work separatly before merging their work in the `dev` branch.
* You are also welcome to fork and submit merge requests into the repository. All contributions are discussed during our weekly meetings or on the mailing list.

### Build process

The build process for the Handbook is documented in the dedicated directory: [/handbook/README.md](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/README.md).

## Contributing to the ospo.zone website

The OSPO.Zone website is hosted on the [Eclipse Foundation's GitLab](https://gitlab.eclipse.org/eclipse/plato/www) and is operated as a standard open source repository: anybody can fork it and submit a pull request. There is a pool of diverse maintainers who will check and merge the proposed changes as they happen. If in doubt, please submit an issue on the project or ask on one of the mailing lists (GGI, ospo.zone).

## Contributing to the Resource Center

The [GGI resource centre](https://www.ow2.org/view/OSS_Governance/) contains a lot of resources and information from the beginning of the initiative.  
To contribute, please follow the instruction below in this page.

## Registering on the OW2 GitLab instance

You do not have to register on the GitLab instance to join the conversation and participate, however contributions need to be reviewed and as such require to work on forks or in specific branches, which implies to get a (free) account on the forge.

To contribute to the GGI Resource Centre and GitLab Handbook, please follow these instructions:

* Create your (free) OW2 user account at https://www.ow2.org/view/services/registration
* Login once at https://www.ow2.org/

From there, depending on what you want to access:
* To access the GitLab group: login once to https://gitlab.ow2.org with your OW2 credentials, then let us know when it's done, and we'll grant you access to the GGI group in GitLab.
* To edit the [Resource Center](https://www.ow2.org/view/OSS_Governance/): send us your username, and we'll grant you appropriate access.
* For access to Rocket.Chat you need to:
  - Login at least once on https://gitlab.ow2.org using your OW2 credentials.
  - Open Rocket.Chat and choose a Rocket.Chat username when prompted.
  - Go to the #general channel and ask for GGI channel access there.
  - Once access is granted, you should be able to access the #good-governance channel.


## Code of conduct

Please note that we have, and enforce, a [code of conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) to ensure a fair, respectful, inclusive and welcoming community.

This applies to all activity and communications around the initiative, from mailing lists to virtual meetings and real-life gatherings. We are deeply committed to its inforcement, so please do not hesitate to contact us for any question, request or complaint.

Some parts of the site, like [OnRamp](/onramp/), also use the [Chatham House rule](https://www.chathamhouse.org/about-us/chatham-house-rule), so please be responsible when quoting people here.
