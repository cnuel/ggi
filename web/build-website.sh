#!/bin/sh

cd $(dirname $0)

# Update contributing guide from repo
echo '---
title: GGI - Contributing
layout: default
---

' > content/CONTRIBUTING.md
cat ../CONTRIBUTING.md >> content/CONTRIBUTING.md

# Update roadmap from repo
echo '---
title: GGI - Roadmap
layout: default
---

' > content/Roadmap.md
cat ../Roadmap.md >> content/Roadmap.md

hugo version
hugo
