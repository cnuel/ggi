#!/bin/bash
# by Silvério Santos for the OW2 Good Governance Initiative
# Usage: cd into the scripts folder and run this without any parameters
# Requires:
# bash packaqe
# gettext package (command: msgfmt)
# imagemagick package (command: convert)

# Fixed text additions
version="v1.x-dev"
date="2022-07-10"

# Cover image: box X-coords
boxStart=64
boxEnd=714
imgEnd=824

#####

tmpdir='/tmp/'
coverimg='../resources/latex/cover-empty.png'
fontname="DejaVu-Serif"

# Echoes a program header to standard output
function echoHeader() {
    echo "tsltCvrImg.sh"
    echo ""
    echo "Creates a cover image with text from translations in .po files."
    echo "Usage: cd into the scripts folder and run this command without parameters."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Languages configured as subfolders of translations
#declare -a langs=('de')
declare -a langs=()
for translatedLang in $(find ../translations -mindepth 1 -maxdepth 1 -type d); do
    langs+=($(basename $translatedLang))
done

# Checks POSIX compatible if the required commands are available.
# Checks if the configured font is available to imagemagick's convert
function checkConditions() {
    if ! command -v msgfmt &> /dev/null; then
        echo "Command msgfmt not found, please install the gettext package."
        exit 2
    fi
    if ! command -v convert &> /dev/null; then
        echo "Command convert not found, please install the imagemagick package."
        exit 2
    fi
    if [ -z "$(convert -list font | grep -i $fontname)" ]; then 
        echo "The configured font is not installed: $fontname"
        exit 2
    fi
}

# Convert .po to .mo
function convertPoToMo() {
    echo "Converting .po to .mo."
    mkdir -p $tmpdir/$1/LC_MESSAGES
    msgfmt -o $tmpdir/$1/LC_MESSAGES/cover.mo ../translations/$1/po_files/resources/cover.po
}

# Configure gettext
function configGettext() {
    export LANGUAGE=$1 
    export TEXTDOMAINDIR=$tmpdir
    export TEXTDOMAIN='cover'
}

# Draw text into image
function drawTitle() {
    echo "Drawing header."
    os=$(gettext -s "Open Source")
    gg=$(gettext -s "Good Governance")
    hb=$(gettext -s "Handbook")

    let cntrOffset=($imgEnd-$boxEnd-$boxStart)/2

    titleYStart=502
    titleYDiff=70
    yos=$titleYStart
    let ygg=$titleYStart+$titleYDiff
    let yhb=$titleYStart+$titleYDiff+$titleYDiff

    convert -font "$fontname" -pointsize 40 -fill white -gravity north -draw "text -$cntrOffset,$yos '$os' text -$cntrOffset,$ygg '$gg' text -$cntrOffset,$yhb '$hb'" $coverimg $tmpdir/cover-tmp.png
}

function drawFooter() {
    echo "Drawing footer."
    
    imgtargetdir=../translations/$1/resources/latex/
    
    footerXStart=88
    footerYStart=832
    footerYDiff=32
    yau=$footerYStart
    yvr=$footerYStart+$footerYDiff
    ydt=$footerYStart+$footerYDiff+$footerYDiff

    au=$(gettext -s "Authors: OW2 & The Good Governance Initiative participants")
    vr=$(gettext -s "Version:")" $version"
    dt=$(gettext -s "Date:")" $date"
    
    convert -font "$fontname" -pointsize 20 -fill white -draw "text $footerXStart,832 '$au' text $footerXStart,864 '$vr' text $footerXStart,896 '$dt'" $tmpdir/cover-tmp.png $imgtargetdir/cover.png 
}

# Cleanup
function cleanup() {
    echo "Cleaning up."
    rm -r $tmpdir/$1
    rm $tmpdir/cover-tmp.png
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader
checkConditions
echo "Languages found:"
for lang in ${langs[@]};  do
    echo $lang
done
for lang in ${langs[@]};  do
    echo "Processing language: $lang"
    convertPoToMo $lang
    configGettext $lang
    drawTitle
    drawFooter $lang
    cleanup $lang
done
endPrg
