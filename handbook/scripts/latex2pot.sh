#!/bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package po4a (po4a-gettextize), 

# Echoes a program header to standard output
function echoHeader() {
    echo "createPoFromLatex.sh"
    echo ""
    echo "Creates initial .pot files from LaTex files. Usually you do this only once, then only update!"
    echo "Usage: cd into the scripts folder and run this command with following parameters:"
    echo "createPoFromLatex.sh -a|<configured filename>"
    echo "-a: create new .pot files for all confgured .tex files"
    echo "<configured filename>: create new .pot files for thisl confgured .tex file"
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Checks POSIX compatible if the required commands are available.
function checkConditions() {
    if ! command -v po4a-gettextize &> /dev/null; then
        echo "Command po4a-gettextize not found, please install the po4a package."
        exit 1
    fi
}

# Translates the .tex file in the first parameter into the .pot file in the second parameter
# Parameters:
#   path relative to this script to the .tex source file
#   path relative to this script to the new .pot target file
function latex2po() {
    po4a-gettextize -M UTF-8 -L UTF-8 --copyright-holder "OW2 and others" --msgid-bugs-address "ossgovernance@ow2.org" -f latex -m $1 -p $2
}

# Translates the customisations.tex file into the .pot file
# To set up another .tex file: 
# - copy this function and adapt,
# - add calling this function to the -a option ,
# - and calling function when the filename is passed as parameter
function createCust() {
    echo "Processing customisations.tex..."
    latex2po ../resources/latex/customisations.tex ../resources/pot_files/resources/customisations.pot
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader
checkConditions

# Option -a passed: create all
while getopts "a" opt
do 
    if [ "${opt}" = "a" ] ; then
        # Add all create... function calls here
        createCust
        endPrg;
    fi
done

# Configured filename passed: create that
if [ $# -eq 0 ]; then
    echo "Required either the name of the LaTex file or -a to create all."
    exit 1;
elif [ $1 = "customisations.tex" ]; then
   createCust
   endPrg;
else
    echo "The provided filename is not configured to create a .pot file for."
fi
