#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#   Package translation toolkit (po2xliff, xliff2po),
#   Package LibreOffice (soffice),

declare -a langs=()
for translatedLang in $(find ../translations -mindepth 1 -maxdepth 1 -type d); do
    langs+=($(basename $translatedLang))
done

# Echoes a program header to standard output
function echoHeader() {
    echo "po2scorecard.sh"
    echo ""
    echo "Converts po to xliff files (temporarily) and combines that with the"
    echo "original ODT files to translated (permanent) ODT and PDF files."
    echo "Usage: cd into the scripts folder and run this command without parameters."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo "" 
}

# Checks POSIX compatible if the required commands are available.
function checkConditions() {
    if ! command -v po2xliff &> /dev/null; then
        echo "Error: Command po2xliff not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v xliff2odf &> /dev/null; then
        echo "Error: Command xliff2odf not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v soffice &> /dev/null; then
        echo "Error: Command soffice not found, please install the LibreOffice package."
        exit 2
    fi
    # Check for POSIX compatible environment variable TMPDIR pointing to the tempory files folder.
    if [ -z "$TMPDIR" ]; then 
        echo "Error: Environment variable TMPDIR does not exist. Please set the variable to your tempory files folder.";
        exit 2
    fi;
}

# Converts po to xliff files (temporarily) and combines that with the
# original ODT files to translated (permanent) ODT and PDF files.
function po2odtFile() {
    echo "File: $1, final: $2"

    for lang in ${langs[@]};  do
        echo "Language: $lang"
        
        # Create a (temporary) XLIFF from the translated PO
        po2xliff --progress=none -i ../translations/$lang/po_files/resources/$1.po -o $TMPDIR/$1.xlf
        # Create a (permanent) ODT from the translated XLIFF
        xliff2odf --progress=none -i $TMPDIR/$1.xlf  -t ../../resources/scorecards/$1.odt  -o ../translations/$lang/resources/latex/$1.odt
        # Create a PDF from the translated ODT
        soffice --headless --convert-to pdf --outdir ../translations/$lang/resources/latex/ ../translations/$lang/resources/latex/$1.odt
        # Copy pdf to final filename
        cp  -f ../translations/$lang/resources/latex/$1.pdf  ../translations/$lang/resources/latex/$2
    done
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader
echo "Languages found:"
for lang in ${langs[@]};  do
    echo $lang
done
po2odtFile "Scorecard-Template-v0" "customised_activity_scorecard_template.pdf"
endPrg
