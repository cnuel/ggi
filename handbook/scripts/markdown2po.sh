#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package mdpo (md2po), 
#   Package gettext (msgmerge), 
#   Folder content on the same level as tis script
# Todo: avoid langs definition in different scripts

# Languages to create PO files for and to have available for translation
declare -a langs=('de')

#####

# Echoes a program header to standard output
function echoHeader() {
    echo "markdown2po.sh"
    echo ""
    echo "Creates a subfolder structure for the configured languages if needed, "
    echo "extracts translatable strings from the handbook's .md files into language"
    echo "unspecific .pot files, creates/updates from the .pot files the language specific"
    echo ".po files to be translated."
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Creates a POT file and merges it with respective language specific PO file
# Parameters: File basename of the MD file (no extension), optionally prepended by the relative path
function mdToPoFile() {
    echo $1
    md2po -q -w 0 -d 'Content-Type: text/plain; charset=UTF-8' -d 'Language: en' -s --pofilepath ../resources/pot_files/content/$1.pot --po-encoding UTF-8 ../content/$1.md
    for lang in ${langs[@]};  do
        if [ -f ../translations/$lang/po_files/content/$1.po ]; then
            msgmerge -U ../translations/$lang/po_files/content/$1.po ../resources/pot_files/content/$1.pot;
        else
            msginit -i ../resources/pot_files/content/$1.pot -l $lang".UTF-8" --no-translator -o ../translations/$lang/po_files/content/$1.po
        fi
    done
}

echoHeader

# Create translation PO subfolders, if not exist
echo "Languages:"
for lang in ${langs[@]};  do
    echo $lang
    if [ ! -d ../translations/$lang/po_files ]; then
        mkdir -p ../translations/$lang/po_files/content
        mkdir -p ../translations/$lang/po_files/resources/html
    fi
done
echo "Files:"
for path in $(find ../content -type f -name '*.md'); do
    mdToPoFile `basename ${path} .md`
done
