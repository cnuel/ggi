#!/bin/bash
# by Silvério Santos for the OW2 Good Governance Initiative
# Usage: cd into the scripts folder and run this without any parameters
# Requires:
# bash packaqe
# gettext package (command: msgfmt)
# imagemagick package (command: convert)

#####

tmpdir='/tmp/'
emptyImg='../resources/images/ggi_maslow-empty.png'
imgWidth=1932
fontname="DejaVu-Serif"

# Echoes a program header to standard output
function echoHeader() {
    echo "tsltNtroImg.sh"
    echo ""
    echo "Creates a Maslow image with text from translations in .po files."
    echo "Usage: cd into the scripts folder and run this command without parameters."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Languages configured as subfolders of translations
#declare -a langs=('de')
declare -a langs=()
for translatedLang in $(find ../translations -mindepth 1 -maxdepth 1 -type d); do
    langs+=($(basename $translatedLang))
done

# Checks POSIX compatible if the required commands are available.
# Checks if the configured font is available to imagemagick's convert
function checkConditions() {
    if ! command -v msgfmt &> /dev/null; then
        echo "Command msgfmt not found, please install the gettext package."
        exit 2
    fi
    if ! command -v convert &> /dev/null; then
        echo "Command convert not found, please install the imagemagick package."
        exit 2
    fi
    if [ -z "$(convert -list font | grep -i $fontname)" ]; then 
        echo "The configured font is not installed: $fontname"
        exit 2
    fi
}

# Convert .po to .mo
function convertPoToMo() {
    echo "Converting .po to .mo."
    mkdir -p $tmpdir/$1/LC_MESSAGES
    msgfmt -o $tmpdir/$1/LC_MESSAGES/maslow.mo ../translations/$1/po_files/resources/maslow.po
}

# Configure gettext
function configGettext() {
    #export LC_ALL=$lang
    export LANGUAGE=$1 
    export TEXTDOMAINDIR=$tmpdir
    export TEXTDOMAIN='maslow'
}

# Draw text into left triangle
function drawTriangle() {
    echo "Drawing triangle."

    # Move offset to center
    cntrOffset=550
    # Vertical distance from the top corner
    yStart=175
    # Vertical distance of  texts to each other
    yDiff=100
    
    yl1=$yStart
    let yl2=$(($yStart + $yDiff))
    let yl3=$(($yStart + (2 * $yDiff)))
    let yl4=$(($yStart + (3 * $yDiff)))
    let yl5=$(($yStart + (4 * $yDiff)))

    convert -font "$fontname" -pointsize 24 -fill black -gravity north -draw " \
text -$cntrOffset,$yl1 '$(gettext -s "Self-Actualization")' \
text -$cntrOffset,$yl2 '$(gettext -s "Esteem")' \
text -$cntrOffset,$yl3 '$(gettext -s "Love & Belonging")' \
text -$cntrOffset,$yl4 '$(gettext -s "Safety")' \
text -$cntrOffset,$yl5 '$(gettext -s "Physiological Needs")' \
" $1 $2
}

# Draw goals
function drawGoals() {
    echo "Drawing goals"

    # Move offset to center
    center=980
    let cntrOffset=$((($center-($imgWidth)/2)*2))
    #cntrOffset=50
    # Vertical distance from the top
    yStart=115
    # Vertical distance of  texts to each other
    yDiff=114
    
    y1=$yStart
    let y2=$(($yStart + $yDiff))
    let y3=$(($yStart + (2 * $yDiff)))
    let y4=$(($yStart + (3 * $yDiff)))
    let y5=$(($yStart + (4 * $yDiff)))

    convert -font "$fontname" -pointsize 34 -fill black -gravity north -draw " \
text $cntrOffset,$y1 '$(gettext -s "Strategy")' \
text $cntrOffset,$y2 '$(gettext -s "Engagement")' \
text $cntrOffset,$y3 '$(gettext -s "Culture")' \
text $cntrOffset,$y4 '$(gettext -s "Trust")' \
text $cntrOffset,$y5 '$(gettext -s "Usage")' \
" $1 $2
}

# Draw goal boxes
function drawGoalbox() {
    echo "Drawing goal boxes"

    # Vertical distance from the top of the text in the first box
    yStart=77

    # Box: Horizontal distance from the left border
    boxOffsetX=1130
    # Box: Vertical distance of each box to another
    boxDiffY=23
    # Box: size
    boxSizeX=800
    boxSizeY=94
    
    boxSt=$(gettext -s "Embracing the full potential of OSS. Proactively using OSS for innovation and competetiveness.")
    boxEn=$(gettext -s "Engaging with the OSS ecosystem. Contributing back. Developing visibility, event participation.")
    boxCu=$(gettext -s "Implementing best practices. Developing OSS culture. Sharing experience.")
    boxTr=$(gettext -s "Securely and responsibly using OSS. Compliance and dependency management policies.")
    boxUs=$(gettext -s "Technically using OSS. Technical ability and experience with OSS. Some OSS awareness.")
    
    ySt=110
    yEn=$(($ySt+$boxDiffY+$boxSizeY))
    yCu=$(($ySt+($boxDiffY+$boxSizeY)*2))
    yTr=$(($ySt+($boxDiffY+$boxSizeY)*3))
    yUs=$(($ySt+($boxDiffY+$boxSizeY)*4))
    
    convert -font "$fontname" -pointsize 22 -size ${boxSizeX}x${boxSizeY} -fill white -draw " \
text $boxOffsetX,$ySt '$boxSt' \
text $boxOffsetX,$yEn '$boxEn' \
text $boxOffsetX,$yCu '$boxCu' \
text $boxOffsetX,$yTr '$boxTr' \
text $boxOffsetX,$yUs '$boxUs' \
" $1 $2
}

function drawFooter() {
    echo "Drawing footer."
        
    footerY=680

    subTri=$(gettext -s "Abraham Maslow's Hierarchy of Behavioral Motives")
    subGoal=$(gettext -s "OW2 Goals to OSS Good Governance")
    
    convert -font "$fontname" -pointsize 30 -fill black  -gravity north -draw " \
text -550,$footerY '$subTri' \
text 550,$footerY '$subGoal' \
" $1 $2
}

# Cleanup
function cleanup() {
    echo "Cleaning up."
    rm -r $tmpdir/$1
    rm $tmpdir/maslow-tmp.png
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader
checkConditions
echo "Languages found:"
for lang in ${langs[@]};  do
    echo $lang
done
for lang in ${langs[@]};  do
    echo "Processing language: $lang"
    convertPoToMo $lang
    configGettext $lang
    drawTriangle "$emptyImg" "$tmpdir/maslow-tmp.png"
    drawGoals "$tmpdir/maslow-tmp.png" "$tmpdir/maslow-tmp.png"
    drawGoalbox "$tmpdir/maslow-tmp.png" "$tmpdir/maslow-tmp.png"
    imgtargetdir="../translations/$lang/resources/images/"
    drawFooter "$tmpdir/maslow-tmp.png" "$imgtargetdir/ggi_maslow.png"
    cleanup $lang
done
endPrg
