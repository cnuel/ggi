#!/bin/bash

######################################################################
# Copyright (c) 2021 Castalia Solutions and others
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
######################################################################

# Default values
UPDATE=NO
DEST_WEB=""

CMD_PANDOC=pandoc
# Export environment variable to use non-default binary for "pandoc"
# Example:
# export CMD_PANDOC=/opt/pandoc/pandoc-2.16/bin/pandoc

while [ $# -gt 0 ]; do
    key="$1"

    case $key in
	-u|--update)
	    UPDATE=YES
	    shift # shift argument
	    ;;
	-l|--language)
	    GGI_LANG="$2"
	    shift # shift argument
	    shift # shift value
	    ;;
	-w|--write-web)
	    DEST_WEB="$2"
	    shift # shift argument
	    shift # shift value
	    ;;
	*) # unknown option
	    echo "Unknown parameter $1. Exit."
	    exit 4
	    ;;
    esac
done

echo "# Setup python3 env."
if [ -d ./env ]; then
    echo "  - Reusing ./env"
else
    echo "  - Creating env in ./env"
    python3 -m venv env/
    BASEDIR=$(dirname "$0")
    pip install -r $BASEDIR/../requirements.txt
fi
. env/bin/activate
    
echo "# Start build of the body of knowledge."
if [ "$UPDATE" = "YES" ]; then
    echo "  - Update activities."
else
    echo "  - No update of activities."
fi

if [ "$GGI_LANG" != "" ]; then
    echo "  - Building for language $GGI_LANG."
    GGI_LANG_OPT="-V lang=$GGI_LANG"
else
    echo "  - Building main english version."
fi

if [ "$DEST_WEB" != "" ]; then
    echo "  - Write web files to $DEST_WEB."
else
    echo "  - No web export."
fi

if [ "$UPDATE" = "YES" ]; then
    echo "# Clean up activities."
    rm content/5*_activity_*.md

    echo "# Retrieving activities from GitLab."
    python3 get_activities.py
fi

echo '# Check that we have everything we need to build the PDF.'
if [ -d 'content' ] && [ -d 'resources' ]; then
    echo '  - Found content and resources dirs.'
else
    echo 'ERROR: Cannot find content or resources dirs. Passing build.'
    exit 0
fi

echo "# Generating pdf file."
$CMD_PANDOC content/*.md \
       --number-sections \
       --from=gfm \
       --include-before-body resources/latex/title.tex \
       --include-in-header resources/latex/customisations.tex \
       -V documentclass:article $GGI_LANG_OPT \
       -V linkcolor:blue \
       -V geometry:a4paper \
       -V geometry:margin=2cm \
       -V mainfont="DejaVu Serif" \
       -V monofont="DejaVu Sans Mono" \
       -M "The GGI Good Book" \
       -o ggi_handbook.tex \
       --pdf-engine=xelatex \
       --toc --toc-depth=2
#       -o ggi_handbook.tex \

head -n-1 ggi_handbook.tex > ggi_handbook_with_appendices.tex
echo "
\includepdf[pages=-]{resources/latex/customised_activity_scorecard_template.pdf}

\end{document}" >> ggi_handbook_with_appendices.tex

xelatex ggi_handbook_with_appendices.tex
xelatex ggi_handbook_with_appendices.tex
xelatex ggi_handbook_with_appendices.tex

mv ggi_handbook_with_appendices.pdf ggi_handbook.pdf

# echo "# Generating pdf file with carlito."
# $CMD_PANDOC content/*.md \
#        --number-sections \
#        --from=gfm \
#        --include-before-body resources/latex/title.tex \
#        --include-in-header resources/latex/customisations.tex \
#        -V documentclass:article \
#        -V linkcolor:blue \
#        -V geometry:a4paper \
#        -V geometry:margin=2cm \
#        -V fontsize=12pt \
#        -V mainfont="Carlito" \
#        -V monofont="DejaVu Sans Mono" \
#        -M "The GGI Good Book" \
#        -o ggi_handbook_carlito.pdf \
#        --pdf-engine=xelatex \
#        --toc --toc-depth=3

if [ "$DEST_WEB" != "" ]; then
    echo "# Generate Markdown export for website."
    rm -rf export_web/
    mkdir -p export_web/

    # BoK early chapters
    for md in introduction organisation methodology conclusion; do
	cp resources/html/$md* export_web/
	file=$(ls export_web/$md*)
	dir=${file%.md}
	mkdir -p $dir
	mv $file $dir/index.md
	cat content/*$md* >> $dir/index.md
	sed -i "s!resources/images/!/images/ggi/!" $dir/index.md
	sed -i 's/^###/##/' $dir/index.md
    done

    mkdir -p export_web/activities/
    for activity in `ls content/*activity*`; do
	base_name=$(basename $activity)
	file_name=${base_name#*_}
	cp resources/html/activity.md export_web/activities/${file_name}
	cat ${activity} >> export_web/activities/${file_name}
	file=$(ls export_web/activities/${file_name}*)
	title=$(grep -E "^## " $activity | cut -d\  -f2-)
	file_out=$(echo $title | tr " " "_" | tr "[A-Z]" "[a-z]")
	dir="export_web/activities/${file_out}"
	echo "- export to [$dir]."
	mkdir -p $dir
	mv $file $dir/index.md
	sed -i "s/GGI_ACTIVITY_TITLE/${title}/" $dir/index.md
	sed -i 's/^###/##/' $dir/index.md
    done

    rm -rf $DEST_WEB/[1-9]{2}_*
    if [ ! -d $DEST_WEB ]; then
	echo "Creating directory $DEST_WEB"
	mkdir -p $DEST_WEB/
    fi
    cp -r export_web/* $DEST_WEB/
fi

# Cleaning up temporary files.
rm ggi_handbook_with_appendices.aux ggi_handbook_with_appendices.log ggi_handbook_with_appendices.tex ggi_handbook_with_appendices.toc ggi_handbook.tex



