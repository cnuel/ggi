#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package bash
#   Package mdpo (po2md), 
#   File structure as created by markdown2po.sh
# Todo: avoid langs definition in different scripts

# Languages to create PO files for and to have available for translation
declare -a langs=('de')

#####

# Echoes a program header to standard output
function echoHeader() {
    echo "po2markdown.sh"
    echo ""
    echo "Transforms all configured language's translations from .po files"
    echo "based on their original .md files into translated .md files."
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""    
}

# Creates a translated MD file based on the language specific (translated) PO file and the original MD file
function poToMdFile() {
    for lang in ${langs[@]};  do
        po2md -q -w 0 --po-encoding UTF-8 -p ../translations/$lang/po_files/content/$1.po -s ../translations/$lang/content/$1.md ../content/$1.md
    done
}

echoHeader

# Create folders for translated MD files, if not exist
echo "Languages:"
for lang in ${langs[@]};  do
    echo $lang
    if [ ! -d ../translations/$lang/content ]; then
        mkdir -p ../translations/$lang/content
    fi
    if [ ! -d ../translations/$lang/resources ]; then
        mkdir -p ../translations/$lang/resources/html
        mkdir -p ../translations/$lang/resources/images
        mkdir -p ../translations/$lang/resources/latex
    fi
done

# Translates PO files as in subfolder translate into MD files
echo "Files:"
for path in $(find ../resources/pot_files/content/ -type f -name '*.pot'); do
    echo `basename ${path}`
    poToMdFile `basename ${path} .pot`
done
