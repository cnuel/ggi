#!/bin/bash

######################################################################
# Copyright (c) 2021 Castalia Solutions and others
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
######################################################################

# Launch using "bash paperback_convert_handbook.sh"

# Default values

CMD_PANDOC=pandoc
# Export environment variable to use non-default binary for "pandoc"
# Example:
# export CMD_PANDOC=/opt/pandoc/pandoc-2.16/bin/pandoc

while [ $# -gt 0 ]; do
    key="$1"

    case $key in
	-c|--cache)
	    UPDATE=NO
	    shift # shift argument
	    ;;
	-l|--language)
	    GGI_LANG="$2"
	    shift # shift argument
	    shift # shift value
	    ;;
	*) # unknown option
	    echo "Unknown parameter $1. Exit."
	    exit 4
	    ;;
    esac
done

if [ "$GGI_LANG" != "" ]; then
    echo "  - Building for language $GGI_LANG."
    GGI_LANG_OPT="-V lang=$GGI_LANG"
else
    echo "  - Building main english version."
fi

echo "# Start build of the body of knowledge."

rm -rf /tmp/content
cp -r content /tmp

for file in /tmp/content/*.md; do sed -r -i 's/\((http\S*)\)/\(\1\) \(\1\)/g' $file; done
 
echo "# Generating pdf file."
$CMD_PANDOC /tmp/content/*.md \
       --number-sections \
       --from=gfm \
       --include-before-body resources/latex/paperback_title.tex \
       --include-in-header resources/latex/customisations.tex \
       -V documentclass:article $GGI_LANG_OPT \
       -V linkcolor:blue \
       -V geometry:a4paper \
       -V geometry:left=20mm \
       -V geometry:right=20mm \
       -V geometry:top=30mm \
       -V geometry:bottom=30mm \
       -V mainfont="DejaVu Serif" \
       -V monofont="DejaVu Sans Mono" \
       -M "The GGI Good Book" \
       -o paperback_ggi_handbook.tex \
       --pdf-engine=xelatex \
       --toc --toc-depth=2
#       -o paperback_ggi_handbook.tex \

head -n-1 paperback_ggi_handbook.tex > paperback_ggi_handbook_with_appendices.tex

# Enlarge PDF scorecard margins to fit printing zone
gs -sDEVICE=pdfwrite -sPageList=1 -dDEVICEHEIGHTPOINTS=600 -dDEVICEWIDTHPOINTS=800 -dFIXEDMEDIA -dPDFFitPage -o /tmp/paperback_customised_activity_scorecard_template.pdf resources/latex/customised_activity_scorecard_template.pdf

# Add PDF scorecard at the end of document
echo "
\\includepdf[pages=-]{/tmp/paperback_customised_activity_scorecard_template.pdf}" >> paperback_ggi_handbook_with_appendices.tex
echo "
\\includepdf[pages=-]{resources/latex/paperback_legal_mentions.pdf}" >> paperback_ggi_handbook_with_appendices.tex
echo "\\end{document}" >> paperback_ggi_handbook_with_appendices.tex

# Page numbering +ToC issues require to run xelatex... 3 times(!)
# Yes it is *really* intentional :)
xelatex paperback_ggi_handbook_with_appendices.tex
xelatex paperback_ggi_handbook_with_appendices.tex
xelatex paperback_ggi_handbook_with_appendices.tex

# Cleanup
rm paperback_ggi_handbook_with_appendices.tex paperback_ggi_handbook.tex
rm paperback_ggi_handbook_with_appendices.aux paperback_ggi_handbook_with_appendices.toc
mv paperback_ggi_handbook_with_appendices.pdf paperback_ggi_handbook.pdf

echo "Paperback internal part of handbook generated in paperback_ggi_handbook.pdf"

