## Politique d'achat de logiciels libres

Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/43>.

### Description 

Cette activité consiste à mettre en œuvre un processus de sélection, d'acquisition, d'achat de logiciels et de services open source. Il s'agit également de prendre en compte le coût réel des logiciels libres et de les provisionner. Les logiciels libres peuvent être "gratuits" à première vue, mais ils ne sont pas sans coûts internes et externes tels que l'intégration, la formation, la maintenance et le support. 

Une telle politique exige que les solutions open source et propriétaires soient considérées de manière symétrique lors de l'évaluation de la valeur de l'investissement comme la combinaison optimale entre le coût total de possession et le rapport qualité-prix. Par conséquent, le département des achats informatiques doit considérer activement et équitablement les options open source, tout en s'assurant que les solutions propriétaires sont considérées sur un pied d'égalité dans les décisions d'achat.

La préférence pour l'open source peut être explicitement exprimée sur la base de la flexibilité intrinsèque de l'option open source lorsqu'il n'y a pas de différence de coût global significative entre les solutions propriétaires et open source.

Les services d'achat doivent comprendre que les entreprises qui offrent un soutien aux logiciels libres ne disposent généralement pas des ressources commerciales nécessaires pour participer à des appels d'offres, et adapter leurs politiques et processus d'achat de logiciels libres en conséquence.

### Évaluation de la progression 

Les **points de vérification** suivants démontrent le progrès dans cette activité :
- [ ] Les nouveaux appels à propositions demandent de manière proactive des soumissions de sources ouvertes.
- [ ] Le service des achats dispose d'un moyen d'évaluer les solutions à code source ouvert par rapport aux solutions propriétaires.
- [ ] Un processus d'achat simplifié pour les logiciels et services à code source ouvert a été mis en place et documenté.
- [ ] Un processus d'approbation faisant appel à une expertise interfonctionnelle a été défini et documenté.

### Évaluation de l'opportunité 

Plusieurs raisons justifient les efforts pour mettre en place des politiques spécifiques d'approvisionnement en logiciels libres :
* L'offre de logiciels et de services commerciaux open source est croissante et ne peut être ignorée, et nécessite la mise en place de politiques et de processus d'approvisionnement spécifiques.
* Il existe une offre croissante de solutions commerciales open source très compétitives pour les systèmes d'information des entreprises.
* Même après avoir adopté un composant open source gratuit et l'avoir intégré dans une application, des ressources internes ou externes doivent être fournies pour maintenir ce code source. 
* Le coût total de possession (TCO) est souvent (mais pas nécessairement) plus faible pour les solutions open source : pas de frais de licence à payer lors de l'achat/la mise à niveau, marché ouvert pour les fournisseurs de services, possibilité de fournir une partie ou la totalité de la solution soi-même.

### Évaluation de la progression 

Les **points de vérification** suivants démontrent le progrès dans cette activité :
- [ ] Les nouveaux appels à propositions demandent de manière proactive des soumissions de solutions open source.
- [ ] Le service des achats dispose d'un moyen d'évaluer les solutions open source par rapport aux solutions propriétaires.
- [ ] Un processus d'achat simplifié pour les logiciels et services open source a été mis en place et documenté.
- [ ] Un processus d'approbation faisant appel à une expertise interfonctionnelle a été défini et documenté.

### Recommandations 

* "Be sure to tap into the expertise of your IT, DevOps, cybersecurity, risk management, and procurement teams when creating the process." (from [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/), en anglais).
* La loi sur la concurrence peut exiger que le terme "open source" ne soit pas spécifiquement mentionné.
* Sélectionnez la technologie en amont, puis passez à l'appel d'offres pour les services de personnalisation et d'assistance.

### Ressources 

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack) : ce n'est pas nouveau, mais c'est une excellente lecture de nos collègues d'OSS-watch au Royaume-Uni. Consultez les [diapositives] (http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices] (https://anchore.com/blog/5-open-source-procurement-best-practices/) : un article récent sur l'approvisionnement en logiciels libres avec des conseils utiles.

