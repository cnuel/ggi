# Introduction

Ce document introduit une méthodologie pour petrte en oeuvre une gestion professionnelle de l'open source au sein d'une organisation. Il répond aux besoins d'utiliser les logiciels open source de manière adéquate et équitablea et de protéger l'organisation des risques techniques ou juridiques, afin de maximiser les avantages de l'open source. Quel que soit votre degré d'implication dans ce domaine, ce document vous proposera conseils et idées vous permettant de progresser et faire de votre démarche un succès.

## Contexte

La plupart des grands utilisateurs ou intégrateurs utilisent déjà du logiciel open source (FOSS = Free and Open Source Software), que ce soit dans leur propre système d'information, ou dans leur offre de produits ou services. La conformité (parfois nommée "compliance") est une préoccupation croissante, et beaucoup de grandes entreprises ont nommé des responsables dédiés. Cependant, bien que l'assainissement de la chaîne de production open source d'une organisation soit fondamental (c'est généralement le but de la conformité), les utilisateurs *doivent* soutenir les communautés et contribuer à la soutenabilité de l'écosystème open source. Nous concevons la gouvernance de l'open source comme englobant l'écosystème dans son ensemble, s'engageant dans les communautés locales, cultivant une saine relation avec les fournisseurs et prestataires de service du logiciel open source. La notion de "conformité" change alors de dimension : voilà ce qu'est la _bonne_ gouvernance de l'open source.

Cette initiative va au-delà de la conformité et de la responsabilité juridique. Il y est question de sensibiliser les utilisateurs (souvent eux-mêmes développeurs de logiciel) et intégrateurs, et de développer des relations mutuellement bénéfiques au sein de l'écosystème open source européen.

La Bonne Gouvernance Open Source permet à tous les types d'organisations (PME ou grandes entreprises, mairies, universités, associations, etc...) de maximiser les avantages tirés de l'open source en les aidant à aligner les processus, les gens, la technologie et la stratégie. Et dans ce domaine, optimiser les avantages de l'open source, particulièrement en Europe, nous sommes tous en phase d'apprentissage et d'innovation, et nul ne sait vraiment où il en est vis-à-vis de l'état de l'art en ce domaine.

Cette initiative vise à aider les organisations à atteindre ce but, avec :
* Un catalogue structuré d'**activités**, feuille de route pour la mise en oeuvre d'une gestion professionnelle de l'open source.
* Un **outillage de gestion** pour définir, suivre, rapporter et communiquer l'éat de la progression.
* Un **cheminement clair et pratique vers l'amélioration**, graduel et abordable, pour Catténuer les risques, former les gens, adapter les processus, communiquer au sein et à l'extérieur de l'organisation.
* **Des conseils d'orientation** et un ensemble de **références sélectionnées** au sujet des licences open source, des bonnes pratiques, de la formation, et de l'engagement dans l'écosystème, pour tirer parti de la connaissance et la culture open source afin de consolider son savoir interne et d'étendre son leadership.

Ce guide a été développé dans le respect des exigences suivantes :
* Etre applicable à totu type d'organisation : de la PME à la grande entreprise ou l'organisation sans but lucratif, des autorités locales (comme les mairies) aux grandes institutions (Européennes ou gouvernementales, par exemple). Le cadre fournit des briques pour bâtir une stratégie et des indications pour sa mise en oeuvre, mais *comment* les activités sont exécutées dépend entièrement du contexte et reste à la discrétion du responsable du programme. Il peut être utile d'avoir recours à du conseil ou d'échanger avec ses pairs.
* Ne faire aucune hypothèse quant au niveau de connaissance technique interne à l'organisation ou au secteur d'activité. Par exemple, certaines organisations auront besoin de mettre en place un parcours complet de formation, alors que d'autres pourraient se contenter de proposer du contenu ad-hoc aux équipes.

Certaines activités ne seront pas applicables à toutes les situations, mais le cadre global fournit une feuille de route détaillée et ouvre la voie à des stratégies sur mesure.

## A propos de l'initiative "Good Governance"

Chez OW2, une initiative est un effort commun pour satisfaire un besoin professionnel. L'initiative [OW2 OSS Good Governance](https://www.ow2.org/view/OSS_Governance) propose un cadre méthodologique pour mettre en place une gestion professionnelle de l'open source au sein d'une organisation.

L'initiative "Good Governance" est basée sur un modèle détaillé inspiré par la célèbre pyramide d'Abraham Maslow pour décrire les motivations humaines, comme illustré par l'image ci-dessous.

![Maslow et la GGI](resources/images/ggi_maslow.png)

Par les idées, directives et activités qu'elle propose, l'initiative "Good Governance" est un schéma directeur de mise en place d'entités chargées de la gestion des logiciels open source, habituellement appelées OSPO (pour "Open Source Program Office"). La méthode est aussi un système de management pour définir des priorités, assurer le suivi et partager les progrès réalisés.

Les organisations mettant en place la méthodologie "Good Governance" vont accroître leur savoir-faire dans plusieurs directions, parmi lesquelles :

* **utiliser** le logiciel open source à bon escient et de façon sûre pour améliorer la réutilisation et la maintenabilité du logiciel et accroître l'agilité de développement;
* **atténuer** les risques juridiques et techniques associés au code de provenance externe et à la collaboration;
* **identifier** les besoins de formation des équipes, des développeurs aux chefs d'équipe et managers, afin que tout le monde partage une même vision;
* **prioriser** les objectifs et activités, pour développer une stratégie open source efficace;
* **communiquer** efficacement en interne et vers l'extérieur pour tirer le meilleur de la stratégie open source;
* **améliorer** la compétitivité et l'attractivité de l'organisation pour les meilleurs talents de l'open source.

## A propos de l'"OSPO Alliance"

L'**OSPO Alliance** a été créée par une coalition d'organisations Européennes sans but lucratif leaders dans l'open source, incluant OW2, la fondation Eclipse, OpenForum Europe, et la Foundation for Public Code, avec la mission d'accroître la sensibilisation à l'open source en Europe et dans le monde et promouvoir un management structuré et professionnel de l'open source par les entreprises et les administrations.

Alors que l'initiative "Good Governance" d'OW2 se focalise sur la mise au point d'une méthode de management, l'"OSPO Alliance" intègre l'objectif plus large d'aider les entreprises, particulièrement dans les secteurs non technologiques, et les institutions publiques à découvrir et comprendre l'open source, commencer à en bénéficier dans leurs activités, et évoluer jusqu'à accueillir leur propre OSPO.

L'"OSPO Alliance" a mis en place le site web **OSPO.Zone** accessible sur https://ospo.zone. Basé sur l'inititative "Good Governance" d'OW2, OSPO.Zone est un référentiel pour un ensemble complet de ressources pour les entreprises, institutions publiques, et organismes académiques ou de recherche. OSPO.Zone permet à l'Alliance de se connecter avec les OSPO à travers l'Europe et le monde, ainsi qu'avec les organisations communautaires soutenant la démarche, pour encourager les bonnes pratiques et encourager les contributions à soutenir l'écosystème open source. Visitez le site web [OSPO Zone](https://ospo.zone) pour un aperçu rapide du cadre des meilleures pratiques de gestion de l'IT.

Le site web [OSPO Zone](https://ospo.zone) est également l'endroit où nous collectons les retours utilisateur concernant l'initiative et son contenu (par exemple, les activités, le corpus des connaissances...), provenant de la communauté au sens large.

## Contributeurs

Merci aux talentueux contributeurs de l'initiative "Good Governance" :

* Frédéric Aatz (Microsoft France)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (Ville de Paris)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Max Mehl (Free Software Foundation Europe)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business Services)
* Cédric Thomas, our master of ceremony (OW2)
* Nicolas Toussaint (Orange Business Services)


## Licence

Cette oeuvre est publiée sous licence [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0). Selon le site web Creative Commons :

> Vous êtes autorisé(e) à:
> * partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
> * Adapter — remixer, transformer et créer à partir du matériel pour toute utilisation, y compris commerciale.
>
> Vous devez créditer l'oeuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'offrant vous soutient ou soutient la façon dont vous avez utilisé son oeuvre.

Tout le contenu est sous Copyright: 2020-2021 OW2 & Les participants de l'initiative "Good Governance".

