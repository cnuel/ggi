## Open source et souveraineté numérique

Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/35>.

### Description 

La souveraineté numérique peut être définie comme

> “La capacité et la possibilité pour les individus et les institutions d'exécuter leur(s) rôle(s) dans le monde numérique de manière indépendante, intentionnelle et sûre.”
> &mdash; Centre de compétences pour l'informatique publique, Allemagne

Afin de mener ses activités correctement, toute entité doit pouvoir s'appuyer sur plusieurs partenaires, services, produits et outils. L'examen des liens et des contraintes de ces dépendances permet à l'organisation d'évaluer et de contrôler sa soumission aux facteurs externes, et par conséquent d’améliorer son autonomie et sa résilience.

Par exemple, une dépendance exclusive à un fournisseur peut entraver les processus et la valeur ajoutée de l’organisation ; en tant que telle, il faut donc l’éviter. L'open source procure un moyen de contourner ce verrouillage. L'open source joue un rôle important dans la souveraineté numérique, en permettant un plus grand choix entre les solutions, les fournisseurs et les intégrateurs, et un meilleur contrôle des feuilles de route informatiques.

Il convient de noter que la souveraineté numérique n'est pas une question de confiance : nous devons évidemment faire confiance à nos partenaires et fournisseurs, mais la relation est encore meilleure lorsqu'elle est fondée sur la reconnaissance et le consentement mutuels, plutôt que sur des contrats forcés et sur des contraintes.

Une meilleure souveraineté numérique apporte les avantages suivants :
* L'organisation peut faire ses propres choix sans contraintes.
* L'entreprise devient plus résiliante face aux acteurs et aux facteurs externes.
* Elle améliore sa position de négociation face aux partenaires et fournisseurs de services.

### Opportunités à déterminer 

* Dans quelle mesure est-il difficile/coûteux de s'éloigner d'une solution ?
* Les fournisseurs de solutions peuvent-ils imposer des conditions non souhaitées à leurs services (par exemple, un changement de licence ou une mise à jour de contrats) ?
* Les fournisseurs de solutions pourraient-ils augmenter unilatéralement leurs prix, faute d’alternatives ?

### Progrès à évaluer 

Les **points de verification** suivants démontrent l'avancement de cette activité :
- [ ] Les dépendances critiques de fournisseurs et de partenaires de l’organisation sont évaluées.
- [ ] Il existe un plan de secours face à ces dépendances identifiées.
- [ ] L’exigence de souveraineté numérique est déclarée à chaque nouvelle solution étudiée.

### Recommandations 

* Identifier les risques de dépendances critiques des fournisseurs de services et les tiers.
* Mettre à jour une liste d'alternatives open source pour les services critiques.
* Ajouter le critère de souveraineté numérique dès l’examen de tout nouvel outil ou service.

### Ressources 

* [Une introduction à la souveraineté numérique et à l'Open Source : partie I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) et [Une introduction à la souveraineté numérique et à l'Open Source : partie II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), du site Open-Sourcerers.
* L’excellent article de superuser.openstack.org sur [Le rôle de l'Open Source dans la souveraineté numérique](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). En voici un court extrait :
  > La souveraineté numérique est une préoccupation majeure du 21ème siècle, en particulier pour l'Europe. L'open source a un rôle majeur à jouer dans la mise en place de la souveraineté numérique, en permettant à chacun d'accéder à la technologie nécessaire, mais aussi en fournissant la transparence de la gouvernance et l'interopérabilité nécessaires au succès de ces solutions.
* Le point de vue de l'Union européenne sur la souveraineté numérique, selon l'[Open Source Observatory (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Open Source, souveraineté numérique et interopérabilité : La déclaration de Berlin.
* La position de l'UNICEF sur l'[Open Source pour la souveraineté numérique](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
