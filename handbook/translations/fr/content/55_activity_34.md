## Sensibiliser les directions

Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/34>.

### Description 

L'initiative open source de l'organisation ne produira ses avantages stratégiques que si elle est appliquée au plus haut niveau, en intégrant l'ADN open source dans la stratégie et le fonctionnement interne de l'entreprise. Un tel engagement ne peut se produire si les cadres supérieurs et le comité de direction n'en font pas eux-mêmes partie. La formation et l'état d'esprit open source doivent également être étendus à ceux qui façonnent les politiques, les décisions et la stratégie globale, tant à l'intérieur qu'à l'extérieur de l'entreprise.

Cet engagement garantit que les améliorations pratiques, les changements d'état d'esprit et les nouvelles initiatives bénéficient d'un soutien constant, bienveillant et durable de la part de la hiérarchie, ce qui entraîne une participation plus fervente des travailleurs. Il façonne la façon dont les acteurs externes perçoivent l'organisation, ce qui apporte des avantages en termes de réputation et d'écosystème. C'est également un moyen d'établir l'initiative et ses avantages à moyen et long terme.


### Opportunités à déterminer 

Cette activité devient essentielle si/quand :

* L'organisation a fixé des objectifs globaux relatifs à la gestion des logiciels open source, mais peine à les atteindre. Il est peu probable que l'initiative puisse aboutir sans une bonne connaissance et un engagement clair des cadres supérieurs.
* L'initiative a déjà commencé et progresse, mais les niveaux supérieurs de la hiérarchie ne la suivent pas correctement.

Avec un peu de chance, il devrait devenir évident que tout ce qui n'est pas une utilisation ad hoc de l'open source nécessite une approche cohérente et bien pensée, étant donné l'éventail d'équipes et le changement culturel qu'il peut apporter.

### Progrès à évaluer 

Les points de vérification suivants démontrent les progrès réalisés par cette activité :
- [ ] Il existe un bureau/responsable de la gouvernance mandaté pour définir une stratégie uniforme en matière de logiciels open source dans toute l'entreprise et s'assurer que le champ d'application est clair.
- [ ] Il y a un engagement clair et contraignant de la hiérarchie envers la stratégie de logiciels open source.
- [ ] Il y a une communication transparente de la hiérarchie sur son engagement envers le programme. 
- [ ] La hiérarchie est disponible pour discuter des logiciels libres. Elle peut être sollicitée et mise au défi sur ses promesses.
- [ ] Il y a un budget et un financement appropriés pour l'initiative.

### Recommandations 

Voici quelques exemples d'actions associées à cette activité :
* Organiser des formations pour démystifier le logiciel open source auprès des cadres supérieurs.
* Obtenir une approbation explicite et pratique de l'utilisation et de la stratégie du logiciel open source.
* Mentionner et approuver explicitement le programme open source dans les communications internes.
* Mentionner et approuver explicitement le programme open source dans les communications publiques.

* L'open source est un _outil stratégique_ qui permet d'instaurer une _culture d'entreprise_. Qu'est-ce que cela signifie ?
* L'open source peut être utilisé comme un mécanisme pour perturber les fournisseurs et réduire les coûts d'acquisition des logiciels.
    * L'open source doit-il relever de la compétence des _gestionnaires d'actifs logiciels_ ou des _services d'achat_ ? 
* Les licences de logiciels open source consacrent les libertés qui procurent les avantages de l'open source, mais elles comportent également des obligations. Si elles ne sont pas respectées de manière appropriée, les responsabilités peuvent créer des risques juridiques, commerciaux et d'image pour une organisation.
    * Les conditions de la licence permettront-elles de voir des parties du code qui devraient rester confidentielles ?
    * Cela aura-t-il un impact sur le portefeuille de brevets de mon organisation ?
    * Comment les équipes de projet doivent-elles être formées et soutenues à ce sujet ?
* La plus grande valeur de l'open source réside dans la contribution aux projets open source externes.
    * Comment mon entreprise doit-elle encourager (et suivre) cette démarche ?
    * Comment les développeurs doivent-ils utiliser GitHub, GitLab, Slack, Discord, Telegram, ou tout autre outil que les projets open source utilisent habituellement ?
    * L'open source peut-il avoir un impact sur les politiques de ressources humaines de l'entreprise ?
* Bien sûr, il ne s'agit pas seulement de contribuer en retour, qu'en est-il de mes propres projets open source ?
    * Suis-je prêt à faire de l'innovation ouverte ?
    * Comment mes projets vont-ils gérer les contributions entrantes ?
    * Dois-je faire l'effort d'entretenir une communauté pour un projet donné ?
    * Comment dois-je diriger la communauté, quel rôle doivent jouer les membres de la communauté ?
    * Suis-je prêt à céder les décisions relatives à la feuille de route à une communauté ?
    * L'open source peut-il être un outil précieux pour réduire le cloisonnement entre les équipes de l'entreprise ?
    * Dois-je gérer le transfert de l'open source d'une entité de l'entreprise à une autre ?
    
