# Conclusion


## La feuille de route

Nous prévoyons de travailler sur les fonctionnalités suivantes, après cette première version que l’on peut considérer comme une première étape du chemin vers le Zen de la bonne gouvernance des logiciels open source :
* **Définir des rôles** pour les activités, afin que chacun puisse choisir et commencer à travailler sur des éléments en fonction de ses missions et de ses capacités. Cela aidera à confier les bonnes tâches aux bonnes personnes, et ouvrira une nouvelle perspective sur le programme.
* **Améliorer et développer** la partie de la méthodologie grâce aux commentaires recueillis auprès de la communauté. Tout en travaillant sur la mise en œuvre de l'initiative de bonne gouvernance, les responsables de l'open source obtiendront de nouvelles perspectives et rassembleront des expériences qui nous aideront à construire un meilleur modèle et une meilleure méthodologie pour le corpus de connaissances.
* **Améliorer les activités canoniques** grâce aux commentaires recueillis auprès de la communauté, et **ajouter de nouvelles activités** à l'ensemble actuel. D'autres domaines d'intérêt pour la bonne gouvernance de l'open source vont bientôt émerger au fur et à mesure que les professionnels commenceront à utiliser et à contribuer au corpus de connaissances.
* **Envisagez un processus simple** pour cloner et mettre en œuvre la méthode dans les organisations, avec une fonction de type installation. Il devrait être facile pour les personnes désireuses d'utiliser le corpus de connaissances de créer leur propre espace privé, avec tout ce qui est nécessaire pour trier les activités et définir les sprints, remplir les tableaux de bord et communiquer de manière transparente sur les progrès réalisés. Cela pourrait inclure le développement d'une application pour soutenir la mise en œuvre de la méthodologie.
* **Developper** des cas d'utilisation, des modèles, des témoignages alignés sur des scénarios typiques tels que PME, grande ville, université, etc.

Nous mettrons en place un forum sur OSPO.zone pour recueillir les réactions des utilisateurs, les idées, les discussions de soutien, etc.



## Contribuer

Nous sommes ouverts aux contributions, et toutes nos activités sont ouvertes et publiques. Si vous souhaitez participer, le meilleur moyen est de vous inscrire sur la liste de diffusion et de rejoindre la conversation : <http://mail.ow2.org/wws/subscribe/ossgovernance>.

L'initiative de bonne gouvernance est un groupe de travail hébergé sur la forge OW2 :

* Le [centre de ressources GGI](https://www.ow2.org/view/OSS_Governance/) contient de nombreuses ressources et informations depuis le début de l'initiative.
* Les activités sont [élaborées et examinées en tant que problèmes](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/449) sur l'instance GitLab.
* Les discussions ont lieu sur [la liste de diffusion publique GGI](https://mail.ow2.org/wws/info/ossgovernance), anet nous organisons régulièrement des réunions. Les procès-verbaux sont disponibles sur la liste de diffusion et les réunions sont ouvertes à tous.

Pour contribuer au centre de ressources GGI et aux activités de GitLab, veuillez suivre les instructions suivantes :

* Créez votre compte utilisateur OW2 à l'adresse https://www.ow2.org/view/services/registration
* Connectez-vous une fois à l'adresse https://www.ow2.org/
* Pour éditer [le Centre de Ressources](https://www.ow2.org/view/OSS_Governance/) : envoyez-nous votre nom d'utilisateur, et nous vous accorderons l'accès approprié.
* Pour accéder au groupe GitLab : connectez-vous une fois à https://gitlab.ow2.org avec vos identifiants OW2, puis faites-nous savoir quand c'est fait, et nous vous accorderons l'accès au groupe GGI dans GitLab.
* Pour l'accès à Rocket.Chat, vous devez :
  - Vous connecter au moins une fois sur https://gitlab.ow2.org en utilisant vos informations d'identification OW2.
  - Ouvrez Rocket.Chat et choisissez un nom d'utilisateur Rocket.Chat lorsque vous y êtes invité.
  - Allez sur le canal #general et demandez l'accès au canal GGI.
  - Une fois l'accès accordé, vous devriez pouvoir accéder au canal #good-governance.


## Contact

ThLa meilleure façon d'entrer en contact avec l'initiative de bonne gouvernance d'OW2 est de s'inscrire sur la liste de diffusion à l'adresse <http://mail.ow2.org/wws/subscribe/ossgovernance>.

Pour les questions administratives, vous pouvez joindre l'initiative GGI à l'adresse  https://www.ow2.org/view/About/Management_Office.


# Annexe : Modèle de fiche d'activité personnalisée

La dernière version du modèle de tableau de bord personnalisé des activités est disponible dans la section "ressources" du [GitLab de l'initiative de bonne gouvernance](https://gitlab.ow2.org/ggi/ggi) d'OW2.







