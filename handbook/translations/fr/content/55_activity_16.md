## Mettre en place une stratégie de gouvernance des logiciels open source d'entreprise

Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/16>.

### Description 

Définir une stratégie de gouvernance des logiciels open source au sein de l'entreprise garantit des approches cohérentes et visibles à la fois pour une utilisation interne et pour des contributions et des implications externes. Une vision et une direction claires et bien établies rendent la communication de l'entreprise plus efficace.

Passer à l'open source procure de nombreux avantages, mais impose aussi certains devoirs et un changement de culture dans l'entreprise. Cela peut avoir un impact sur les modèles commerciaux et influencer la manière dont une organisation présente sa valeur et son offre, ainsi que sa position vis-à-vis des clients et des concurrents. 

Cette activité comprend les tâches suivantes : 

* Mettre en place un responsable du logiciel open source, avec le parrainage et le soutien de la direction générale.
* Établir et publier une feuille de route claire pour l'open source, avec des objectifs précis et des avantages attendus.
* S’assurer que tous les cadres supérieurs en prennent connaissance et agissent conformément à cette feuille de route.
* Promouvoir les logiciels open source au sein de l'entreprise : encourager leur usage, les initiatives internes et le niveau de connaissances.
* Promouvoir le logiciel libre à l'extérieur de l'entreprise : par des déclarations et une communication officielles, et par une participation visible aux initiatives.

Définir, publier et appliquer une stratégie claire et cohérente favorise également l'adhésion de toutes les personnes au sein de l'entreprise et facilite les initiatives des équipes. 

### Opportunités à déterminer 

Il est temps de travailler sur cette activité si :

* Il n'y a pas d'effort coordonné de la part de la direction, et l'open source est encore considéré comme une solution ponctuelle.
* Il existe déjà des initiatives internes, mais elles ne remontent pas jusqu'aux niveaux supérieurs de la direction.
* L'initiative a été lancée il y a un certain temps mais elle se heurte à de nombreux obstacles et ne donne toujours pas les résultats attendus.

### Progrès à évaluer 

Les **points de vérification** suivants démontrent les progrès réalisés dans cette activité :
- [ ] Il existe une charte de gouvernance open source claire pour l'entreprise. La charte doit contenir :
  * les objectifs à atteindre,
  * à qui s’adresse-t’elle,
  * le pouvoir du ou des stratèges et ses limites.
- [ ] Une feuille de route open source est largement disponible et acceptée dans toute l'entreprise.

### Recommandations 

* Mettre en place un groupe de personnes et des processus pour définir et surveiller la gouvernance de l'open source au sein de l'entreprise.
* S'assurer que la direction s'engage clairement dans les initiatives open source.
* Communiquer sur la stratégie open source au sein de l'organisation, en faire une préoccupation majeure et un véritable engagement de l'entreprise.
* Veillez à ce que la feuille de route et la stratégie soient bien comprises par tout le monde, des équipes de développement aux gestionnaires et techniciens d’infrastructure. 
* Communiquez sur ses progrès, afin que les gens sachent où en est l'organisation par rapport à son engagement. Publiez régulièrement des mises à jour et des indicateurs.

### Ressources 

* [Liste de vérifications et références pour une gouvernance open source](https://opengovernance.dev/).
* [L'open source comme enjeu de souveraineté numérique, par Cédric Thomas, OW2 CEO, Workshop at Orange Labs, Paris, January 28, 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (french only).
* [Une série de guides pour gérer les logiciels open source dans l'entreprise, par la Fondation Linux](https://todogroup.org/guides/).
* [Un bel exemple de document stratégique sur les sources ouvertes, par le groupe LF Energy](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)
