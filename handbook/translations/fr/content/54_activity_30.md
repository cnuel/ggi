## Soutenir les communautés open source

Lien vers GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/30>.

### Description 

Cette activité consiste à s'engager auprès des représentants institutionnels du monde de l'open source. 

Elle est réalisée par :
* L’adhésion aux fondations OSS (y compris le coût financier de l'adhésion).
* Le soutien et la défense des activités des fondations.

Cette activité implique d'allouer aux équipes de développement et d'informatique du temps et du budget pour participer aux communautés open source.

### Évaluation de l'opportunité 

Les communautés open source sont à l'avant-garde de l'évolution de l'écosystème open source. S'engager dans les communautés présente plusieurs avantages : 
* cela permet de rester informé et à jour,
* le profil de l'organisation est amélioré,
* l'adhésion inclue des bénéfices,
* elle fournit une structure et une motivation supplémentaires à l'équipe informatique open source.

Les coûts comprennent :
* les frais d'adhésion,
* le temps de personnel et un budget de voyage alloué pour participer aux activités de la communauté,
* le suivi de l'engagement IP.

### Évaluation des progrès 

Les **points de vérification** suivants démontrent l'avancement de cette activité :
- [ ] L'organisation est un membre signataire d'une fondation open source.
- [ ] L'organisation participe à la gouvernance.
- [ ] Les logiciels développés par l'organisation sont soumis à la base de code d'une fondation ou ont été ajoutés à celle-ci.
- [ ] L'adhésion est reconnue sur les sites web de l'organisation et de la communauté.
- [ ] Une évaluation des coûts/avantages de l'adhésion a été réalisée.
- [ ] Un point de contact pour la communauté a été désigné.

### Recommandations 

* Rejoignez une communauté compatible avec votre taille et vos ressources, c'est-à-dire une communauté qui peut entendre votre voix et où vous pouvez être un contributeur reconnu.

### Ressources 

* Consultez cette [page utile (en anglais)] (https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) de la Fondation Linux sur le pourquoi et le comment de l'adhésion à une communauté open source.


