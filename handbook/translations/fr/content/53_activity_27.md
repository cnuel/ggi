## Belong to the open source community

Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/27>.

### Description 

This activity is about developing among developers a feeling of belonging to the greater open source community. As with every community, people and entities have to participate and contribute back to the whole. It reinforces the links between practitioners and brings sustainability and activity to the ecosystem. On a more technical side, it allows choosing the priorities and roadmap of projects, improves the level of general knowledge and technical awareness. 

This activity covers the following:
* **Identify events** worth attending. Connecting people, learning about new technologies and building a network are key factors to get the full benefits of open-source. 
* Consider **foundation memberships**. Open-source foundations and organizations are a key component of the open-source ecosystem. They provide technical and organizational resources to projects, and are a good neutral place for sponsors to discuss common issues and solutions, or work on standards.
* Watch **working groups**. Working groups are neutral collaborative workspace where experts interact on a specific domain like IoT, modelling or science. They are a very efficient and cost-effective mechanism to tackle common, albeit domain-specific concerns together.
* **Budget participation**. At the end of the journey, money is the enabler. Plan required expenses, allow people paid time for these activities, anticipate next moves, so the program doesn't have to stop after a few months short of funding.

### Opportunity Assessment 

Open source works best when done in relation with the open source community at large. It facilitates bug fixing, solution sharing, etc. 

It is also a good way for companies to show their support to open-source values. Communicating about the company's involvement is important both for the company's reputation and for the open-source ecosystem.

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] A list of events people could attend is drafted.
- [ ] There is a monitoring of public talks given by team members.
- [ ] People can submit events participation requests.
- [ ] People can submit projects for sponsorship.

### Recommendations 

* Survey people to get to know what events they like or would be the most beneficial for their work.
* Set up in-house communications (newsletter, resource centre, invitations…) so people know about the initiatives and can participate.
* Make sure that these initiatives can benefit various types of people (developers, administrators, support…), not only C-level executives. 

### Resources 

* [What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) An article by Michael Sweeney on clearcode.cc.
* [Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) An article by Velichka Atanasova from VMWare.
* [Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) A good read by Robert Kowalski from CloudBees.
* [7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) An article from Simon Phipps for InfoWorld.
* [Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) An article by Donna Benjamin from RedHat.
