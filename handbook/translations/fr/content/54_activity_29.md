
## S'engager dans des projets open source

Lien vers GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/29>.

### Description 

Cette activité consiste à engager des contributions significatives à certains projets OSS qui sont importants pour vous. Les contributions sont mises à l'échelle et engagées au niveau de l'organisation (et non au niveau personnel comme dans #26). Elles peuvent prendre plusieurs formes, du financement direct à l'allocation de ressources (personnes, serveurs, infrastructure, communication, etc.), pour autant qu'elles bénéficient au projet ou à l'écosystème de manière durable et efficace. 

Cette activité est une continuité de l'activité #26 et amène les contributions aux projets open source au niveau de l'organisation, les rendant plus visibles, puissantes et bénéfiques. Dans cette activité, les contributions sont censées apporter une amélioration substantielle et à long terme au projet OSS : par exemple, un développeur ou une équipe qui développe une nouvelle fonctionnalité très désirée, des actifs d'infrastructure, des serveurs pour un nouveau service, la reprise de la maintenance d'une branche largement utilisée.

L'idée est de réserver un pourcentage des ressources pour parrainer des développeurs de logiciels libres qui écrivent et maintiennent des bibliothèques ou des projets que nous utilisons. 

Cette activité implique d'avoir une cartographie des logiciels open source utilisés, et une évaluation de leur criticité pour décider lequel soutenir.

### Évaluation de l'opportunité 

> Si chaque entreprise utilisant des logiciels libres contribuait au moins un peu, nous aurions un écosystème sain. https://news.ycombinator.com/item?id=25432248

Soutenir des projets permet d'assurer leur pérennité et de fournir un accès à l'information, voire même d'influencer et de prioriser certains développements (bien que cela ne doive pas être la raison principale du soutien aux projets).

Avantages potentiels de cette activité : garantir que les rapports de bogues sont traités en priorité et que les développements sont intégrés dans la version stable.
Coûts potentiels associés à cette activité : consacrer du temps aux projets, engagement financier.

### Évaluation de l'avancement 

Les **points de vérification** suivants démontrent l'avancement de cette activité :
- [ ] Projet bénéficiaire identifié.
- [ ] Option de soutien décidée, telle qu'une contribution monétaire directe ou une contribution au code.
- [ ] Nomination d'un chef de projet.
- [ ] Une contribution a été apportée.
- [ ] Le résultat de la contribution a été évalué.

Points de vérification empruntés au questionnaire d'OpenChain [auto-certification] (https://certification.openchainproject.org/) :
- [ ] Nous avons une politique de contribution aux projets open source au nom de l'organisation.
- [ ] Nous disposons d'une procédure documentée régissant les contributions aux projets open source.
- [ ] Nous disposons d'une procédure documentée pour sensibiliser tout le personnel du logiciel à la politique de contribution aux projets open source.

### Outils 

Certaines organisations offrent des mécanismes de financement de projets open source (cela pourrait être pratique si votre projet cible se trouve dans leurs portefeuilles).
* [Open Collective](https://opencollective.com/).
* [Software Freedom Conservancy](https://sfconservancy.org/).
* [Tidelift](https://tidelift.com/).

### Recommandations 

* Concentrez-vous sur les projets qui sont essentiels pour l'organisation : ce sont les projets que vous souhaitez le plus aider avec vos contributions.
* Ciblez les projets communautaires.
* Cette activité nécessite une connaissance minimale du projet cible.

### Ressources (en anglais)

* [Comment soutenir les projets open source maintenant] (https://sourceforge.net/blog/support-open-source-projects-now/) :  Une courte page avec des idées sur le financement des projets open source.
* [Sustain OSS : un espace pour les conversations sur le maintien de l'open source] (https://sustainoss.org)
