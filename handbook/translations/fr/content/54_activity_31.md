## Affirmer publiquement l'utilisation de l'open source

Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/31>.

### Description 

Cette activité consiste à reconnaître l'utilisation de logiciels libres dans un système d'information, dans des applications et dans de nouveaux produits.
* Fournir des exemples de réussite.
* Présenter à des événements.
* Financer la participation à des événements.

### Évaluation de l'opportunité 

Il est maintenant généralement admis que la plupart des systèmes d'information fonctionnent avec des logiciels libres et que les nouvelles applications sont pour la plupart réalisées en réutilisant des logiciels libres. 

Le principal avantage de cette activité est de créer un terrain de jeu équitable entre le logiciel libre et les logiciels propriétaires, de s'assurer que les logiciels libres reçoivent la même attention et sont gérés de manière aussi professionnelle que les logiciels propriétaires. 

Un autre avantage est qu'elle contribue grandement à améliorer le profil de l'écosystème open source et, puisque les utilisateurs d'open source sont identifiés comme des "innovateurs", elle renforce également l'attractivité de l'organisation.

### Évaluation des progrès 

Les **points de vérification** suivants démontrent les progrès réalisés dans cette activité :
- [ ] Les fournisseurs commerciaux de logiciels libres ont l'autorisation d'utiliser le nom de l'organisation comme référence client.
- [ ] Les contributeurs sont autorisés à le faire et à s'exprimer sous le nom de l'organisation.
- [ ] L'utilisation des logiciels libres est ouvertement mentionnée dans le rapport annuel du département informatique.
- [ ] Il n'y a aucun obstacle à ce que l'organisme explique son utilisation des logiciels open source dans les médias (interviews, événements open source et industriels, etc.).

### Recommandations 

* L'objectif de cette activité n'est pas que l'organisation devienne un organisme d'activisme OSS, mais de s'assurer qu'il n'y a aucun obstacle à ce que le public reconnaisse son utilisation des OSS.

### Ressources 

* Exemple du [CERN] (https://superuser.openstack.org/articles/cern-openstack-update/) affirmant publiquement son utilisation d'OpenStack.


