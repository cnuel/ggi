## Inventory of open source skills and resources

Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/17>.

### Description 

At any stage, from a management perspective, it is useful to have a mapping, an inventory of open source resources, assets, usage and their status, as well as potential needs and available solutions. It also includes assessing the required effort and skills to fill the gap. 

This activity aims to take a snapshot of the open source situation within the organisation and on the market and evaluate the bridge between them. 

* Inventory of OSS usage in the software development chain as well as in the software products and components used in production.
* Identify open source technologies (solutions, frameworks, innovative features) that could fit your needs and help improve your process.

Not included
* Identify and qualify related OSS ecosystems and communities. (Culture Goal)
* Identify dependencies on OSS libraries and components. (Trust Goal)
* Identify the technical (e.g. languages, frameworks..) and soft (e.g. collaboration, communication) skills needed. (belongs to next Activities: OSS competency growth  and Open source software development skills)

### Opportunity Assessment 

An inventory of available open source resources that will help optimise investment and prioritise skills development. 

This activity creates the conditions for improving development productivity given the efficiency and popularity of OSS components, development principles and tools, particularly in the development of modern applications and infrastructures.
- This may require simplifying the portfolio of OSS resources. 
- This may require retraining personnel.
- This enables the identification of needs and feeds your IT roadmap.

### Progress Assessment  

The following **verification points** demonstrate progress in this Activity:
- [ ] There is a workable list of OSS resources "We use", "We integrate", "We produce", "We host", and the related Skills
- [ ] We are on a path to improve efficiency by using state of the art methods and tools.
- [ ] We have identified OSS resources unaccounted for until now (that may have been creeping in, and do we have elements to define policy in this domain?)
- [ ] We request new projects to endorse or reuse existing OSS resources. (Culture Goal?)
- [ ] We have a reasonably safe perception and understanding of the scope of OSS usage in our organisation.


### Tools 

There are many different ways to establish such inventory. 
One way would be to classify OSS resources into four categories: 
- OSS we use: software we use either in production or in development
- OSS we integrate: for example, OSS libraries we integrate into a custom-made application
- OSS we produce: for example, a library we have published on GitHub or an OSS project we develop or regularly contribute to.
- OSS we host: OSS we run to offer an in-house service such as a CRM, GitLab, nexus, etc.
  An example table would look like the following:

| We use | We integrate | We produce | We host | Skills |
| ------ | ------------ | ---------- | ------- | ------ |
| Firefox, <br />OpenOffice, <br />Postgresql | Library slf4j | Library YY on GH | GitLab, <br />Nexus | Java, <br />Python |

The same identification should apply to skills
- Skills & experiences available through the existing teams
- Skills & experiences that could be developed or acquired internally (training, coaching, experiment)
- Skills & experiences that need to be sought out on the market or through partnership / contracting

### Recommendations 

* Keep things simple. 
* It's a relatively high-level exercise, not a detailed inventory for the accounting department.
* While this activity is a good starting point, you do not need to have it 100% completed before launching other activities.
* Handle issues, resources and skills related to **software development** in Activity #42.
* The inventory should cover all IT categories: operating systems, middlewares, DBMS, system administration, development and testing tools, etc.
* Start identifying related communities: it's easier to get support and feedback from the project when they already know you.

### Resources 

* An excellent course on [Free (/Libre), and Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), by Professor Dirk Riehle.
