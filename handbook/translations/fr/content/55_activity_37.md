## l’Open Source au service de la transformation numérique


Lien à ce sujet sur GitLab : <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/37>.

### Description 

> "La transformation numérique consiste à adopter les technologies numériques pour transformer les services ou l’entreprise, en remplaçant ses processus manuels par des processus numériques ou en remplaçant une ancienne technologie numérique par une plus récente." (source : Wikipedia)

Lorsque les organisations les plus avancées dans la transformation numérique pilotent conjointement le changement de leur activités métiers, informatiques et financières pour ancrer le numérique dans leur évolution, elles reconsidèrent leur :
- Modèle économique : la chaîne de valeurs et ses écosystèmes, les solutions SaaS, as a service.
- Finance : opex/capex, personnel, externalisation.
- Informatique : innovation, modernisation des actifs en place et de l’héritage.

L'Open Source est au cœur de la transformation numérique :
- Technologies, pratiques agiles et gestion des produits.
- Ressources humaines : collaboration, communication ouverte, cycle de développement/décision.
- Modèles économiques : essayer avant d’acheter, innovation ouverte.

En termes de compétitivité, les processus les plus visibles sont probablement ceux qui ont un impact direct sur l'expérience client. Et nous devons reconnaître que les grands acteurs, ainsi que les start-ups, ont radicalement changé les attentes des clients en offrant une expérience client inédite.

L'expérience client, comme tous les autres processus de l’entreprise, dépendent entièrement de l'informatique. Chaque entreprise doit transformer son informatique, c'est l'objet de la transformation numérique. Les entreprises qui ne l'ont pas encore fait doivent maintenant réaliser leur transformation numérique le plus rapidement possible, sinon elles risquent d'être rayées du marché. La transformation numérique est une condition de survie. Les enjeux étant très importants, une entreprise ne peut pas entièrement confier sa transformation numérique à un fournisseur. Chaque entreprise doit mettre la main à la pâte avec son informatique, ce qui signifie que chaque entreprise doit mettre la main à la pâte avec les logiciels libres, car il n'y a pas d'informatique sans logiciels libres. Les avantages attendus de la transformation numérique sont les suivants :
* Simplifier, automatiser les processus de base, les rendre temps réel.
* Permettre des réponses rapides aux changements de la concurrence.
* Tirer parti de l'intelligence artificielle et du big data.


### Opportunités à déterminer 

La transformation numérique pourrait être gérée par :
* Un segment informatique : l’IT de production, l’IT au service d’activités (CRM, facturation, approvisionnement...), l’IT orientée métier (RH, finance, comptabilité...), le Big Data.
* Plusieurs technologies ou processus soutenant l'IT : l’infrastructure (cloud), l’IA (Intelligence Artificielle, les processus (Make-or-Buy, DevSecOps, SaaS).

Injecter de l'open source dans un segment ou une technologie particulière de votre informatique révèle que vous voulez garder la main dessus, car vous avez évalué qu’il est important pour la compétitivité de votre entreprise. Il importe d'évaluer la position de votre entreprise par rapport à vos concurrents, mais aussi par rapport à d'autres industries et aux acteurs clés en termes d'expérience client et de solutions de marché.


### Évaluation des progrès 

1. Niveau 1 : Évaluation de la situation
* J'ai identifié :
  - les segments de l'informatique qui sont importants pour la compétitivité de mon entreprise, et
  - les technologies open source nécessaires pour développer des applications dans ces segments.
  Et j'ai donc décidé :
  - sur quels segments je veux gérer en interne le développement de projets, et
  - sur quelles technologies open source je dois développer une expertise en interne.

2. Niveau 2 : Engagement
* Sur certaines technologies open source utilisées dans l'entreprise, plusieurs développeurs ont été formés et sont reconnus comme des contributeurs de valeur par la communauté open source. Dans certains segments sélectionnés, des projets fondés sur des technologies open source ont été lancés.

3. Level 3: Généralisation
* Pour tous les projets, une alternative open source est systématiquement étudiée dès la phase de lancement du projet. Pour faciliter l'étude de cette alternative open source par l'équipe de projet, un budget central et une équipe centrale d'architectes, hébergés au sein du département informatique, sont consacrés à l'assistance aux projets.  

**Les KPIs**:
* KPI 1. Ratio pour lequel une alternative open source a été étudiée : (nombre de projets / nombre total de projets).
* KPI 2. Ratio pour lequel l'alternative open source a été choisie : (nombre de projets / nombre total de projets).


### Recommandations 

Au-delà d'un titre, la transformation numérique est un état d'esprit qui implique des changements fondamentaux, qui devraient aussi (ou même principalement) venir des strates supérieures de l'organisation. La direction doit promouvoir les initiatives et les nouvelles idées, gérer les risques et éventuellement mettre à jour les procédures existantes pour les adapter aux nouveaux concepts.

La passion est un énorme facteur de réussite. L'un des moyens développés par les acteurs clés dans ce domaine est de créer des espaces ouverts aux nouvelles idées, où les gens peuvent soumettre et travailler librement sur leurs idées concernant la transformation numérique. La direction doit encourager de telles initiatives.


### Ressources 

* [Fondation Eclipse : Favoriser la transformation numérique en Europe grâce à la collaboration mondiale en matière de logiciels libres](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
* [Europe : stratégie pour les logiciels Open Source](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
* [Europe : stratégie 2020-2023 pour les logiciels Open Source](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
