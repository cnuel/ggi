## HR perspective

Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/28>.

### Description 

Switching to open source culture has deep HR impacts: 
* **New processes and contracts**: Contracts have to be adapted to allow and promote external contributions. This includes IP and licensing issues for work done within the company, but also the ability for the employee or contractor to have their own projects. 
* **Different types of people**: People working with open source often have different incentives and mindsets than with pure proprietary, corporate people. Processes and mindsets need to adapt to this community reputation-oriented paradigm, in order to attract new types of talent and retain them along.
* **Career development**: need to offer a career path that nurtures and values employees for their technical and soft skills as well as the competencies expected by your organisation (collaboration to drive community efforts, communication to act as spokesperson for your company, etc.). By all means, HR has a key role in enabling open source as a cultural goal.

**Workforce**

For a developer who has been working on the same proprietary solution for a long time, switching to open source may seem quite a change, and require adaptation. But for most developers, open source software only brings benefits.  

Developers getting out of school or university today have all always been working on open source.  Within a company, the large majority of developers are using open source languages and importing open source libraries or snippets every day.  It is indeed much easier to paste lines of open source code in a program than to trigger the internal sourcing process, which escalates through multiple validations through the management line.

Open source makes the job of the developer more interesting because with open source, a developer is always on the lookout to find out what their peers outside the company have been inventing, and therefore remains on the cutting edge of the technology.

For an organisation, there needs to be an HR strategy to 1/ skill or re-skill the existing workforce 2/ reflect and position the company on hiring new talents hence what is the attractiveness of the company when it comes to open source.

> Getting people with a good FLOSS mindset, who already understand the code, and know how to work well with others is wonderful. The alternative of evangelizing / training / interning is worth doing but more expensive & time consuming.
>
> &mdash; <cite>OSS Software Vendor CEO</cite>

This illustrates that hiring people with open source DNA is an acceleration path to consider in HR strategy. 

#### Processes

- Establish or revisit job descriptions (technical skills, soft skills, competencies and experiences)
- Training programs: self-training, formal training, management coaching, peer mapping, communities
- Establish or revisit career path: competencies, key results/impact and career steps


### Opportunity Assessment 

1. Frame development practices: the problem is probably not so much to spur developers to use more open source, but rather to make sure that they use it safely, in compliance with the licensing terms of each open source technology, and without abandoning traditional security checks (open source lines of code could contain malicious codes),
2. Revisit collaboration practices: with development practices, the opportunity is to extend the agility and collaboration to other lines of business in your organisation. Inner sourcing is often used to foster these behaviours, though this might be half of the way to open source culture,
3. Organisation's culture: in the end, this is all about your organisation's culture: open source can be the flagship for values such as openness, collaboration, ethics, sustainability.


### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] Training is available for presenting both the benefits and the constraints (Intellectual Property licensing terms compliance) related to open source.
- [ ] Every developer, every architect, every project leader (or Product Owner/Business Owner), understands the benefits and the constraints (Intellectual Property licensing terms compliance) related to open source.
- [ ] Developers are encouraged to contribute to open source communities, and take responsibility for them, and could receive adequate training to do it.
- [ ] Skills and competencies are reflected in organisation job descriptions and career steps.
- [ ] The experience that developers acquired in open source (contributions to open source communities, participation in the internal compliance process, external spoke persons for the company...) is taken into account in the HR evaluation process.


### Tools 

* Skills matrix.
* Public training programs (ex. open source school).
* Sourcing: GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita ...
* Contract templates (Loyalty clause).
* Job descriptions (templates) & career steps (templates).


### Recommendations 

Most of the time nowadays, developers already know some open source principles and are willing to work with, and on, open source software. However, there are still a few actions that the management should take:
* Preference for OSS experience in hiring, even though the job for which the developer is being hired only relates to proprietary technology. Chances are, with the digital transformation, that the developer will someday have to work on open source.
* OSS training program: Every developer, every architect, every project leader (or Product Owner/Business Owner), should have access to training resources (videos or face-to-face training) that present the benefits of open source and also the constraints in terms of Intellectual Property and licensing compliance.
* Training should be made available for developers who want to contribute to open source communities and be part of the governance bodies of these communities (Linux certifications).
* Recognition in the HR personal assessment processes of the contribution of the employee (developer or architect) to open source related topics such as contributions to open source communities and compliance with Intellectual Property licensing terms. Most topics are shared and fit into technical career paths, whereas some might or should be specific.
* Best kept secret and company posture: need to address the communication aspects (how core this is to your organisation that it might be reflected in your annual report), how does it impact your communication posture (an open source contributor could be a spoke person for your company including press contacts).


### Resources 

* Regarding the ability of people to speak outside the company during events, please see Activity 31: "(Engagement Goal) Publicly asserting the use of open source".
