## Open source supervision

Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/19>.

### Description 

This activity is about controlling the use of open source and ensuring open source software is proactively managed. This concerns several perspectives, be it to use OSS tools and business solutions, or to include OSS as components in own developments or modify a version of a software adapting it to own needs, etc. It is also about identifying areas where open source has become a (sometimes covert) de facto solution and assessing its suitability. 

It may be necessary to clarify the following:
* Is the required functionality provided?
* Is there additional functionality provided that is not needed but is increasing complexity in the BUILD and RUN phases?
* What does the license require, what are the legal constraints?
* How much does the decision make your organisation supplier-independent?
* Does a support option, ready for your business needs, exist, and how much does it cost?
* TCO (Total Cost of Ownership).
* Does the management know about open source's advantages, e.g. beyond "saving licence cost"? Being comfortable with open source helps get the maximum benefit from working with project communities and vendors.
* See if it makes sense to share development costs by giving one's developments to the community and all its implications, like license compliance.
* Check for availability of community support or professional support.


### Opportunity Assessment 

Defining a decision process specifically directed at open source is a way to maximise its benefits.
* It avoids the uncontrolled creeping usage and hidden costs of OSS technologies. 
* It leads to informed and OSS-aware strategic and organisational decisions. 

Costs: the activity may challenge and reconsider sub-optimal de facto use of open source as inefficient, risky, etc.

### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] OSS has become a comfortable option when selecting OSS is not seen as an exception or a dangerous choice.
- [ ] OSS has become a "mainstream" option.
- [ ] Key players are sufficiently convinced the open source solution has strategical advantages worth investing in.
- [ ]  It can be demonstrated the TCO of the solution based on open source gives your organisation a higher value than the alternative.
- [ ] There is an evaluation of how supplier independence saves money or potentially can save money in the future.
- [ ] There is an evaluation that solution independence reduces risks to be too expensive to change the solution (no closed data formats possible).

### Tools 

At this stage, we cannot think of any tool relevant or concerned by this activity.


### Recommendations 

* Proactively managing the use of open-source requires basic levels of awareness and understanding of open source fundamentals because they should be considered in any OSS decision. 
* Compare the needed functionality instead of looking for an alternative for a known closed source solution.
* Make sure to have support and further development.
* Regard the effects of the solution's license on your organisation.
* Convince all key players about the value of the advantages of open source, beyond "saving license cost".
* Be honest, do not exaggerate the open source solution's effect.
* In the decision-making process it is equally important to assess different open source solutions in order to avoid disappointment through wrong expectations, to make it clear what the organisation is required to do and all the advantages the openness of the solutions brings. This must be identified so the organisation can assess it for its own context.

### Resources 

* [Top 5 Benefits of Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Sponsored blog, but still interesting, quick read.
* [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): an IBM-sponsored look at OSS support costs.
