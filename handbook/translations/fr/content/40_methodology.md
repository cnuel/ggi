# Methodology

Implementing the OSS Good Governance methodology is ultimately a consequential and impactful initiative. It involves several categories of company people, services, and processes, from everyday practices to HR management and from developers to C-level executives. There really is no silver bullet mechanism to implement open source good governance. Different types of organisation and company cultures and situation will demand different approaches to open source governance. For each organisation, there will be different constraints and expectations, leading to different paths and ways of managing the programme.

With this in mind the Good Governance Initiative provides a generic blueprint of activities that can be tailored to an organisation's own domain, culture and requirements. While the blueprint claims to be comprehensive, the methodology can be implemented progressively. It is possible to bootstrap the program by simply selecting the most relevant Goals and Activities in one's specific context. The idea is to build a first-draft roadmap to help set up the local initiative.

Besides this framework, we also highly recommend getting in touch with peers through an established network like the European [OSPO Alliance](https://ospo.zone) initiative, or other like-minded initiatives from the TODO group or OSPO++. What is important is to be able to exchange with people running a similar initiative, and share the issues encountered and the solutions that exist.

## Setting the Stage

Given the ambition of the good governance methodology and its potentially broad impact, it is important to communicate with a variety of people within an organisation. It would be appropriate to onboard them to establish an initial set of realistic expectations and requirements to get off to a good start, attract interest and support. A good idea is to publish the Customised Activity Scorecards on the organisation's collaborative platform so they can be used to communicate with stakeholders. Some hints:
* Identify key stakeholders, make them agree on a set of primary objectives. Engage them in the success of the initiative as a part of their own agenda.
* Get initial buy-in, agree on the steps and pace, and set up regular checks to inform them of progress.
* Make sure they understand the benefits of what can be achieved and what it involves: expected improvement should be clear and outcome visible.
* Establish a first diagnostic or state of the art of open source in the candidate organisation.
  Outcome: a document describing what this program will achieve, where the organisation stands and where it aims to go.

## Using Customised Activity Scorecards
A Customised Activity Scorecard is a form describing a Canonical Activity customised to the specifics of an organisation. Put together, the deck of Customised Activity Scorecards provides the roadmap for managing open source software. 

`Please note, from early experience with the methodology, it takes up to one hour to adapt a Canonical Activity into an organisation's specific Customised Score Card.` 

The Customised Activity Scorecard contains the following sections:
* **Title Disambiguation** First of all take a few minutes to develop an understanding of what the Activity might be about and its relevance, how it can fit in your overall OSS management journey.
* **Customised Description** Adapt the Activity to the specifics of the organisation, scoping. Define the scope of the Activity, the particular use case you will address.
* **Opportunity Assessment** Explain why it is relevant to undertake this activity, what needs it addresses. What are our pain points? What are the opportunities for progressing? What can be gained? 
* **Objectives** Define a couple of crucial objectives for the Activity. Pain points to be fixed, progress opportunities, wishes. Identify key tasks. What we aim to achieve in this iteration.
* **Tools** Technologies, tools and products used in the Activity.
* **Operational Notes** Indications on approach, method, strategy to progress in this Activity.
* **Key Results** Define measurable, verifiable expected results. Choose results indicating progress with regard to the Objectives. Indicate KPIs here.
* **Progress and Score** Progress is, in %, the completion rate of the result; Score is the personal success rating.
* **Personal Assessment** For each result you can add a brief explanation and explain your personal satisfaction rate expressed in the Score.
* **Timeline** Indicate Start-End dates, Phasing tasks, Critical steps, Milestones.
* **Efforts** Evaluate requested time and material resources, internal and third-party. What are the efforts expected? How much will it cost? What resources do we need?
* **Assignees** Say who participates. Assign tasks or Activity leadership and responsibilities.
* **Issues** Identify key issues, foreseen difficulties, risks, roadblocks, uncertainties, points of attention, critical dependencies.
* **Status** Write here a synthetic assessment of how the Activity is doing: healthy? Delayed? Etc.
* **Overall Progress Rating** Your own high-level, management-oriented, synthetic Activity progress evaluation.


## Iterating

As modern software practitioners, we like agile-like methods that define small and safe increments, since it is good practice to reassess the situation regularly and to provide meaningful minimum intermediate results. 

In the context of a live OSPO program this is highly relevant, as many side aspects will change over time, from the organisation's strategy and response to open source to people's availability and engagement. Periodic reassessment and iteration also allows for adaptation to ongoing programme acceptance, better tracking of current trends and opportunities, and small incremental benefits to stakeholders and the organisation as a whole.

Ideally, the methodology could be implemented in five phases as follows:
1. **Discovery** Understanding key concepts, taking ownership of the methodology, aligning goals expectations.
1. **Customisation** Adapting Activity description and opportunity assessment to organisation specifics.
1. **Prioritisation** Identifying objectives and key results tasks and tools, scheduling milestones and drafting timeline.
1. **Activation** Finalising Scorecard, budget, assignments, document tasks on issue manager.
1. **Iteration** Assessing and scoring results, highlighting issues, improving, adjusting. Iterate every quarter or semester.

Preparing for the first program iteration:

* Identify a first set of tasks to work on, and prioritise them according to the needs (gaps to the desired state) and the timeline. Outcome: a list of tasks to work on during the iteration.
* Define a set of requirements and areas of improvement, communicate it to the stakeholders and end-users, and get their approval or commitment.
* Fill the scorecards for progress tracking. A template scorecard can be downloaded from the [GGI repository](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

At the end of each iteration, do a retrospective and prepare for the next iteration:
* Communicate about the latest improvements.
* Assess where you are, if the targeted tasks have been completed, refine the roadmap accordingly.
* Check the remaining pain points or issues, ask for support from other actors or services if needed.
* Re-prioritise tasks according to the updated context.
* Define a new subset of tasks to execute.


## Enjoy

Communicate on your success and enjoy the peace of mind of a state of the art open source strategy!

The OSS Good Governance is a method to deploy a continuous improvement program, and as such it never ends. Nevertheless, it's important to highlight intermediate steps and appreciate the changes it yields, to make progress visible and share the results. 

* Communicate with stakeholders and end-users to let them know the advantages and benefits that the initiative's effort brings.
* Foster sustainability of the program. Ensure that the best practices and lessons learned from the program are always applied and updated.
* Share your experience with your peers: provide feedback to the GGI working group and within your OSPO community of adoption, and share your approach.
