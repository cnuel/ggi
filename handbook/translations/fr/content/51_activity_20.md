## Open source enterprise software

Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/20>.

### Description 

This activity is about proactively selecting OSS solutions, either vendor or community-supported, in business-oriented areas. It may also cover defining preference policies for the selection of open source business application software.

While open source software is most often used by IT professionals -- operating system, middleware, DBMS, system administration, development tools -- it has yet to be recognized in areas where business professionals are the primary users. 

The activity concerns areas such as: Office suites, Collaboration environments, User management, Workflow management, Customer relationship management, Email, e-Commerce, etc.

### Opportunity Assessment 

As open source becomes mainstream it reaches out well beyond operating systems and development tools, it increasingly finds its way into the upper layers of the information systems, well into the business applications.
It is relevant to identify what OSS applications are successfully used to meet the needs of the organisation and how they can become an organisation's cost-saving preferred choice. 

The activity may bring some retraining and migration costs. 


### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] There is a list of recommended OSS solutions to address pending needs in business applications.
- [ ] A preference policy for the selection of open source business application software is drafted.
- [ ] Proprietary business applications in use are being evaluated against OSS equivalents.
- [ ] Procurement process and calls for proposals specify open source preference (if legally doable).

### Tools 

Tools to map out software and business applications?

At this stage, we cannot think of any tool relevant or concerned by this Activity.

### Recommendations 

* Talk to colleagues, learn from what other companies comparable to yours do.
* Visit local industry events to find out about OSS solutions and professional support.
* Try out community editions and community support first before committing to paid support plans.


### Resources 

* [What is enterprise open source?](https://www.redhat.com/en/blog/what-enterprise-open-source): a quick read about enterprise-ready open source.
* [101 Open Source Apps to Help your Business Thrive](https://digital.com/creating-an-llc/open-source-business/): An indicative list of business-oriented open source solutions.
