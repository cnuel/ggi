## S'engager auprès des fournisseurs de logiciels libres

Lien à ce sujet sur GitLab : ggi-castalia#33.

### Description
Obtenez des contrats avec les vendeurs de logiciels libres qui fournissent les logiciels essentiels pour vous. Les entreprises et entités qui produisent des logiciels open source doivent prospérer pour assurer la maintenance et le développement de nouvelles fonctionnalités. Leur expertise spécifique est requise sur le projet, et la communauté d'utilisateurs s'appuie sur leur activité et leurs contributions continues.

L'engagement avec les vendeurs de logiciels libres prend plusieurs formes :

* Souscrire à des plans d'assistance.
* Passer des contrats avec des sociétés de services locales.
* Sponsoriser des développements.
* Payer pour une licence commerciale.

Cette activité implique de considérer les projets open source comme des produits à part entière qui méritent d'être payés, tout comme les produits propriétaires, bien que généralement beaucoup moins chers.

### Évaluation de l'opportunité

L'objectif de cette activité est d'assurer un soutien professionnel aux logiciels libres utilisés dans l'organisation. Elle présente plusieurs avantages :

* Continuité du service grâce à des corrections de bogues en temps voulu.
* Performance du service grâce à une installation optimisée.
* Clarification du statut légal/commercial du logiciel utilisé.
* Accès rapide aux informations.
* Stabilité des prévisions budgétaires.

Le coût est évidemment celui des plans d'assistance choisis. Un autre coût pourrait être d'abandonner l'externalisation en masse auprès de grands intégrateurs de systèmes au profit d'une contractualisation fine avec des PME expertes.

### Évaluation des progrès

Les points de vérification suivants démontrent les progrès réalisés dans cette activité :

- [ ] Les logiciels libres utilisés dans l'organisation bénéficient d'un soutien commercial. 
- [ ] Des plans de support pour certains logiciels open source ont été contractés.
- [ ] Le coût des plans de support est une entrée légitime dans le budget informatique.

### Recommandations

* Dans la mesure du possible, trouvez des PME expertes locales.
* Méfiez-vous des grands intégrateurs de systèmes qui revendent l'expertise de tiers (en revendant des plans de support que les PME expertes en logiciels libres fournissent en réalité).


### Ressources

Quelques liens illustrant la réalité commerciale des logiciels libres :

* [Le point de vue d'un investisseur sur l'évolution des projets open source de la communauté vers l'entreprise.](https://a16z.com/2019/10/04/commercializing-open-source/).
* [Une lecture rapide pour comprendre l'open source commercial.](https://www.webiny.com/blog/what-is-commercial-open-source).


