# Contributing to the translations

---

## Workflow

Make sure all the required software for each script from the handbook/scripts subfolder you intend to run is installed. 

* System packages: `gettext`, `imagemagick`, `translation toolkit`, `LibreOffice`, `po4a`.
* Python/pip packages: `mdpo`.

On Debian, one needs to install the following system packages: `gettext`, `po4a`, `imagemagick`, `libreoffice-common` and `translate-toolkit`.

Depending on the translated languages, one will also need to install hyphenation files for LaTeX. On Debian they can be found under the names: `texlive-lang-<lang>`, e.g. `texlive-lang-german` or `texlive-lang-french`.

This process has been tested under Debian and Ubuntu systems.

### Add a new language to translate

#### <a name="AddLangMarkdown"></a> Markdown files

1. Open the markdown2po.sh and po2markdown.sh scripts from the handbook/scripts subfolder.
1. In the line `declare -a langs=("`, before the closing parenthesis ) add a blank space followed by the new 2 letter language code between 2 single quotes ', e.g 'pt'.
1. Do [Update translation files](#UpdTranslat).

#### Image files

1. Make sure the file structure for the language exists. If not, perform the steps in [Adding a language to markdown files](#AddLangMarkdown)
1. Copy cover.pot and maslow.pot from handbook/resources/pot_files/resources/ to handbook/translations/*lang code*/po_files/resources/, renaming the extensions from .pot to .po .
1. Start translating the .po files.

#### LaTex files

1. Make sure the file structure for the language exists. If not, perform the steps in [Adding a language to markdown files](#AddLangMarkdown)
1. Open the latexPo4a.cfg configuration file in the handbook/scripts subfolder.
1. Append a blank space and the new 2 letter language code to the \[po4a_langs\] line.

### <a name="UpdTranslat"></a> Update translation files

1. Cd into the handbook/scripts subfolder.
1. Run `./markdown2po.sh`.
1. Start translating the .po files.

#### LaTex files

1. Cd into the handbook/scripts subfolder.
1. Run `./latexUpdate2po.sh`, the command both updates the strings to be translated from the source and tries to create the translated target file in one go.

#### OpenDocument Text files

1. Make sure the file structure for the language exists. If not, perform the steps in [Adding a language to markdown files](#AddLangMarkdown)
1. Cd into the handbook/scripts subfolder and run `./scorecard2po.sh`

### Create translated markdown files

#### Markdown files

1. Cd into the handbook/scripts subfolder.
1. Run `./po2markdown.sh`.

#### Image files

1. Cd into the handbook/scripts subfolder.
1. For the cover page run `./tsltCvrImg.sh`.
1. For the Maslow image run `./tsltNtroImg.sh`.

#### LaTex files

1. Cd into the handbook/scripts subfolder.
1. Run `./latexUpdate2po.sh`, the command both updates the strings to be translated from the source and tries to create the translated target file in one go.

#### OpenDocument Text files

1. Cd into the handbook/scripts subfolder.
1. Run `./po2scorecard.sh`, the command creates translated ODT and PDF files in one go.

## General translation rules

- Use a language the audience is used to and try to avoid anglicisms and latinisms to increase readability.
- Build a commonly accepted glossary the other translators orient on to use the same words for the same purpose and meaning. 
- Write positively, try to avoid negations, they are less easy to understand.

