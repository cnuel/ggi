#
# Silvério Santos <SSantos@web.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2022-07-05 12:53+0200\n"
"Last-Translator: Silvério Santos <SSantos@web.de>\n"
"Language-Team: German <ossgovernance@ow2.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../content/53_activity_39.md:block 1 (header)
msgid "Upstream first"
msgstr "Upstream first"

#: ../content/53_activity_39.md:block 2 (paragraph)
msgid ""
"Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/39>."
msgstr ""
"Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/"
"issues/39>."

#: ../content/53_activity_39.md:block 3 (header)
msgid "Description"
msgstr "Beschreibung"

#: ../content/53_activity_39.md:block 4 (paragraph)
msgid ""
"This activity is concerned with developing awareness with regard to the "
"benefits of contributing back and enforcing the upstream first principle."
msgstr ""
"Bei dieser Aktivität geht es um die Entwicklung eines Bewusstseins für die "
"Vorteile von Beiträgen und die Durchsetzung des Upstream-First-Prinzips."

#: ../content/53_activity_39.md:block 5 (paragraph)
msgid ""
"With the upstream first approach all development on an open source project "
"is to be made with the level of quality and openness required to be "
"submitted to a project's core developers and published by them."
msgstr ""
"Beim Upstream-First-Prinzips müssen alle Entwicklungen an einem Open-Source-"
"Projekt in der Qualität und Offenheit erfolgen, die erforderlich sind, um "
"den Kernentwicklern eines Projekts vorgelegt und von diesen veröffentlicht "
"zu werden."

#: ../content/53_activity_39.md:block 6 (header)
msgid "Opportunity Assessment"
msgstr "Gelegenheitsbeurteilung"

#: ../content/53_activity_39.md:block 7 (paragraph)
msgid "Writing code with upstream first in mind results in:"
msgstr "Das Schreiben von Code mit Blick auf den Upstream führt zu:"

#: ../content/53_activity_39.md:block 8 (unordered list)
msgid "better quality code,"
msgstr "einer besseren Qualität des Codes,"

#: ../content/53_activity_39.md:block 8 (unordered list)
msgid "code that is ready to be submitted upstream,"
msgstr "Code, der bereit ist, upstream eingereicht zu werden,"

#: ../content/53_activity_39.md:block 8 (unordered list)
msgid "code that is merged in the core software,"
msgstr "Code, der in die Kernsoftware eingebunden wird,"

#: ../content/53_activity_39.md:block 8 (unordered list)
msgid "code that will be compatible with future version,"
msgstr "Code, der mit zukünftigen Versionen kompatibel sein wird,"

#: ../content/53_activity_39.md:block 8 (unordered list)
msgid ""
"recognition by the project community and better and more profitable "
"cooperation."
msgstr ""
"Anerkennung durch die Projektgemeinschaft und eine bessere und profitablere "
"Zusammenarbeit."

#: ../content/53_activity_39.md:block 9 (quote)
msgid ""
"Upstream First is more than just \"being kind\". It means you have a say in "
"the project. It means predictability. It means you are in control. It means "
"you act rather than react. It means you understand open source. ([Maximilian "
"Michels](https://maximilianmichels.com/2021/upstream-first/))"
msgstr ""
"Upstream First ist mehr als nur \"nett sein\". Es bedeutet, dass Sie ein "
"Mitspracherecht bei dem Projekt haben. Es bedeutet Vorhersehbarkeit. Es "
"bedeutet, dass Sie die Kontrolle haben. Es bedeutet, dass Sie agieren und "
"nicht reagieren. Es bedeutet, dass Sie Open Source verstehen.  ([Maximilian "
"Michels](https://maximilianmichels.com/2021/upstream-first/))"

#: ../content/53_activity_39.md:block 10 (header)
msgid "Progress Assessment"
msgstr "Fortschrittsbeurteilung"

#: ../content/53_activity_39.md:block 11 (paragraph)
msgid ""
"The following **verification points** demonstrate progress in this Activity: "
"Upstream first is implemented?"
msgstr ""
"Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser "
"Aktivität:? Ist Upstream First umgesetzt?"

#: ../content/53_activity_39.md:block 12 (unordered list)
msgid ""
"Significant increase in the number of pull/merge requests submitted to third "
"party projects."
msgstr ""
"Erheblicher Anstieg der Zahl der Pull-/Merge-Anträge, die bei Drittprojekten "
"eingereicht werden."

#: ../content/53_activity_39.md:block 12 (unordered list)
msgid ""
"A list of third party projects for which upstream first must be applied has "
"been drafted."
msgstr ""
"Es wurde eine Liste von Drittprojekten erstellt, für die zuerst ein Upstream-"
"Antrag gestellt werden muss."

#: ../content/53_activity_39.md:block 13 (header)
msgid "Recommendations"
msgstr "Empfehlungen"

#: ../content/53_activity_39.md:block 14 (unordered list)
msgid ""
"Identify developers with most experience at interacting with upstream "
"developers."
msgstr ""
"Ermittlung der Entwickler mit der größten Erfahrung in der Zusammenarbeit "
"mit Upstream-Entwicklern."

#: ../content/53_activity_39.md:block 14 (unordered list)
msgid ""
"Facilitate interaction between developers and core developers (events, "
"hackathons, etc.)"
msgstr ""
"Erleichterung der Interaktion zwischen Entwicklern und Kernentwicklern "
"(Veranstaltungen, Hackathons, usw.)"

#: ../content/53_activity_39.md:block 15 (header)
msgid "Resources"
msgstr "Hilfsmittel"

#: ../content/53_activity_39.md:block 16 (unordered list)
msgid ""
"A clear explanation of the Upstream First principle and why it fits in the "
"Culture Goal: https://maximilianmichels.com/2021/upstream-first/."
msgstr ""
"Eine klare Erklärung des Upstream-First-Prinzips und warum es in das "
"Kulturziel passt: https://maximilianmichels. com/2021/upstream-first/."

#: ../content/53_activity_39.md:block 17 (quote)
msgid ""
"Upstream First means that whenever you solve a problem in your copy of the "
"upstream code which others could benefit from, you contribute these changes "
"back upstream, i.e. you send a patch or open a pull request to the upstream "
"repository."
msgstr ""
"Upstream First bedeutet, dass Sie jedes Mal, wenn Sie ein Problem in Ihrer "
"Kopie des Upstream-Codes lösen, von dem andere profitieren könnten, tragen "
"Sie diese Änderungen zum Upstream bei, d.h. Sie senden einen Patch oder "
"öffnen eine Pull-Anfrage an das Upstream-Repository."

#: ../content/53_activity_39.md:block 18 (unordered list)
msgid ""
"[What is Upstream and Downstream in Software Development?](https://"
"reflectoring.io/upstream-downstream/) A crystal clear explanation."
msgstr ""
"[What is Upstream and Downstream in Software Development?](https://"
"reflectoring.io/upstream-downstream/) Eine glasklare Erklärung."

#: ../content/53_activity_39.md:block 18 (unordered list)
msgid ""
"A paper by Dave Neary: [Upstream first: Building products from open source "
"software](https://inform.tmforum.org/features-and-analysis/2017/05/upstream-"
"first-building-products-open-source-software/)."
msgstr ""
"Ein Papier von Dave Neary: [Upstream first: Building products from open "
"source software](https://inform.tmforum.org/features-and-analysis/2017/05/"
"upstream-first-building-products-open-source-software/)."

#: ../content/53_activity_39.md:block 18 (unordered list)
msgid ""
"Explained from the Chromium OS design documents: [Upstream First](https://"
"www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first)."
msgstr ""
"Erläutert über die Chromium OS Design Dokumenten: [Upstream First](https://"
"www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first)."

#: ../content/53_activity_39.md:block 18 (unordered list)
msgid ""
"Red Hat on upstream and the advantages of [upstream first](https://www."
"redhat.com/en/blog/what-open-source-upstream)."
msgstr ""
"Red Hat über Upstream und die Vorteile von [Upstream First](https://www."
"redhat.com/en/blog/what-open-source-upstream)."

#~ msgid "content/53_activity_39.md"
#~ msgstr "content/53_activity_39.md"
