#
# Silvério Santos <SSantos@web.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2022-07-28 20:21+0200\n"
"Last-Translator: Silvério Santos <SSantos@web.de>\n"
"Language-Team: German <ossgovernance@ow2.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../content/51_activity_20.md:block 1 (header)
msgid "Open source enterprise software"
msgstr "Geschäftliche Open-Source-Software"

#: ../content/51_activity_20.md:block 2 (paragraph)
msgid ""
"Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/20>."
msgstr ""
"Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/"
"issues/20>."

#: ../content/51_activity_20.md:block 3 (header)
msgid "Description"
msgstr "Beschreibung"

#: ../content/51_activity_20.md:block 4 (paragraph)
msgid ""
"This activity is about proactively selecting OSS solutions, either vendor or "
"community-supported, in business-oriented areas. It may also cover defining "
"preference policies for the selection of open source business application "
"software."
msgstr ""
"Bei dieser Aktivität geht es um die vorausschauende Auswahl von OSS-"
"Lösungen, die entweder von Anbietern oder von der Gemeinschaft unterstützt "
"werden, in geschäftsorientierten Bereichen. Sie kann auch die Festlegung von "
"Vorzugsrichtlinien für die Auswahl von Open-Source-Anwendungssoftware für "
"Unternehmen umfassen."

#: ../content/51_activity_20.md:block 5 (paragraph)
msgid ""
"While open source software is most often used by IT professionals -- "
"operating system, middleware, DBMS, system administration, development tools "
"-- it has yet to be recognized in areas where business professionals are the "
"primary users."
msgstr ""
"Während Open-Source-Software am häufigsten von IT-Fachleuten verwendet wird "
"- Betriebssysteme, Middleware, DBMS, Systemverwaltung, Entwicklungswerkzeuge "
"- ist sie in Bereichen, in denen Geschäftsleute die Hauptanwender sind, noch "
"nicht anerkannt."

#: ../content/51_activity_20.md:block 6 (paragraph)
msgid ""
"The activity concerns areas such as: Office suites, Collaboration "
"environments, User management, Workflow management, Customer relationship "
"management, Email, e-Commerce, etc."
msgstr ""
"Die Aktivität betrifft Bereiche wie: Office-Suiten, Umgebungen für die "
"Zusammenarbeit, Benutzerverwaltung, Workflow-Management Verwaltung von "
"Arbeitsabläufen, Verwaltung von Kundenbeziehungen, E-Mail, elektronischer "
"Geschäftsverkehr, usw."

#: ../content/51_activity_20.md:block 7 (header)
msgid "Opportunity Assessment"
msgstr "Gelegenheitsbeurteilung"

#: ../content/51_activity_20.md:block 8 (paragraph)
msgid ""
"As open source becomes mainstream it reaches out well beyond operating "
"systems and development tools, it increasingly finds its way into the upper "
"layers of the information systems, well into the business applications. It "
"is relevant to identify what OSS applications are successfully used to meet "
"the needs of the organisation and how they can become an organisation's cost-"
"saving preferred choice."
msgstr ""
"In dem Maße, in dem Open Source zum Regelfall wird, reicht es weit über "
"Betriebssysteme und Entwicklungswerkzeuge hinaus und findet zunehmend seinen "
"Weg in den oberen Schichten der Informationssysteme, bis hin zu den "
"Geschäftsanwendungen. Es ist wichtig zu ermitteln, welche OSS-Anwendungen "
"erfolgreich eingesetzt werden, um die Anforderungen des Unternehmens zu "
"erfüllen und wie sie zur bevorzugten Wahl eines Unternehmens werden können, "
"um Kosten zu sparen."

#: ../content/51_activity_20.md:block 9 (paragraph)
msgid "The activity may bring some retraining and migration costs."
msgstr "Die Maßnahme kann Umschulungs- und Umstellungskosten mit sich bringen."

#: ../content/51_activity_20.md:block 10 (header)
msgid "Progress Assessment"
msgstr "Fortschrittsbeurteilung"

#: ../content/51_activity_20.md:block 11 (paragraph)
msgid ""
"The following **verification points** demonstrate progress in this Activity:"
msgstr ""
"Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:"

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid ""
"There is a list of recommended OSS solutions to address pending needs in "
"business applications."
msgstr ""
"Es gibt eine Liste von empfohlenen OSS-Lösungen, um anstehende Bedürfnisse "
"bei Geschäftsanwendungen zu erfüllen."

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid ""
"A preference policy for the selection of open source business application "
"software is drafted."
msgstr ""
"Es wurde eine Vorzugsregelung für die Auswahl von quelloffener "
"Geschäftsanwendungssoftware ausgearbeitet."

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid ""
"Proprietary business applications in use are being evaluated against OSS "
"equivalents."
msgstr ""
"Proprietäre Geschäftsanwendungen werden im Vergleich zu OSS-Entsprechungen "
"bewertet."

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid ""
"Procurement process and calls for proposals specify open source preference "
"(if legally doable)."
msgstr ""
"Im Beschaffungsprozess und in Ausschreibungen wird Open-Source-Lösungen der "
"Vorzug gegeben (sofern rechtlich möglich)."

#: ../content/51_activity_20.md:block 13 (header)
msgid "Tools"
msgstr "Werkzeuge"

#: ../content/51_activity_20.md:block 14 (paragraph)
msgid "Tools to map out software and business applications?"
msgstr "Werkzeuge für die Entwicklung von Software und Geschäftsanwendungen?"

#: ../content/51_activity_20.md:block 15 (paragraph)
msgid ""
"At this stage, we cannot think of any tool relevant or concerned by this "
"Activity."
msgstr ""
"Zum jetzigen Zeitpunkt können wir uns kein Werkzeug vorstellen, das für "
"diese Tätigkeit von Bedeutung oder von ihr betroffen ist."

#: ../content/51_activity_20.md:block 16 (header)
msgid "Recommendations"
msgstr "Empfehlungen"

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid ""
"Talk to colleagues, learn from what other companies comparable to yours do."
msgstr ""
"Sprechen Sie mit Kollegen, lernen Sie von dem, was andere Unternehmen, die "
"mit Ihrem vergleichbar sind, tun."

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid ""
"Visit local industry events to find out about OSS solutions and professional "
"support."
msgstr ""
"Besuchen Sie lokale Branchenveranstaltungen, um sich über OSS-Lösungen und "
"professionelle Unterstützung zu informieren."

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid ""
"Try out community editions and community support first before committing to "
"paid support plans."
msgstr ""
"Testen Sie zunächst Community-Editionen und Community-Unterstützung, bevor "
"Sie sich für kostenpflichtige Unterstützungspakete entscheiden."

#: ../content/51_activity_20.md:block 18 (header)
msgid "Resources"
msgstr "Hilfsmittel"

#: ../content/51_activity_20.md:block 19 (unordered list)
msgid ""
"[What is enterprise open source?](https://www.redhat.com/en/blog/what-"
"enterprise-open-source): a quick read about enterprise-ready open source."
msgstr ""
"[What is enterprise open source?](https://www.redhat.com/de/blog/what-enterpri"
"se-open-source): Eine kurze Lektüre über unternehmenstaugliche Open-"
"Source-Lösungen."

#: ../content/51_activity_20.md:block 19 (unordered list)
msgid ""
"[101 Open Source Apps to Help your Business Thrive](https://digital.com/"
"creating-an-llc/open-source-business/): An indicative list of business-"
"oriented open source solutions."
msgstr ""
"[101 Open Source Apps to Help your Business Thrive](https://digital.com/"
"creating-an-llc/open-source-business/): Eine unverbindliche Liste von "
"geschäftsorientierten Open-Source-Lösungen."

#~ msgid "content/51_activity_20.md"
#~ msgstr "content/51_activity_20.md"
