#
# Silvério Santos <SSantos@web.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2022-07-05 12:57+0200\n"
"Last-Translator: Silvério Santos <SSantos@web.de>\n"
"Language-Team: German <ossgovernance@ow2.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../content/55_0_strategy.md:block 1 (header)
msgid "Strategy goal activities"
msgstr "Strategieziel-Aktivitäten"

#~ msgid "content/55_0_strategy.md"
#~ msgstr "content/55_0_strategy.md"
