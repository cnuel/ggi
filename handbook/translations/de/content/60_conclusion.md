# Fazit

## Fahrplan

Auf dem Weg zum Zen der Open-Source-Good-Governance planen wir, nach der ersten Veröffentlichung an den folgenden Funktionen zu arbeiten:

* **Definieren von Rollen** für die Aktivitäten, damit Mitarbeiter entsprechend ihrem Auftrag und ihren Fähigkeiten an den Aufgaben arbeiten können. Dies wird dazu beitragen, die richtigen Aufgaben an die richtigen Personen zu vergeben und eine neue Sichtweise auf das Programm zu ermöglichen.

* **Verbesserung und Erweiterung des Abschnitts über die Verfahrensweise** anhand der von der Gemeinschaft gesammelten Rückmeldungen. Während der Arbeit an der Umsetzung der Good-Governance-Initiative werden die Open-Source-Beauftragten neue Erkenntnisse gewinnen und Erfahrungen sammeln, die uns dabei helfen werden, ein besseres Muster und eine bessere Methodik für den Wissensschatz zu entwickeln.

* **Verbesserung der anerkannten Aktivitäten** anhand der Rückmeldungen aus der Gemeinschaft und Aufnahme neuer Aktivitäten in das bestehende Angebot. Es werden bald weitere Interessengebiete für Open-Source-Good-Governance auftauchen, wenn andere anfangen, den Wissensfundus zu nutzen und dazu beizutragen.

* Berücksichtigen eines einfachen Verfahrens zum Nachbilden und Umsetzen des Verfahrens in Organisationen, mit einer installationsähnlichen Fähigkeit. Es sollte eine einfache Art und Weise geben, wie Menschen, die den Wissensfundus nutzen wollen, ihren persönlichen Bereich einrichten können, mit allem, was nötig ist, um Aktivitäten zu sortieren und Sprints zu definieren, Scorecards auszufüllen und sichtbar über Fortschritte zu berichten. Dazu könnte auch die Entwicklung einer Anwendung gehören, die die Umsetzung der Methodik unterstützt.

* Entwicklung von Anwendungsfällen, Vorlagen und Erfahrungsberichten, die auf typische Szenarien wie KMU, Großstädte, Universitäten, usw. abgestimmt sind.

Wir werden ein Forum in der OSPO.zone einrichten, um Nutzerrückmeldungen, Ideen, Unterstützungsdiskussionen, usw. zu sammeln.

## Beitragen

Wir sind offen für Beiträge und alle unsere Aktivitäten sind offen und öffentlich. Wenn Sie sich beteiligen möchten, ist es am besten, sich auf der Mailingliste einzutragen und an der Diskussion teilzunehmen: <http://mail.ow2.org/wws/subscribe/ossgovernance>.

Die Good Governance Initiative ist eine Arbeitsgruppe, die in der OW2-Forge untergebracht ist:

* Das [GGI-Ressourcenzentrum](https://www.ow2.org/view/OSS_Governance/) enthält eine Vielzahl von Materialien und Informationen aus der Anfangszeit der Initiative.

* Die Aktivitäten werden auf der GitLab-Instanz [als Tickets](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/449) ausgearbeitet und überprüft.

* Diskussionen finden auf der [öffentlichen GGI-Mailingliste](https://mail.ow2.org/wws/info/ossgovernance) statt und wir halten regelmäßige Treffen ab. Die Protokolle sind auf der Mailingliste verfügbar und die Treffen sind für jeden zugänglich.

Um zum GGI-Ressourcenzentrum und zu den GitLab-Aktivitäten beizutragen, folgen Sie bitte diesen Anweisungen:

* Erstellen Sie Ihr OW2-Benutzerkonto unter https://www.ow2.org/view/services/registration

* Melden Sie sich einmalig unter https://www.ow2.org/ an.

* Um das [Ressourcenzentrum](https://www.ow2.org/view/OSS_Governance/): zu bearbeiten: Senden Sie uns Ihren Benutzernamen, und wir werden Ihnen den entsprechenden Zugang gewähren.

* Um auf die GitLab-Gruppe zuzugreifen: Melden Sie sich einmalig unter https://gitlab.ow2.org mit Ihren OW2-Zugangsdaten an, teilen Sie uns das mit und wir gewähren Ihnen Zugang zur GGI-Gruppe in GitLab.

* Für den Zugang zu Rocket.Chat müssen Sie:

   - Melden Sie sich mindestens einmal auf https://gitlab.ow2.org mit Ihren OW2-Zugangsdaten an.

   - Öffnen Sie Rocket.Chat und wählen Sie einen Rocket.Chat-Benutzernamen, wenn Sie dazu aufgefordert werden.

   - Gehen Sie zum Kanal #general und fragen Sie dort nach dem Zugang zum GGI-Kanal.

   - Sobald der Zugang gewährt wurde, sollten Sie in der Lage sein, auf den #good-governance-Channel zuzugreifen.

## Kontakt

Am besten können Sie mit der OW2 Good Governance Initiative in Kontakt treten, indem Sie auf der Mailingliste unter <http://mail.ow2.org/wws/subscribe/ossgovernance> posten.

Für administrative Anfragen können Sie die GGI-Initiative unter https://www.ow2.org/view/About/Management_Office erreichen.

# Anhang: Vorlage für eine maßgeschneiderte Aktivitäts-Scorecard

Die neueste Version der Vorlage für die maßgeschneiderte Aktivitäts-Scorecard ist im Abschnitt "Ressourcen" des [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) unter OW2 verfügbar.
