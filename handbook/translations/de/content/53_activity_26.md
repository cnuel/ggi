## Zu Open-Source-Projekten beitragen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/26>.

### Beschreibung

Der Beitrag zu Open-Source-Projekten, die frei genutzt werden können, ist eines der wichtigsten Prinzipien der Good Governance. Es geht darum, nicht einfach ein passiver Verbraucher zu sein, sondern dem Projekt etwas zurückzugeben. Wenn jemand eine Funktion hinzufügt oder einen Fehler für seine eigenen Zwecke behebt, sollte er dies allgemein genug tun, um zum Projekt beizutragen. Den Entwicklern muss Zeit für ihre Beiträge eingeräumt werden.

Diese Aktivität deckt den folgenden Bereich ab:

* Die Arbeit mit Upstream-Open-Source-Projekten.

* Meldung von Fehlern und Funktionswünschen.

* Einbringen von Code und Fehlerkorrekturen.

* Teilnahme an Mailinglisten der Community.

* Austausch von Erfahrungen.

### Gelegenheitsbeurteilung

Die wichtigsten Vorteile dieser Aktivität sind:

* Sie erhöht das Allgemeinwissen und das Engagement für Open Source innerhalb des Unternehmens, da die Mitarbeiter beginnen, Beiträge zu leisten und sich an Open-Source-Projekten zu beteiligen. Sie erhalten ein Gefühl der Gemeinnützigkeit und verbessern ihren eigenen Ruf.

* Das Unternehmen erhöht seinen Bekanntheitsgrad und sein Ansehen, da die Beiträge durch das Projekt wandern. Dies zeigt, dass das Unternehmen tatsächlich an Open Source beteiligt ist, einen Beitrag leistet und Fairness und Transparenz fördert.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine klare und offizielle Methode für diejenigen, die einen Beitrag leisten wollen.

- [ ] Entwickler werden ermutigt, zu Open-Source-Projekten beizutragen, die sie nutzen.

- [ ] Es gibt ein Verfahren, das die Einhaltung von Rechtsvorschriften und die Sicherheit der Beiträge von Entwicklern gewährleistet.

* Leistungskennzahlen: Umfang der externen Beiträge (Code, Mailinglisten, Probleme, ...) von Einzelpersonen, Teams oder Unternehmen.

### Werkzeuge

Es kann sinnvoll sein, die Beiträge zu verfolgen, sowohl um den Überblick über die Beiträge zu behalten als auch, um über die Bemühungen des Unternehmens berichten zu können. Zu diesem Zweck können Dashboards und Software zur Aktivitätsverfolgung eingesetzt werden. Prüfen Sie:

* Bitergias [GrimoireLab](https://chaoss.github.io/grimoirelab/)

* [Alambic](https://alambic.io)

* [ScanCode](https://scancode-toolkit.readthedocs.io)

### Empfehlungen

Ermutigen Sie die Mitarbeiter des Unternehmens, zu externen Projekten beizutragen, indem Sie:

* Ihnen Zeit geben, um allgemeine, gut getestete Fehlerkorrekturen und Funktionen zu schreiben und sie der Gemeinschaft zurückzugeben.

* Schulung der Mitarbeiter in Bezug auf Beiträge zu Open-Source-Communities. Dabei geht es sowohl um technische Fähigkeiten (Verbesserung der Kenntnisse Ihres Teams) als auch um die Gemeinschaft (Zugehörigkeit zu den Open-Source-Communities, Verhaltenskodex, usw.).

* Bieten Sie Schulungen zu rechtlichen und technischen Fragen sowie die des geistigen Eigentums an und stellen Sie innerhalb des Unternehmens einen Ansprechpartner zur Verfügung, der bei diesen Themen helfen kann, wenn die Mitarbeiter Zweifel haben.

* Bieten Sie Anreize für veröffentlichte Arbeiten.

* Beachten Sie, dass die Beiträge des Unternehmens/der Einrichtung die Qualität des Codes und die Beteiligung widerspiegeln, stellen Sie also sicher, dass Ihr Entwicklungsteam einen ausreichend guten Code liefert.

### Hilfsmittel

* Die [CHAOSS](https://chaoss.community/) Initiative der Linux Foundation bietet Werkzeuge und Hinweise zur Verfolgung von Beiträgen in der Entwicklung.
