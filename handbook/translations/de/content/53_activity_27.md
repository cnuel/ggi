## Gehören Sie zur Open-Source-Community

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/27>.

### Beschreibung

Bei dieser Aktivität geht es darum, unter den Entwicklern ein Gefühl der Zugehörigkeit zu einer größeren Open-Source-Community zu entwickeln. Wie in jeder Gemeinschaft müssen sich Menschen und Einrichtungen beteiligen und einen Beitrag zum Ganzen leisten. Es stärkt die Verbindung zwischen Praktikern und bringt Nachhaltigkeit und Bewegung in das Ökosystem. Auf der eher technischen Seite, ermöglicht es, die Schwerpunkte und den Fahrplan von Projekten festzulegen, verbessert den allgemeinen Wissensstand und das technische Bewusstsein.

Diese Aktivität deckt das folgende ab:

* **Ermitteln Sie Veranstaltungen**, an denen es sich lohnt teilzunehmen. Kontakte knüpfen, sich über neue Technologien informieren und ein Netzwerk aufbauen sind Schlüsselfaktoren, um die Vorteile von Open Source voll auszuschöpfen.

* Bedenken Sie die **Mitgliedschaft in einer Stiftung**. Open-Source-Stiftungen und -Organisationen sind ein wichtiger Bestandteil des Open-Source-Ökosystems. Sie stellen technische und organisatorische Mittel für Projekte zur Verfügung und sind ein guter neutraler Ort für Förderer, um gemeinsame Probleme und Lösungen zu diskutieren oder an Normen zu arbeiten.

* Beobachten Sie **Arbeitsgruppen**. Arbeitsgruppen sind unabhängige Arbeitsgruppen, in denen Experten in einem bestimmten Bereich wie IoT, Modellierung oder Wissenschaft zusammenarbeiten. Sie sind ein sehr wirksamer und kostengünstiger Mechanismus, um gemeinsame, wenn auch fachspezifische Anliegen gemeinsam anzugehen.

* **Beteiligung am Budget**. Am Ende ist Geld der Schlüssel zum Erfolg. Planen Sie die erforderlichen Ausgaben, gewähren Sie den Mitarbeitern bezahlte Zeit für diese Aktivitäten, sehen Sie die nächsten Schritte voraus, damit das Programm nicht nach ein paar Monaten wegen fehlender Finanzierung eingestellt werden muss.

### Gelegenheitsbeurteilung

Open Source funktioniert am besten in Verbindung mit der Open-Source-Community im Allgemeinen. Es erleichtert die Fehlerbehebung, den Austausch von Lösungen, usw.

Es ist auch eine gute Art für Unternehmen, ihre Unterstützung für Open-Source-Werte zu zeigen. Die Kommunikation über die Mitwirkung des Unternehmens ist sowohl für den Ruf des Unternehmens als auch für das Open-Source-Ökosystem wichtig.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es wird eine Liste von Veranstaltungen erstellt, an denen man teilnehmen könnte.

- [ ] Es gibt eine Beobachtung der öffentlichen Vorträge der Teammitglieder.

- [ ] Es können Anträge auf Teilnahme an Veranstaltungen eingereicht werden.

- [ ] Es können Projekte zum Sponsoring eingereicht werden.

### Empfehlungen

* Führen Sie eine Umfrage durch, um herauszufinden, welche Veranstaltungen beliebt sind oder für die Arbeit am nützlichsten wären.

* Richten Sie eine interne Kommunikation ein (Newsletter, Informationsstelle, Einladungen ...), damit die Menschen von Initiativen wissen und sich beteiligen können.

* Stellen Sie sicher, dass diese Initiativen für verschiedene Personengruppen (Entwickler, Administratoren, Unterstützer, ...) von Nutzen sein können, nicht nur für Führungskräfte auf Vorstandsebene.

### Hilfsmittel

* [What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Ein Artikel von Michael Sweeney über clearcode.cc.

* [Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) Ein Artikel von Velichka Atanasova von VMWare.

* [Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) Ein guter Text von Robert Kowalski von CloudBees.

* [7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) Ein Artikel von Simon Phipps für InfoWorld.

* [Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) Ein Artikel von Donna Benjamin von RedHat.
