## Verwalten von Softwareschwachstellen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/22>.

### Beschreibung

Der eigene Code ist so sicher wie sein unsicherster Teil. Jüngste Fälle (z.B. Heartbleed[^heartbleed], Equifax[^equifax]) haben gezeigt, wie wichtig es ist, Schwachstellen in Teilen des Codes zu prüfen, die nicht direkt vom Unternehmen entwickelt wurden. Die Folgen der Schwachstellen reichen von Datenlecks (mit enormen Auswirkungen auf den Ruf) über Ransomware-Angriffen und der das Geschäft bedrohenden Nichtverfügbarkeit von Diensten.

Open-Source-Software ist dafür bekannt, dass sie ein besseres Schwachstellenmanagement aufweist als proprietäre Software, vor allem aus folgenden Gründen:

* Das Mehr-Augen-Prinzip hilft Probleme in offenem Code und Prozessen zu finden und zu beheben.

* Open-Source-Projekte beheben Schwachstellen und veröffentlichen Patches und neue Versionen viel schneller.

So ergab eine Studie von WhiteSource über proprietäre Software, dass für 95 % der in ihren Open-Source-Komponenten gefundenen Schwachstellen zum Zeitpunkt der Analyse bereits eine Lösung gefunden worden war. Es geht also darum, Schwachstellen **sowohl in der Codebasis als auch in deren Abhängigkeiten besser zu verwalten**, unabhängig davon, ob es sich um geschlossene oder Open-Source-Software handelt.

Um diese Risiken zu entschärfen, muss ein Programm zur Bewertung der Softwareausstattung und ein regelmäßig durchgeführter Prozess zur Überprüfung der Schwachstellen eingerichtet werden. Setzen Sie Werkzeuge ein, die betroffene Teams warnen, bekannte Schwachstellen verwalten und Bedrohungen durch Software-Abhängigkeiten verhindern.

### Gelegenheitsbeurteilung

Jedes Unternehmen, das Software einsetzt, muss die Schwachstellen beobachten in:

* seiner Infrastruktur (z.B. Cloud-Infrastruktur, Netzwerkinfrastruktur, Datenspeicher),

* seine Geschäftsanwendungen (HR, CRM-Werkzeuge, interne und kundenbezogene Datenverwaltung),

* seinen internen Code: z. B. die Website des Unternehmens, interne Entwicklungsprojekte, usw.

* und alle direkten und indirekten Abhängigkeiten von Software und Dienstleistungen.

Der Investitionsertrag (ROI) von Schwachstellen ist kaum bekannt, bis etwas Schlimmes passiert. Man muss die Folgen einer größeren Datenpanne oder der Nichtverfügbarkeit von Diensten berücksichtigen, um die wahren Kosten von Sicherheitslücken abzuschätzen.

Ebenso muss eine Kultur der Geheimhaltung und des Versteckens von sicherheitsrelevanten Problemen innerhalb des Unternehmens um jeden Preis vermieden werden. Stattdessen müssen Informationen über den Stand der Schwachstellen ausgetauscht und diskutiert werden, um die besten Antworten von den richtigen Leuten zu finden, von den Entwicklern bis zu den Führungskräften.

Die Vorbeugung von Cyberangriffen durch eine sorgfältige Verwaltung von Softwareschwachstellen hat zahlreiche Vorteile:

* Vermeidung von Risiken der Rufschädigung,

* Vermeidung von Verlusten durch Ausbeutung (DDoS, Ransomware, Zeit für den Wiederaufbau eines alternativen IT-Systems nach einem Angriff),

* Einhaltung von Datenschutzbestimmungen.

Die Verwaltung von OSS-Software-Schwachstellen ist nur ein Teil des umfassenderen Cybersicherheitsprozesses, der sich ganzheitlich mit der Sicherheit der Systeme und Dienste im Unternehmen befasst.

### Fortschrittsbeurteilung

Es sollte eine Person oder ein Team geben, die für die Überwachung von Schwachstellen zuständig ist und die Entwickler sollten sich auf einfach zu handhabende Prozesse verlassen können. Die Bewertung von Schwachstellen ist ein gängiger Bestandteil des kontinuierlichen Integrationsprozesses und die Mitarbeiter sind in der Lage, den aktuellen Risikostatus in einem speziellen Dashboard zu überwachen.

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Die Tätigkeit ist abgedeckt, wenn die gesamte firmeneigene Software und die Dienste auf bekannte Schwachstellen geprüft und überwacht werden.

- [ ] Die Aktivität ist abgedeckt, wenn ein spezielles Werkzeug und ein Prozess in der Software-Produktionskette implementiert sind, um die Einbringung von Problemen in die täglichen Entwicklungsabläufe zu verhindern.

- [ ] Eine Person oder ein Team ist für die Bewertung des Risikos von CVEs/Schwachstellen im Hinblick auf die Gefährdung verantwortlich.

- [ ] Eine Person oder ein Team ist für die Weitergabe von CVE-/Schwachstelleninformationen an die betroffenen Personen (SysOps, DevOps, Entwickler usw.) verantwortlich.

### Werkzeuge

* GitHub-Werkzeuge

   - GitHub bietet Richtlinien und Werkzeuge zur Sicherung von auf der Plattform gehostetem Code. Siehe [GitHub-Dokumente](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) für weitereInformationen.

   - GitHub bietet [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies), um Schwachstellen in Abhängigkeiten automatisch zu entdecken.

* [Eclipse Steady](https://eclipse.github.io/steady/) ist ein kostenloses, quelloffenes Werkzeug, das Java- und Python-Projekte auf Schwachstellen analysiert und Entwicklern hilft, diese zu beseitigen.

* [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): ein quelloffener Schwachstellen-Scanner.

* [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): ein Open-Source-Orchestrierer, der in der Lage ist, Sicherheitshinweise für verwendete Abhängigkeiten von konfigurierten Schwachstellendatendiensten zu sammeln.

### Hilfsmittel

* Die [MITRE's vulnerability database](https://cve.mitre.org/) für CVEs. Siehe auch [NIST's security database](https://nvd.nist.gov/) für NVDs und umgebende Hilfmittel wie [CVE Details](https://www.cvedetails.com/).

* Schauen Sie auch nach dieser neuen Initiative von Google: die [open source Vulnerabilities](https://osv.dev/).

* Die OWASP-Arbeitsgruppe veröffentlicht eine Liste von Schwachstellen-Scannern [auf ihrer Website](https://owasp.org/www-community/Vulnerability_Scanning_Tools), sowohl aus der kommerziellen als auch der Open-Source-Welt.

* J. Williams and A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012.

* [Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pages 3175–3215(2020).

* [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. Es gibt auch ein [toolkit in development to implement the aforementioned dataset](https://sap.github.io/project-kb/).

[^heartbleed]: https://fr.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
