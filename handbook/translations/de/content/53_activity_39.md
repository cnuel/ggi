## Upstream first

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/39>.

### Beschreibung

Bei dieser Aktivität geht es um die Entwicklung eines Bewusstseins für die Vorteile von Beiträgen und die Durchsetzung des Upstream-First-Prinzips.

Beim Upstream-First-Prinzips müssen alle Entwicklungen an einem Open-Source-Projekt in der Qualität und Offenheit erfolgen, die erforderlich sind, um den Kernentwicklern eines Projekts vorgelegt und von diesen veröffentlicht zu werden.

### Gelegenheitsbeurteilung

Das Schreiben von Code mit Blick auf den Upstream führt zu:

* einer besseren Qualität des Codes,

* Code, der bereit ist, upstream eingereicht zu werden,

* Code, der in die Kernsoftware eingebunden wird,

* Code, der mit zukünftigen Versionen kompatibel sein wird,

* Anerkennung durch die Projektgemeinschaft und eine bessere und profitablere Zusammenarbeit.

> Upstream First ist mehr als nur "nett sein". Es bedeutet, dass Sie ein Mitspracherecht bei dem Projekt haben. Es bedeutet Vorhersehbarkeit. Es bedeutet, dass Sie die Kontrolle haben. Es bedeutet, dass Sie agieren und nicht reagieren. Es bedeutet, dass Sie Open Source verstehen. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))


### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:? Ist Upstream First umgesetzt?

- [ ] Erheblicher Anstieg der Zahl der Pull-/Merge-Anträge, die bei Drittprojekten eingereicht werden.

- [ ] Es wurde eine Liste von Drittprojekten erstellt, für die zuerst ein Upstream-Antrag gestellt werden muss.

### Empfehlungen

* Ermittlung der Entwickler mit der größten Erfahrung in der Zusammenarbeit mit Upstream-Entwicklern.

* Erleichterung der Interaktion zwischen Entwicklern und Kernentwicklern (Veranstaltungen, Hackathons, usw.)

### Hilfsmittel

* Eine klare Erklärung des Upstream-First-Prinzips und warum es in das Kulturziel passt: https://maximilianmichels. com/2021/upstream-first/.

> Upstream First bedeutet, dass Sie jedes Mal, wenn Sie ein Problem in Ihrer Kopie des Upstream-Codes lösen, von dem andere profitieren könnten, tragen Sie diese Änderungen zum Upstream bei, d.h. Sie senden einen Patch oder öffnen eine Pull-Anfrage an das Upstream-Repository.


* [What is Upstream and Downstream in Software Development?](https://reflectoring.io/upstream-downstream/) Eine glasklare Erklärung.

* Ein Papier von Dave Neary: [Upstream first: Building products from open source software](https://inform.tmforum.org/features-and-analysis/2017/05/upstream-first-building-products-open-source-software/).

* Erläutert über die Chromium OS Design Dokumenten: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).

* Red Hat über Upstream und die Vorteile von [Upstream First](https://www.redhat.com/en/blog/what-open-source-upstream).
