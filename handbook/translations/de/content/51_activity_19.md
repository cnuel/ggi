## Open-Source-Überwachung

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/19>.

### Beschreibung

Bei dieser Tätigkeit geht es darum, die Nutzung von Open Source zu überwachen und sicherzustellen, dass Open-Source-Software vorausschauend verwaltet wird. Dies betrifft mehrere Blickwinkel, sei es die Verwendung von OSS-Werkzeugen und Geschäftslösungen oder OSS als Bestandteile in eigene Entwicklungen einzubeziehen oder eine Version einer Software zu ändern und an die eigenen Bedürfnisse anzupassen, usw. Es geht auch um die Ermittlung von Bereichen, in denen Open Source zu einer (manchmal verdeckten) De-facto-Lösung geworden ist und die Bewertung ihrer Eignung.

Es kann notwendig sein, folgendes zu klären:

* Wird die erforderliche Leistung erbracht?

* Gibt es zusätzliche Leistungen, die nicht benötigt werden, aber die Unübersichtlichkeit in der BUILD- und RUN-Phase erhöhen?

* Was verlangt die Lizenz, was sind die rechtlichen Einschränkungen?

* Inwieweit macht die Entscheidung Ihr Unternehmen unabhängig von den Lieferanten?

* Gibt es eine Support-Option, die auf die Bedürfnisse Ihres Unternehmens zugeschnitten ist und wie viel kostet sie?

* TCO (Total Cost of Ownership, Gesamtbetriebskosten).

* Kennt das Management die Vorteile von Open Source, z. B. über die Einsparung von Lizenzkosten hinaus? Wenn man mit Open Source vertraut ist, kann man den größtmöglichen Nutzen aus der Zusammenarbeit mit Projektgemeinschaften und Anbietern ziehen.

* Prüfen Sie, ob es sinnvoll ist, die Entwicklungskosten zu teilen, indem Sie Ihre Entwicklungen der Gemeinschaft zur Verfügung stellen, mit allen damit verbundenen Konsequenzen, wie z. B. der Einhaltung von Lizenzen.

* Prüfen Sie die Verfügbarkeit von Community- oder professioneller Unterstützung.

### Gelegenheitsbeurteilung

Die Festlegung eines speziell auf Open Source ausgerichteten Entscheidungsprozesses ist eine Art, die Vorteile von Open Source zu maximieren.

* Er vermeidet die unkontrollierte schleichende Nutzung und die versteckten Kosten von OSS-Technologien.

* Er führt zu informierten und OSS-bewussten strategischen und organisatorischen Entscheidungen.

Kosten: Die Aktivität kann eine unzureichende De-facto-Nutzung von Open Source als unwirtschaftlich, gefährlich usw. in Frage stellen und neu überdenken.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] OSS ist zu einer bequemen Möglichkeit geworden, wenn die Entscheidung für OSS nicht als Ausnahme oder gefährliche Wahl gesehen wird.

- [ ] OSS ist zum "Mainstream" geworden.

- [ ] Die Schlüsselpersonen sind hinreichend davon überzeugt, dass die Open-Source-Lösung strategische Vorteile bietet, die eine Investition wert sind.

- [ ] Es kann nachgewiesen werden, dass die TCO der auf Open Source basierenden Lösung für Ihr Unternehmen einen höheren Wert als die Alternative bietet.

- [ ] Es gibt eine Bewertung, wie die Unabhängigkeit von Anbietern Geld spart oder in Zukunft sparen kann.

- [ ] Es gibt eine Bewertung, dass die Unabhängigkeit der Lösung das Risiko reduziert, dass eine Änderung der Lösung zu teuer wird (keine geschlossenen Datenformate möglich).

### Werkzeuge

Zum jetzigen Zeitpunkt können wir uns kein Werkzeug vorstellen, das für diese Tätigkeit von Bedeutung oder von ihr betroffen ist.

### Empfehlungen

* Ein vorausschauendes Verwaltungshandeln beim Einsatz von Open Source erfordert ein grundlegendes Bewusstsein und Verständnis der Open-Source-Grundlagen, da diese bei jeder OSS-Entscheidung berücksichtigt werden sollten.

* Vergleichen Sie die benötigten Leistungen, anstatt nach einer Alternative für eine bekannte Closed-Source-Lösung zu suchen.

* Stellen Sie sicher, dass es Unterstützung und Weiterentwicklung gibt.

* Bedenken Sie die Auswirkungen der Lizenz der Lösung auf Ihr Unternehmen.

* Überzeugen Sie alle wichtigen Akteure von den Vorteilen von Open Source, die über die "Einsparung von Lizenzkosten" hinausgehen.

* Seien Sie ehrlich, übertreiben Sie die Wirkung der Open-Source-Lösung nicht.

* Bei der Entscheidungsfindung ist es ebenso wichtig, verschiedene Open-Source-Lösungen zu bewerten, um Enttäuschungen aufgrund falscher Erwartungen zu vermeiden, um deutlich zu machen, was die Organisation tun muss und welche Vorteile die Offenheit der Lösungen mit sich bringt. Diese müssen aufgezeigt werden, damit die Organisation sie für das eigene Umfeld bewerten kann.

### Hilfsmittel

* [Top 5 Benefits of Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Gesponserter Blog, aber trotzdem interessant und schnell zu lesen.

* [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): eine von IBM gesponserte Untersuchung der OSS-Unterstützungskosten.
