## In Open Source Projekten engagieren

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/29>.

### Beschreibung

Bei dieser Aktivität geht es darum, bedeutende Beiträge zu OSS-Projekten zu leisten, die für Sie wichtig sind. Die Beiträge werden auf der Ebene der Organisation geleistet (nicht auf persönlicher Ebene wie in Nr. 26). Sie können verschiedene Formen annehmen, von der direkten Finanzierung bis zur Bereitstellung von Mitteln (z. B. Menschen, Server, Infrastruktur, Kommunikation, usw.), solange sie dem Projekt oder Ökosystem nachhaltig und wirkungsvoll zugute kommen.

Diese Aktivität knüpft an die Aktivität Nr. 26 an und bringt die Beiträge von Open-Source-Projekten auf die Ebene der Organisation, wodurch sie sichtbarer, mächtiger und nützlicher werden. Bei dieser Aktivität sollen die Beiträge eine wesentliche, langfristige Verbesserung für das OSS-Projekt bringen: z.B. ein Entwickler oder ein Team, das eine dringend benötigte neue Funktion entwickelt, Infrastrukturmittel, Server für einen neuen Dienst, die Übernahme der Wartung eines weit verbreiteten Zweigs.

Die Idee ist, einen bestimmten Prozentsatz der Ressourcen für die Förderung von Open-Source-Entwicklern bereitzustellen, die Bibliotheken oder Projekte schreiben und pflegen, die wir nutzen.

Diese Aktivität setzt eine Zuordnung der verwendeten Open-Source-Software und eine Bewertung ihrer Kritikalität voraus, um zu entscheiden, welche zu unterstützen ist.

### Gelegenheitsbeurteilung

> Wenn jedes Unternehmen, das Open Source nutzt, zumindest einen kleinen Beitrag leisten würde, hätten wir ein gesundes Ökosystem.


Die Unterstützung von Projekten trägt dazu bei, ihre Nachhaltigkeit zu gewährleisten und ermöglicht den Zugang zu Informationen, vielleicht sogar die Beeinflussung und die Priorisierung einiger Entwicklungen (obwohl dies nicht der Hauptgrund für die Unterstützung von Projekten sein sollte).

Möglicher Nutzen dieser Aktivität: Sicherstellen, dass Fehlerberichte bevorzugt behandelt werden und Entwicklungen in die stabile Version übernommen werden. Mögliche Kosten im Zusammenhang mit dieser Aktivität: Zeitaufwand für Projekte, Geldaufwand.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Gefördertes Projekt ist ausgewählt.

- [ ] Unterstützungsmöglichkeit ist beschlossen, z. B. direkter Geldbeitrag oder Codebeitrag.

- [ ] Aufgabenleiter ist ernannt.

- [ ] Ein Beitrag wurde geleistet.

- [ ] Das Ergebnis des Beitrags wurde bewertet.

Die Verifizierungspunkte stammen aus dem OpenChain Fragebogen der [Selbstzertifizierung](https://certification.openchainproject.org/):

- [ ] Wir haben eine Richtlinie für Beiträge zu Open-Source-Projekten im Namen der Organisation.

- [ ] Wir haben ein dokumentiertes Verfahren für Open-Source-Beiträge.

- [ ] Wir haben ein dokumentiertes Verfahren, um alle Software-Mitarbeiter mit der Open-Source-Beitragspolitik vertraut zu machen.

### Werkzeuge

Einige Organisationen bieten Mechanismen zur Finanzierung von Open-Source-Projekten an (es könnte praktisch sein, wenn Ihr Zielprojekt in deren Portfolios ist).

* [Open Collective](https://opencollective.com/).

* [Software Freedom Conservancy](https://sfconservancy.org/).

* [Tidelift](https://tidelift.com/).

### Empfehlungen

* Konzentrieren Sie sich auf Projekte, die für die Organisation von entscheidender Bedeutung sind: Das sind Projekte, die Sie mit Ihrem Beitrag am meisten unterstützen möchten.

* Zielprojekte der Gemeinschaft.

* Diese Aktivität erfordert ein Mindestmaß an Vertrautheit mit einem Zielprojekt.

### Hilfsmittel

* [How to support open source projects now](https://sourceforge.net/blog/support-open-source-projects-now/): Eine kurze Seite mit Ideen zur Finanzierung von Open-Source-Projekten.

* [Sustain OSS: a space for conversations about sustaining open source](https://sustainoss.org)
