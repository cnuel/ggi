## Codeüberprüfungen durchführen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/44>.

### Beschreibung

Codeüberprüfung ist eine regelmäßige Aufgabe, die die manuelle und/oder automatische Überprüfung des Quellcodes einer Anwendung umfasst, vor Freigabe eines Produkts oder der Übergabe eines Projekts an den Kunden. Im Fall von Open-Source-Software ist die Codeüberprüfung mehr als nur das Aufspüren von Fehlern; es ist ein durchgängiges Konzept für die gemeinschaftliche Entwicklung der auf Teamebene durchgeführt wird.

Codeprüfungen sollten sowohl für selbst entwickelten als auch für aus Fremdquellen wiederverwendeten Code durchgeführt werden, da sie das allgemeine Vertrauen in den Code stärken und die Eigenverantwortung fördern. Es ist auch eine hervorragende Art, umfassende Fähigkeiten und Kenntnisse innerhalb des Teams zu verbessern und die Zusammenarbeit im Team zu fördern.

### Gelegenheitsbeurteilung

Codeüberprüfungen sind immer dann sinnvoll, wenn ein Unternehmen Software entwickelt oder Fremdsoftware wiederverwendet. Codeüberprüfungen sind zwar ein Standardschritt im Softwareentwicklungsprozess, bringen aber im Zusammenhang mit Open Source besondere Vorteile mit sich:

* Bei der Veröffentlichung von eigenem Quellcode wird sichergestellt, dass angemessene Qualitätsrichtlinien eingehalten werden.

* Bei der Beteiligung an einem bestehenden Open-Source-Projekt ist zu prüfen, ob die Richtlinien des Zielprojekts eingehalten werden.

* Die öffentlich zugängliche Dokumentation wird entsprechend aktualisiert.

Es ist auch eine ausgezeichnete Gelegenheit, einige der Regeln Ihres Unternehmens zur Einhaltung der Rechtsvorschriften bekannt zu machen und durchzusetzen, wie zum Beispiel:

* Entfernen Sie niemals bestehende Lizenz-Header oder Urheberrechte, die sich in wiederverwendetem Open-Source-Code befinden.

* Kopieren und fügen Sie keinen Quellcode von Stack Overflow ohne vorherige Genehmigung des Rechtsteams ein.

* Fügen Sie bei Bedarf die korrekte Copyright-Zeile ein.

Codeüberprüfungen schaffen Vertrauen in den Code. Wenn man sich über die Güte oder die möglichen Risiken bei der Verwendung eines Softwareprodukts nicht sicher ist, sollte man Peer-Reviews und Codeprüfungen durchführen.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Die Überprüfung von Open-Source-Code wird als notwendiger Schritt anerkannt.

- [ ] Open-Source-Codeprüfungen sind geplant (entweder regelmäßig oder zu entscheidenden Zeitpunkten).

- [ ] Ein Prozess zur Durchführung von Open-Source-Codeprüfungen wurde gemeinsam festgelegt und anerkannt.

- [ ] Open-Source-Codeprüfungen sind ein Standardbestandteil des Entwicklungsprozesses.

### Empfehlungen

* Codeüberprüfung ist eine gemeinsame Aufgabe, die in einer guten Zusammenarbeitsumgebung besser funktioniert.

* Zögern Sie nicht, bestehende Werkzeuge und Muster aus der Open-Source-Welt zu verwenden, in der Codeprüfungen seit Jahren (Jahrzehnten) die Norm sind.

### Hilfsmittel

* [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): eine didaktische Lektüre zum Thema Codeprüfung, die in der Open Practice Library von Red Hat zu finden ist.

* [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): eine weitere interessante Sicht auf das Thema der Codeprüfung.
