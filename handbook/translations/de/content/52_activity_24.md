## Verwalten von Kennzahlen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/24>.

### Beschreibung

Im Rahmen dieser Tätigkeit wird eine Reihe von Kennzahlen gesammelt und überwacht, die als Grundlage für die täglichen Verwaltungsentscheidungen und die strategischen Möglichkeiten für professionell verwaltete Open-Source-Software dient.

Die wichtigsten Kennzahlen im Zusammenhang mit Open-Source-Software bilden den Hintergrund dafür, wie gut die Verwaltungsprogramme umgesetzt werden. Die Tätigkeit umfasst die Auswahl einiger Kennzahlen, die Veröffentlichung dieser Kennzahlen für die Teams und die Verwaltung sowie die regelmäßige Übermittlung über die Initiative, z. B. über einen Newsletter oder Unternehmensnachrichten.

Diese Aktivität erfordert:

* Stakeholder, um die Ziele des Programms zu diskutieren und festzulegen,

* Einführung eines Werkzeugs zur Messung und Datenerfassung in Verbindung mit der Entwicklungsinfrastruktur,

* Veröffentlichung von mindestens einem Dashboard für die Stakeholder und für alle an der Initiative beteiligten Personen.

Kennzahlen beruhen auf Daten, die aus einschlägigen Quellen erhoben werden müssen. Glücklicherweise gibt es eine Vielzahl von Quellen für Open Source Software Engineering. Beispiele hierfür sind:

* eine Entwicklungsumgebung, die CI/CD-Produktionskette,

* die Personalabteilung,

* Werkzeuge zum Testen und zur Prüfung der Softwarezusammensetzung,

* Repositories.

Beispiele für Indikatoren sind:

* Anzahl der aufgelösten Abhängigkeiten, angezeigt nach Lizenztyp.

* Anzahl der veralteten/anfälligen Abhängigkeiten.

* Anzahl der entdeckten Probleme mit Lizenzen und geistigem Eigentum.

* Beiträge zu externen Projekten.

* Dauer ungelöster Fehler.

* Anzahl der Mitwirkenden an einer Komponente, Anzahl der Commits, usw.

Bei dieser Aktivität geht es darum, diese Anforderungen und Messbedürfnisse zu definieren und ein Kontrollinstrument umzusetzen, das das auf einfache und wirkungsvolle Weise die wichtigsten Kennzahlen des Programms anzeigt.

### Gelegenheitsbeurteilung

Kennzahlen helfen dabei, die für Open-Source-Software bereitgestellten Mittel zu verstehen, besser zu verwalten und die Ergebnisse zu messen, um effektiv zu kommunizieren und den vollen Nutzen aus den Investitionen zu ziehen. Durch eine breit angelegte Kommunikation können mehr Menschen die Initiative verfolgen und sich beteiligt fühlen, sodass sie letztlich zu einem Anliegen und Ziel auf Verwaltungsebene wird.

Zwar gibt es für jede Aktivität Bewertungskriterien, die helfen, Fragen zu den erzielten Fortschritten zu beantworten, aber es besteht immer noch Bedarf an einer Überwachung, die mit Zahlen und Mengenkennzahlen erfolgt.

Unabhängig davon, ob es sich um ein kleines Start-up oder ein großes weltweites Unternehmen handelt, Schlüsselmessgrößen helfen den Teams, sich zu konzentrieren und die Leistung zu überwachen. Messgrößen sind wichtig, weil sie die Entscheidungsfindung unterstützen und die Grundlage für die Überwachung bereits getroffener Entscheidungen bilden.

Mit einfachen und praktischen Zahlen und Grafiken können die Mitglieder der gesamten Organisation die Bemühungen um Open Source verfolgen und abgleichen, so dass es zu einem gemeinsamen Anliegen und Handeln wird. Dies ermöglicht es den verschiedenen Beteiligten auch, besser einzusteigen, zum Projekt beizutragen und den Gesamtnutzen zu erhalten.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Eine Liste von Messgrößen und deren Erfassung wurde erstellt.

- [ ] Es werden Werkzeuge zur Erfassung, Speicherung, Verarbeitung und Anzeige von Kennzahlen verwendet.

- [ ] Es gibt ein allgemeines Dashboard, das allen Teilnehmern zur Verfügung steht und den Fortschritt der Initiative anzeigt.

### Werkzeuge

* [GrimoireLab](https://chaoss.github.io/grimoirelab) von Bitergia.

* [Alambic](https://alambic.io) von Castalia Solutions.

* Generische BI Werkzeuge (elasticsearch, grafana, R/Python Visualisierungen...) sind ebenfalls eine gute Lösung, wenn die richtigen Konnektoren entsprechend der definierten Ziele eingerichtet werden.

### Empfehlungen

* Schreiben Sie die Ziele und den Fahrplan für die Open Source Verwaltung auf.

* Kommunizieren Sie intern über die Maßnahmen und den Status der Initiative.

* Beteiligen Sie die Mitarbeiter an der Definition der Leistungskennzahlen, um sicherzustellen, dass

   - sie gut verstanden werden,

   - sie einen vollständigen Überblick über die Bedürfnisse bieten und

   - sie bedacht und befolgt werden.

* Erstellen Sie mindestens ein Dashboard, das für alle einsehbar ist (z. B. auf einem Bildschirm im Raum), mit den wichtigsten Kennzahlen, die den Fortschritt und die Gesamtsituation zeigen.

### Hilfsmittel

* Die [CHAOSS-Gemeinschaft](https://chaoss.community/) verfügt über viele gute Empfehlungen und Hilfsmittel im Zusammenhang mit Open-Source-Kennzahlen.

* Informieren Sie sich über die Messgrößen für [Projektattribute](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) aus der [Methodik](https://www.ow2.org/view/MRL/Overview) des OW2 Market Readiness Levels.

* [A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) von Liz Laffan ist eine interessante Lektüre über Offenheit in Open-Source-Projekten

* [Governance Indicators: A Users’ Guide](https://www.un.org/ruleoflaw/files/Governance%20Indicators_A%20Users%20Guide.pdf) ist der Leitfaden der UN für Verwaltungskennzahlen. Obwohl er sich auf Demokratie, Korruption und Transparenz von Staaten bezieht, sind die Grundlagen der Messung und der Kennzahlen in Bezug auf die Verwaltung durchaus lesenswert.
