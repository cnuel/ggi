# Methodologie

Die Umsetzung der OSS-Good-Governance-Methodik ist letztlich eine konsequente und wirkungsvolle Initiative. Sie betrifft mehrere Kategorien von Mitarbeitern, Diensten und Prozessen im Unternehmen, von alltäglichen Praktiken bis zur Personalverwaltung und von Entwicklern bis zu Führungskräften. Es gibt kein Patentrezept für die Umsetzung von Open-Source-Good-Governance. Unterschiedliche Organisationstypen, Unternehmenskulturen und -situationen erfordern unterschiedliche Ansätze für die Open-Source-Verwaltung. Für jede Organisation gibt es unterschiedliche Zwänge und Erwartungen, die zu unterschiedlichen Wegen und Arten der Verwaltung des Programms führen.

Vor diesem Hintergrund bietet die Good-Governance-Initiative eine allgemeine Vorlage von Aktivitäten, die auf den Geschäftsbereich, die Kultur und die Anforderungen einer Organisation zugeschnitten werden können. Obwohl die Vorlage den Anspruch erhebt, umfassend zu sein, kann die Methodik schrittweise umgesetzt werden. Es ist möglich, das Programm zu starten, indem man einfach die maßgeblichen Ziele und Aktivitäten für den eigenen Kontext auswählt. Die Idee besteht darin, einen ersten Entwurf eines Fahrplans zu erstellen, der den Aufbau der lokalen Initiative unterstützt.

Neben diesem Rahmen empfehlen wir auch dringend, über ein etabliertes Netzwerk wie die europäische Initiative [OSPO Alliance](https://ospo.zone) oder andere gleichgesinnte Initiativen aus der TODO-Gruppe oder OSPO++ mit Gleichgesinnten in Kontakt zu treten. Wichtig ist, dass man sich mit Menschen austauschen kann, die eine ähnliche Initiative betreiben und dass man sich über die aufgetretenen Probleme und die vorhandenen Lösungen austauschen kann.

## Die Kulisse bereiten

In Anbetracht der ehrgeizigen Ziele der Good-Governance-Methodik und ihrer potenziell breiten Wirkung ist es wichtig, mit einer Vielzahl von Personen innerhalb einer Organisation zu kommunizieren. Es wäre sinnvoll, sie einzubinden, um eine erste Reihe realistischer Erwartungen und Anforderungen festzulegen, um einen guten Start zu haben und Interesse und Unterstützung zu gewinnen. Eine gute Idee ist, die angepassten Aktivitäts-Scorecards auf der Kooperationsplattform der Organisation zu veröffentlichen, damit sie für die Kommunikation mit den Interessenvertretern genutzt werden können. Einige Hinweise:

* Ermitteln Sie die wichtigsten Interessengruppen und bringen Sie sie dazu, sich auf eine Reihe von Hauptzielen zu einigen. Beziehen Sie sie in den Erfolg der Initiative als Teil ihrer eigenen Pläne ein.

* Holen Sie sich die anfängliche Zustimmung, vereinbaren Sie die Schritte und das Tempo und vereinbaren Sie regelmäßige Überprüfungen, um sie über die Fortschritte zu informieren.

* Stellen Sie sicher, dass verstanden wird, was erreicht werden kann und was dazu gehört: Die erwarteten Verbesserungen sollten klar und die Ergebnisse sichtbar sein.

* Erstellung einer ersten Diagnose oder einer Bestandsaufnahme der Open-Source-Technologie in der Bewerberorganisation. Ergebnis: ein Dokument, das beschreibt, was dieses Programm erreichen wird, wo die Organisation gerade ist und wohin sie will.

## Angepasste Aktivitäts-Scorecard verwenden

Eine Angepasste Aktivitäts-Scorecard ist ein Formular, das eine Anerkannte Aktivität beschreibt, die auf die Besonderheiten einer Organisation zugeschnitten ist. Zusammengenommen stellt das Deck der Customised Activity Scorecards den Fahrplan für die Verwaltung von Open-Source-Software dar.

Bitte beachten Sie, dass nach ersten Erfahrungen mit der Methodik die Anpassung einer Anerkannten Aktivität an die unternehmensspezifische angepasste Scorecard bis zu einer Stunde dauert.

Die Angepasste Aktivitäts-Scorecard enthält die folgenden Abschnitte:

* **Bestimmung des Titels** Nehmen Sie sich zunächst ein paar Minuten Zeit, um ein Verständnis dafür zu entwickeln, worum es bei der Aktivität geht, welche Bedeutung sie hat und wie sie sich in Ihre gesamte OSS-Verwaltung einfügen kann.

* **Angepasste Beschreibung** Passen Sie die Aktivität an die Gegebenheiten der Organisation an, Anpassung des Geltungsbereichs. Definieren Sie den Umfang der Aktivität, den speziellen Anwendungsfall, den Sie behandeln werden.

* **Gelegenheitsbeurteilung** Erläutern Sie, warum es wichtig ist, diese Aktivität durchzuführen und welche Bedürfnisse sie anspricht. Was sind unsere Probleme? Welche Möglichkeiten gibt es, Fortschritte zu erzielen? Was kann gewonnen werden?

* **Ziele** Definieren Sie einige wichtige Ziele für die Aktivität. Schwachstellen, die behoben werden sollen, Fortschrittsmöglichkeiten, Wünsche. Stellen Sie Schlüsselaufgaben fest und was wir in dieser Iteration erreichen wollen.

* **Werkzeuge** Technologien, Werkzeuge und Produkte, die in der Aktivität verwendet werden.

* **Betriebliche Hinweise** Angaben zu Ansatz, Methode und Strategie für den Fortschritt in dieser Aktivität.

* **Schlüsselergebnisse** Definieren Sie messbare, überprüfbare zu erwartende Ergebnisse. Wählen Sie Ergebnisse, die den Fortschritt im Hinblick auf die Ziele anzeigen. Geben Sie hier Leistungskennzahlen an.

* **Fortschritt und Ergebnis** Der Fortschritt ist die Abschlussrate des Ergebnisses in %; Ergebnis ist die persönliche Erfolgsquote.

* **Persönliche Beurteilung** Für jedes Ergebnis können Sie eine kurze Erklärung hinzufügen und Ihre persönliche Zufriedenheit in der Punktzahl ausdrücken.

* **Zeitplan** Geben Sie Start- und Enddaten, Phasenaufgaben, kritische Schritte, Zwischenziele an.

* **Aufwand** Bewerten Sie die angeforderten zeitlichen und materiellen Mittel, interne und externe. Wie hoch ist der erwartete Aufwand? Wie viel wird er kosten? Welche Hilfsmittel benötigen wir?

* **Beauftragte** Sagen Sie, wer teilnimmt. Weisen Sie Aufgaben oder die Leitung der Aktivität und Verantwortlichkeiten zu.

* **Probleme** Identifizieren Sie Schlüsselfragen, voraussichtliche Schwierigkeiten, Risiken, Hindernisse, Unwägbarkeiten, wichtige Punkte und kritische Abhängigkeiten.

* **Zustand** Schreiben Sie hier eine zusammenfassende Bewertung, wie die Aktivität verläuft: gesund, verspätet, etc.

* **Gesamtfortschrittsbewertung** Ihre eigene übergeordnete, verwaltungsorientierte, zusammenfassende Fortschrittsbewertung der Aktivität.

## Iterieren

Als moderne Software-Praktiker mögen wir agil-ähnliche Methoden, die kleine und sichere Inkremente definieren, da es gute Sitte ist, die Situation regelmäßig neu zu bewerten und sinnvolle minimale Zwischenergebnisse zu liefern.

Im Umfeld eines laufenden OSPO-Programms ist dies von großer Bedeutung, da sich viele Nebenaspekte im Laufe der Zeit ändern werden, von der Organisationsstrategie und der Reaktion auf Open Source bis hin zur Verfügbarkeit und dem Einsatz der Mitarbeiter. Regelmäßige Neubewertungen und Wiederholungen ermöglichen auch eine Anpassung an die laufende Programmakzeptanz, eine bessere Verfolgung aktueller Trends und Möglichkeiten sowie kleine, zunehmende Vorteile für die Beteiligten und die Organisation als Ganzes.

Im besten Fall kann die Methodik in den folgenden fünf Abschnitten umgesetzt werden:

1. **Entdeckung** Verstehen von Schlüsselkonzepten, Übernahme von Verantwortung für die Methodik, Angleichung der Ziele und Erwartungen.

1. **Anpassung** Anpassung der Tätigkeitsbeschreibung und Gelegenheitsbeurteilung an die Besonderheiten der Organisation.

1. **Priorisierung** Ermittlung von Zielen und Schlüsselergebnissen, Aufgaben und Werkzeugen, Planung von Zwischenzielen und Erstellung eines Zeitplans.

1. **Aktivierung** Fertigstellung der Scorecard, Budgets, Zuweisungen, Beschreiben der Aufgaben in der Problemverwaltung.

1. **Iteration** Bewertung und Einstufung der Ergebnisse, Hervorhebung von Problemen, Verbesserung, Anpassen. Iterieren Sie jedes Quartal oder Semester.


Die erste Iteration des Programms vorbereiten:

* Festlegung einer ersten Reihe von Aufgaben, an denen gearbeitet werden soll und Festlegung von Prioritäten entsprechend des Bedarfs (Abweichungen vom gewünschten Zustand) und des Zeitplans. Ergebnis: eine Liste von Aufgaben, an denen während der Iteration gearbeitet werden soll.

* Definieren Sie eine Reihe von Anforderungen und Verbesserungsbereichen, teilen Sie diese den Interessengruppen und Endnutzern mit und holen Sie deren Zustimmung oder Engagement ein.

* Füllen Sie die Scorecards zur Fortschrittskontrolle aus. Eine Vorlage für eine Scorecard kann aus dem [GGI-Repository](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/) heruntergeladen werden

Führen Sie am Ende jeder Iteration eine Retrospektive durch und bereiten Sie sich auf die nächste Iteration vor:

* Teilen Sie die letzten Verbesserungen mit.

* Beurteilen Sie, wo Sie sind, ob die angestrebten Aufgaben abgeschlossen sind und verfeinern Sie den Fahrplan entsprechend.

* Überprüfen Sie die verbleibenden Schwachstellen und bitten Sie bei Bedarf andere Akteure oder Dienste um Hilfe, falls erforderlich.

* Neupriorisierung der Aufgaben entsprechend dem erneuerten Umfeld.

* Definieren Sie eine neue Untermenge von Aufgaben, die ausgeführt werden sollen.

## Genießen

Teilen Sie Ihren Erfolg mit und genießen die Erleichterung durch eine hochmoderne Open-Source-Strategie!

Die OSS Good Governance ist eine Methode zur Umsetzung eines Programms zur kontinuierlichen Verbesserung und ist als solche nie abgeschlossen. Dennoch ist es wichtig, Zwischenschritte hervorzuheben und die sich daraus ergebenden Veränderungen zu würdigen, um Fortschritte sichtbar zu machen und die Ergebnisse zu teilen.

* Kommunikation mit Interessengruppen und Endnutzern, um sie über die Vorteile und den Nutzen, den die Initiative mit sich bringt, zu informieren.

* Förderung der Nachhaltigkeit des Programms. Sicherstellen, dass die bewährten Verfahren und die aus dem Programm gezogenen Lehren stets angewendet und aktualisiert werden.

* Teilen Sie Ihre Erfahrungen mit Gleichgesinnten: Geben Sie der GGI-Arbeitsgruppe und innerhalb Ihrer OSPO-Gemeinschaft Rückmeldung und teilen Sie Ihren Ansatz mit ihnen.
