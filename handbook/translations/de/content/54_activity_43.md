## Open-Source-Beschaffungspolitik

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/43>.

### Beschreibung

Bei dieser Aktivität geht es um die Einrichtung eines Verfahrens zur Auswahl, zum Erwerb und zur Beschaffung von Open-Source-Software und -Diensten. Es geht auch darum, die tatsächlichen Kosten von Open-Source-Software zu bedenken und sie bereitzustellen. OSS mag auf den ersten Blick "kostenlos" sein, aber sie ist nicht ohne interne und externe Kosten wie Einbindung, Schulung, Wartung und Unterstützung.

Eine solche Politik erfordert, dass sowohl Open-Source- als auch proprietäre Lösungen gleichermaßen bedacht werden bei der Bewertung des Preis-Leistungs-Verhältnisses als bestmögliche Kombination von Gesambetriebstkosten (TCO) und Güte. Daher sollte die IT-Beschaffungsabteilung die Möglichkeiten von Open Source aktiv und fair bedenken und gleichzeitig sicherstellen, dass proprietäre Lösungen bei Kaufentscheidungen gleichberechtigt berücksichtigt werden.

Wenn es keinen wesentlichen Kostenunterschied zwischen proprietären und Open-Source-Lösungen gibt, kann die Bevorzugung von Open Source ausdrücklich auf der innewohnenden Freiheit der Open Source-Lösung begründet sein.

Beschaffungsabteilungen müssen verstehen, dass Unternehmen, die Unterstützung für OSS anbieten, in der Regel nicht über die kommerziellen Mittel verfügen, um an Beschaffungswettbewerben teilzunehmen und ihre Open-Source-Beschaffungspolitik und -verfahren entsprechend anpassen.

### Gelegenheitsbeurteilung

Mehrere Gründe rechtfertigen die Bemühungen um eine besondere Open-Source-Beschaffungspolitik:

* Das Angebot an kommerzieller Open-Source-Software und -Dienstleistungen wächst und kann nicht ignoriert werden und erfordert die Einführung spezieller Beschaffungsstrategien und -verfahren.

* Es gibt ein wachsendes Angebot an äußerst wettbewerbsfähigen kommerziellen Open-Source-Unternehmenslösungen für Unternehmensinformationssysteme.

* Selbst nach der Übernahme einer kostenlosen OSS-Komponente und deren Einbindung in eine Anwendung müssen eigene oder fremde Mittel für die Pflege dieses Quellcodes bereitgestellt werden.

* Die Gesamtbetriebskosten (TCO) sind bei FOSS-Lösungen oft (aber nicht unbedingt) niedriger: keine Lizenzgebühren beim Kauf/Upgrade, offener Markt für Dienstleister, die Möglichkeit, einen Teil oder die gesamte Software selbst bereitzustellen.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Neue Aufforderungen zur Einreichung von Vorschlägen fordern vorausschauend die Einreichung von Open-Source-Lösungen.

- [ ] Die Beschaffungsabteilung verfügt über eine Methode zur Bewertung von Open-Source- gegenüber proprietären Lösungen.

- [ ] Ein vereinfachtes Beschaffungsverfahren für Open-Source-Software und -Dienstleistungen wurde eingeführt und dokumentiert.

- [ ] Ein Genehmigungsprozess, der sich auf bereichsübergreifendes Fachwissen stützt, wurde definiert und dokumentiert.

### Empfehlungen

* "Stellen Sie sicher, dass Sie bei der Erstellung des Verfahrens das Fachwissen Ihrer IT-, DevOps-, Cybersicherheits-, Risikoverwaltungs- und Beschaffungsteams nutzen." (aus [5 Open Source Procurement Best Practices] (https://anchore.com/blog/5-open-source-procurement-best-practices/)).

* Das Wettbewerbsrecht kann verlangen, dass "Open Source" nicht ausdrücklich erwähnt wird.

* Wählen Sie die Technologie im Voraus aus und führen Sie dann eine Ausschreibung für Anpassungs- und Unterstützungsdienste durch.

### Hilfsmittel

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): nicht neu, aber dennoch sehr lesenswert von unseren Kollegen von OSS-watch in Großbritannien. Sehen Sie sich die [Folien](http://oss-watch.ac.uk/files/procurement.odp) an.

- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): ein kürzlich erschienener Artikel über Open-Source-Beschaffung mit nützlichen Hinweisen.
