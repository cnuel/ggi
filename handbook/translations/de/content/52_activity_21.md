## Einhaltung von Rechtsvorschriften verwalten

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/21>.

### Beschreibung

Organisationen müssen ein Verfahren zur Einhaltung von Rechtsvorschriften einführen, um ihre Nutzung und Teilnahme an Open-Source-Projekten zu sichern.

Es geht um eine ausgereifte und professionelle Verwaltung der Einhaltung von Rechtsvorschriften im Unternehmen und in der gesamten Lieferkette:

* Durchführung einer gründlichen Analyse des geistigen Eigentums, die die Bestimmung von Lizenzen und die Prüfung der Kompatibilität umfasst.

* Sicherstellung, dass die Organisation Open-Source-Komponenten als Teil ihrer Produkte oder Dienstleistungen sicher nutzen, integrieren, verändern und weitergeben kann.

* Bereitstellung eines transparenten Verfahrens für Mitarbeiter und Auftragnehmer, wie sie Open-Source-Software erstellen und zu ihr beitragen können.

*Software-Kompositionsanalyse (SCA)*: Ein großer Teil der rechtlichen Probleme und der des geistigen Eigentums ergeben sich aus der Verwendung von Komponenten, die unter Lizenzen freigegeben wurden, die entweder untereinander nicht kompatibel sind oder mit der Art und Weise, wie das Unternehmen die Komponenten verwenden und weitergeben möchte, nicht vereinbar sind. SCA ist der erste Schritt, um diese Probleme zu lösen, denn "man muss das Problem kennen, um es zu lösen". Der Prozess besteht darin, alle an einem Projekt beteiligten Komponenten in einem Stücklistendokument (Bill of Material) zu bestimmen, einschließlich der Build- und Test-Abhängigkeiten.

*Lizenzprüfung*: Bei der Lizenzprüfung wird ein Tool eingesetzt, das die Codebasis automatisch analysiert und die darin enthaltenen Lizenzen und Urheberrechte. Wird dieser Prozess regelmäßig durchgeführt und idealerweise in kontinuierliche Build- und Integrationsketten integriert, können können IP-Probleme frühzeitig erkannt werden.

### Gelegenheitsbeurteilung

Mit dem zunehmenden Einsatz von OSS in den Informationssystemen eines Unternehmens ist es unerlässlich, die potenziellen rechtlichen Risiken zu bewerten und zu verwalten.

Die Überprüfung von Lizenzen und Urheberrechten kann jedoch kompliziert und kostspielig sein. Die Entwickler müssen in der Lage sein, geistige Eigentums- und rechtliche Fragen schnell zu prüfen. Ein Team und ein Unternehmensbeauftragter, die sich mit Fragen des geistigen Eigentums und Rechtsfragen befassen, gewährleisten eine vorausschauende und einheitliche Verwaltung von Rechtsfragen, hilft bei der Sicherung der Nutzung von Open-Source-Komponenten und Beiträgen und bietet eine klare strategische Aussicht.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt ein einfach zu bedienendes Verfahren zur Lizenzprüfung für Projekte.

- [ ] Es gibt ein einfach zu bedienendes Verfahren zur Überprüfung von geistigem Eigentum für Projekte.

- [ ] Es gibt ein Team oder eine Person, die in der Organisation für die Einhaltung der Rechtsvorschriften verantwortlich ist.

- [ ] Regelmäßige Prüfungen zur Bewertung der Einhaltung von Rechtsvorschriften sind geplant.

Andere Arten der Einrichtung von Prüfpunkten:

- [ ] Es gibt ein einfach zu handhabendes Verfahren zur Lizenzprüfung.

- [ ] Es gibt ein einfach einsetzbares Team für Fragen des Rechts und des geistigen Eigentums, wie in Aktivität #13.

- [ ] Alle Projekte stellen die erforderlichen Informationen zur Verfügung, damit Personen das Projekt nutzen und zu ihm beitragen können.

- [ ] Es gibt einen Ansprechpartner im Team für Fragen zu geistigem Eigentum und Lizenzen.

- [ ] Es gibt einen Beauftragten im Unternehmen, der sich mit geistigem Eigentum und Lizenzen befasst.

- [ ] Es gibt ein besonderes Team für Fragen im Zusammenhang mit geistigem Eigentum und Lizenzen.

### Werkzeuge

* [ScanCode](https://scancode-toolkit.readthedocs.io)

* [Fossology](https://www.fossology.org/)

* [SW360](https://www.eclipse.org/sw360/)

* [Fossa](https://github.com/fossas/fossa-cli)

* [OSS Review Toolkit](https://oss-review-toolkit.org)

### Empfehlungen

* Informieren Sie über die Risiken, wenn die Lizenzierung im Konflikt mit den Unternehmenszielen steht.

* Schlagen Sie eine einfache Lösung für Projekte vor, um die Lizenzprüfung in ihrer Codebasis einzurichten.

* Informieren Sie über deren Bedeutung und helfen Sie den Projekten, sie in ihre CI-Systeme einzubauen.

* Stellen Sie eine Vorlage oder offizielle Richtlinien für die Projektstruktur bereit.

* Richten Sie automatische Kontrollen ein, um sicherzustellen, dass alle Projekte die Richtlinien einhalten.

* Erwägen Sie die Durchführung eines internen Prüfverfahrens zur Ermittlung von Lizenzen der Unternehmensinfrastruktur.

* Grundlegende Schulungen zu geistigem Eigentum und Lizenzierung für mindestens eine Person pro Team anbieten.

* Bieten Sie eine vollständige Schulung zum Thema geistiges Eigentum und Lizenzierung für den Beauftragten an.

* Einrichtung eines Verfahrens zur Eskalation von Problemen mit geistigem Eigentum und Lizenzen an den Beauftragten.

Denken Sie daran, dass es bei der Einhaltung von Rechtsvorschriften nicht nur um Recht geht, sondern auch um geistiges Eigentum. Deshalb hier ein paar Fragen zum Verständnis der Folgen der Einhaltung von Rechtsvorschriften:

* Wenn ich eine Open-Source-Komponente verteile und die Lizenzbedingungen nicht einhalte, verstoße ich gegen die Lizenz --> rechtliche Konsequenzen.

* Wenn ich eine Open-Source-Komponente in einem Projekt verwende, das ich verbreiten/veröffentlichen möchte, kann diese Lizenz dazu verpflichten, Teile des Codes sichtbar zu machen, die ich nicht als Open Source zur Verfügung stellen möchte --> Auswirkungen auf die Vertraulichkeit und taktischen Vorteil meines Unternehmens und gegenüber Dritten (rechtliche Auswirkungen).

* Es ist eine offene Diskussion darüber, ob die Verwendung einer Open-Source-Lizenz für ein Projekt, das ich veröffentlichen möchte, wichtiges geistiges Eigentum gewährt --> Auswirkungen auf das geistige Eigentum.

* Wenn ich ein Projekt *vor* einem Patentverfahren zu Open Source mache, schließt das wahrscheinlich die Entstehung von Patenten für das Projekt aus --> Auswirkung auf das geistige Eigentum.

* Wenn ich ein Projekt *nach* einem Patentverfahren zu Open Source mache, ermöglicht dies wahrscheinlich die Erstellung von (defensiven) Patenten für dieses Projekt --> Potential für geistiges Eigentum.

* Bei komplexen Projekten, die viele Teile mit vielen Abhängigkeiten beinhalten, kann die Vielzahl der Open-Source-Lizenzen zu Inkompatibilitäten zwischen den Lizenzen führen --> rechtliche Implikationen (vgl. Ticket #23).

### Hilfsmittel

* Es gibt eine umfangreiche Liste von Hilfsmitteln auf der [Existing OSS compliance group page](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence- Compliance-Tools/).

* [Recommended Open Source Compliance Practices for the enterprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Ein Buch von Ibrahim Haddad, von der Linux Foundation, über Open-Source-Compliance-Praktiken für Unternehmen.

* [OpenChain Project](https://www.openchainproject.org/)
