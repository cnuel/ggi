## Öffentliche Darstellung der Verwendung von Open Source

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/31>.

### Beschreibung

Bei dieser Aktivität geht es um die Anerkennung des Einsatzes von OSS in einem Informationssystem, in Anwendungen und in neuen Produkten.

* Bereitstellung von Erfolgsgeschichten.

* Präsentieren bei Veranstaltungen.

* Finanzielle Unterstützung der Teilnahme an Veranstaltungen.

### Gelegenheitsbeurteilung

Es ist inzwischen allgemein anerkannt, dass die meisten Informationssysteme auf OSS basieren und dass neue Anwendungen größtenteils durch die Wiederverwendung von OSS entstehen.

Der Hauptnutzen dieser Tätigkeit besteht darin, gleiche Bedingungen für OSS und proprietäre Software zu schaffen, um sicherzustellen, OSS die gleiche Aufmerksamkeit zu schenken und sie genauso professionell zu verwalten wie proprietäre Software.

Ein Nebeneffekt ist, dass das Profil des OSS-Ökosystems deutlich aufgewertet wird und da die OSS-Anwender als "Innovatoren" identifiziert werden, steigert dies auch die Attraktivität der Organisation.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Gewerbliche Open-Source-Anbieter sind berechtigt, den Namen der Organisation als Kundenreferenz zu verwenden.

- [ ] Mitwirkende dürfen dies tun und sich unter dem Namen der Organisation äußern.

- [ ] Der Einsatz von OSS wird im Jahresbericht der IT-Abteilung offen erwähnt.

- [ ] Es gibt kein Hindernis für die Organisation, ihren Einsatz von OSS in den Medien zu erläutern (Interviews, OSS- und Branchenveranstaltungen, etc.).

### Empfehlungen

* Ziel dieser Tätigkeit ist es nicht, dass die Organisation zu einer OSS-Aktivistenorganisation wird, sondern dass die Öffentlichkeit die Nutzung von OSS erkennt.

### Hilfsmittel

* Beispiel von [CERN](https://superuser.openstack.org/articles/cern-openstack-update/), das öffentlich seine Verwendung von OpenStack bestätigt
