## Steigerung der Open-Source-Kompetenz

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/18>.

### Beschreibung

Bei dieser Aktivität geht es um die Planung und den Aufbau von technischen Fähigkeiten und ersten Erfahrungen mit OSS, nachdem eine Bestandsaufnahme durchgeführt wurde (#17). Es ist auch die Gelegenheit, mit der Erstellung eines grundlegenden, einfachen Fahrplans für die Entwicklung von Fähigkeiten zu beginnen.

- Feststellen welche Fähigkeiten und Schulungen benötigt werden.

- Führen Sie ein Pilotprojekt durch, um den Ansatz in Gang zu setzen, aus der Praxis zu lernen und ein erstes Zwischenziel zu erreichen.

- Nutzen Sie die gewonnenen Erkenntnisse und bauen Sie einen Wissensschatz auf.

- Beginnen Sie damit, die nächsten Schritte für eine breitere Einführung festzulegen und zu beschreiben.

- Erarbeiten Sie eine Strategie für die nächsten Monate oder für ein Jahr, um Verwaltungs- und finanzielle Unterstützung zu gewinnen.

Tätigkeitsumfang:

* Linux, Apache, Debian, Verwaltungsfähigkeiten.

* Open-Source-Datenbanken (MariaDB, MySQL, PostgreSQL, etc.).

* Open-Source-Virtualisierung und Cloud-Technologien.

* LAMP-Stack und dessen Alternativen.

### Gelegenheitsbeurteilung

Wie jede Informationstechnologie bringt Open Source Neuerungen mit sich, wahrscheinlich sogar noch mehr. Open Source wächst schnell und verändert sich rasch. Es erfordert, dass Organisationen auf dem Laufenden bleiben.

Diese Aktivität hilft dabei, Bereiche zu ermitteln, in denen Schulungen den Mitarbeitern helfen könnten, effizienter zu werden und sich sicherer im Umgang mit Open Source zu fühlen. Sie hilft, Entscheidungen zur Mitarbeiterentwicklung zu treffen. Die Vermittlung grundlegender Open-Source-Kenntnisse bietet die Möglichkeit, diese zu bewerten:

- Erweiterung der IT-Lösungen mit bestehenden Markttechnologien, die vom Ökosystem entwickelt wurden.

- Entwicklung neuer Arten der Zusammenarbeit innerhalb und außerhalb der Organisation.

- Erwerb von Fähigkeiten in neuen und innovativen Technologien.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es wird eine Fähigkeitsmatrix entwickelt.

- [ ] Der Umfang der verwendeten OSS-Technologien wird vorsorglich festgelegt, z.B. um eine unkontrollierte Nutzung von OSS-Technologien zu vermeiden.

- [ ] Für diese Technologien wird ein ausreichendes Maß an Fachwissen erworben.

- [ ] Teams haben eine "Open Source-Grundlagen"-Schulung für den Einstieg erhalten.

### Werkzeuge

Ein wichtiges Werkzeug hierfür ist die so genannte Aktivitäts- (oder Fähigkeits-) Matrix (oder Zuordnung).

Diese Aktivität kann von folgenden Personen durchgeführt werden:

* Nutzung von Online-Anleitungen (viele davon sind kostenlos im Internet verfügbar),

* Teilnahme an Entwicklertreffen,

* Anbieterschulungen erhalten, usw.

### Empfehlungen

* Die sichere und leistungsfähige Nutzung und Entwicklung von Open-Source-Teilen erfordert eine offene, gemeinschaftliche Denkweise, die sowohl von oben (Verwaltung) als auch von unten (Entwickler) anerkannt und vermittelt werden muss.

* Stellen Sie sicher, dass der Ansatz aktiv von der Geschäftsleitung unterstützt und gefördert wird. Ohne das Bekenntnis der Hierarchie wird nichts geschehen.

* Beziehen Sie die Menschen (Entwickler, Interessenvertreter) in den Vorgang ein: Organisieren Sie runde Tische und hören Sie sich Ideen an.

* Geben Sie den Mitarbeitern Zeit und Mittel, um diese neuen Ansätze zu entdecken, auszuprobieren und damit zu spielen. Wenn möglich sollte es Spaß machen - Gamification und Belohnungen sind gute Anreize.

Ein Pilotprojekt mit den folgenden Schritten könnte als Beschleuniger dienen:

- Bestimmen Sie die Technologie oder den Rahmen, mit dem Sie beginnen möchten.

- Finden Sie Online-Schulungen, Anleitungen und Beispielcode zum Experimentieren.

- Erstellen Sie einen Prototyp der endgültigen Lösung.

- Finden Sie Fachleute, die Sie bei der Umsetzung herausfordern und beraten.

### Hilfsmittel

* [What is a Competency Matrix](https://blog.kenjo.io/what-is-a-competency-matrix): eine schnelle Lektüre zur Einführung.

* [How to Make a Skills Matrix for your Team](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): eine Vorlage mit Kommentaren.

* [MOOC on Free (libre) culture](https://librecours.net/parcours/upload-lc000/) (French Only): dies ist ein 6-teiliger Kurs über die freie Kultur, Einführung ins Urheberrecht, geistiges Eigentum, Open-Source-Lizenzierung
