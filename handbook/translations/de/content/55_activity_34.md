## Wahrnehmung auf Vorstandsebene

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/34>.

### Beschreibung

Die Open-Source-Vorhaben des Unternehmens werden nur dann ihren strategischen Nutzen entfalten, wenn sie auf höchster Ebene durchgesetzt werden, indem das Open-Source-Selbstverständnis in die Strategie und die innere Arbeitsweise des Unternehmens eingebunden wird. Ein solches Engagement kann nur stattfinden, wenn die höheren Führungskräfte und die oberste Leitung selbst ein Teil davon sind. Die Ausbildung und die Denkweise in Bezug auf Open Source müssen auch auf diejenigen ausgedehnt werden, die die Politik, die Entscheidungen und die Gesamtstrategie sowohl innerhalb als auch außerhalb des Unternehmens gestalten.

Dieses Engagement stellt sicher, dass praktische Verbesserungen, Veränderungen in der Denkweise und neue Vorhaben von der Hierarchie konsequent, wohlwollend und nachhaltig unterstützt werden, was zu einer stärkeren Beteiligung der Arbeitnehmer führt. Es prägt das Bild, das Außenstehende von der Organisation haben und bringt Vorteile für den Ruf und das Ökosystem. Sie ist auch ein Mittel, um das Vorhaben und seine Vorteile mittel- und langfristig zu festigen.

### Gelegenheitsbeurteilung

Diese Tätigkeit ist von wesentlicher Bedeutung, wenn:

* Die Organisation hat sich umfassende Ziele für die Verwaltung von Open Source gesetzt, tut sich aber schwer, diese zu erreichen. Es ist unwahrscheinlich, dass die Vorhaben ohne gute Kenntnisse und ein klares Engagement der höheren Führungsebene etwas erreichen können.

* Die Initiative ist bereits angelaufen und macht Fortschritte, aber die höheren Ebenen der Hierarchie verfolgen sie nicht richtig.

Es sollte hoffentlich deutlich werden, dass die Nutzung von Open Source nicht nur ad hoc erfolgt, sondern angesichts der Vielzahl von Teams und des kulturellen Wandels, den sie mit sich bringen kann, einen beständigen und gut durchdachten Ansatz erfordert.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt ein beauftragtes Verwaltungs-Büro oder einen -Beauftragten, das/der befugt ist, eine einheitliche Open-Source-Strategie für das gesamte Unternehmen festzulegen und sicherzustellen, dass der Geltungsbereich klar ist.

- [ ] Es gibt ein klares, verbindliches Bekenntnis der Hierarchie zur OSS-Strategie.

- [ ] Es gibt eine transparente Kommunikation der Hierarchie über ihr Engagement für das Programm.

- [ ] Die Hierarchie steht für Gespräche über Open-Source-Software zur Verfügung. Sie kann hinsichtlich ihrer Versprechen befragt und herausgefordert werden.

- [ ] Es gibt ein angemessenes Budget und eine angemessene Finanzierung für das Vorhaben.

### Empfehlungen

Beispiele für Maßnahmen im Zusammenhang mit dieser Aktivität sind:

* Durchführung von Schulungen zur Entzauberung von OSS für die Vorstandsebene.

* Einholung ausdrücklicher, praktischer Unterstützung für die OSS-Nutzung und -Strategie.

* Ausdrückliche Erwähnung und Befürwortung des OSS-Programms in der internen Kommunikation.

* Ausdrückliche Erwähnung und Befürwortung des OSS-Programms in der öffentlichen Kommunikation.

Open Source ist ein *strategischer Wegbereiter*, der die Unternehmenskultur prägt. Was bedeutet das?

* Open Source kann als Verfahren zur Unterbrechung der Lieferkette und zur Senkung der Softwarebeschaffungskosten genutzt werden.

   * Sollte Open Source in den Zuständigkeitsbereich von *Software-Anlagenverwalter* oder *Einkaufsabteilungen* fallen?

* Open-Source-Lizenzen verankern die Freiheiten, die die Vorteile von Open Source ausmachen, aber sie sind auch mit Verpflichtungen verbunden. Wenn sie nicht angemessen erfüllt werden, können die Verpflichtungen rechtliche, wirtschaftliche Risiken und solche für den Ruf eines Unternehmens mit sich bringen.

   * Gewähren Lizenzbedingungen Einblick in Bereiche des Codes, die vertraulich bleiben sollten?

   * Wird sich dies auf das Patentportfolio meiner Organisation auswirken?

   * Wie sollten Projektteams zu diesem Thema geschult und unterstützt werden?

* Der Beitrag zu fremden Open-Source-Projekten ist der größte Wert von Open Source.

   * Wie sollte mein Unternehmen dies fördern (und erfassen)?

   * Wie sollten Entwickler GitHub, GitLab, Slack, Discord, Telegram oder eines der anderen Werkzeugen nutzen, die in Open-Source-Projekten üblicherweise verwendet werden?

   * Kann sich Open Source auf die Personalpolitik des Unternehmens auswirken?

* Natürlich geht es nicht nur darum, einen Beitrag zu leisten, was ist mit meinen eigenen Open-Source-Projekten?

   * Bin ich bereit für *offene* Innovation?

   * Wie werden meine Projekte mit *eingehenden* Beiträgen umgehen?

   * Sollte ich mir die Mühe machen, eine Community für ein bestimmtes Projekt aufzubauen?

   * Wie sollte ich die Community leiten, welche Rolle sollten die Community-Mitglieder haben?

   * Bin ich bereit, Entscheidungen über den Fahrplan an eine Community abzutreten?

   * Kann Open Source ein wertvolles Werkzeug sein, um die Abschottung zwischen den Unternehmensteams zu verringern?

   * Muss ich den Austausch von Open Source von einer Unternehmenseinheit zur anderen regeln?
