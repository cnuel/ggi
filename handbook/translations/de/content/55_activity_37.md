## Open Source ermöglicht die digitale Transformation

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/37>.

### Beschreibung

> "Die digitale Transformation (auch „digitaler Wandel“) bezeichnet einen fortlaufenden, tiefgreifenden Veränderungsprozess in Wirtschaft und Gesellschaft, der durch die Entstehung immer leistungsfähigerer digitaler Techniken und Technologien ausgelöst worden ist." (deutsche Wikipedia)


Wenn die bei der digitalen Transformation am weitesten fortgeschrittenen Organisationen gemeinsam den Wandel durch ihre Geschäfts-, IT- und Finanzabteilung vorantreiben, um die Digitalisierung auf ihre Art zu verankern, überdenken sie:

- Geschäftsmodell: Wertschöpfungskette mit Ökosystemen, as a Service, SaaS.

- Finanzen: Opex/Capex, Mitarbeiter, Outsourcing.

- IT: Innovation, Legacy-/Anlagenmodernisierung.

Open Source ist das Herzstück der digitalen Transformation:

- Technologien, agile Verfahren, Produktverwaltung.

- Menschen: Zusammenarbeit, offene Kommunikation, Entwicklungs-/Entscheidungszyklen.

- Geschäftsmodelle: Testen und Kaufen, offene Innovation.

Im Hinblick auf die Wettbewerbsfähigkeit sind die Verfahren, die sich unmittelbar auf das Kundenerlebnis auswirken, wahrscheinlich am besten sichtbar. Und wir müssen erkennen, dass sowohl die großen Marktteilnehmer als auch die neu gegründeten Unternehmen die Erwartungen der Kunden drastisch verändert haben, indem sie ihnen ein noch nie dagewesenes Kundenerlebnis bieten.

Das Kundenerlebnis wie auch alle anderen Abläufe innerhalb eines Unternehmens hängen vollständig von der Informationstechnologie ab. Jedes Unternehmen muss seine Informationstechnologie umgestalten, darum geht es bei der digitalen Transformation. Unternehmen, die es noch nicht getan haben, müssen jetzt so schnell wie möglich ihre digitale Transformation vollziehen, sonst besteht die Gefahr, dass sie vom Markt verdrängt werden. Die digitale Transformation ist eine Voraussetzung für das Überleben. Da so viel auf dem Spiel steht, kann ein Unternehmen die digitale Transformation nicht vollständig einem Zulieferer überlassen. Jedes Unternehmen muss sich mit seiner Informationstechnologie auseinandersetzen, was bedeutet, dass jedes Unternehmen sich mit Open-Source-Software auseinandersetzen muss, denn ohne Open-Source-Software gibt es keine Informationstechnologie.

Erwartete Vorteile der digitalen Transformation sind unter anderem:

* Vereinfachung und Automatisierung von Kernverfahren und deren Umsetzung in Echtzeit.

* Ermöglichung schneller Reaktionen auf Veränderungen im Wettbewerb.

* Nutzung der Vorteile von künstlicher Intelligenz und Big Data.

### Gelegenheitsbeurteilung

Die digitale Transformation könnte verwaltet werden von:

* IT-Bereiche: Produktions-IT, Business Support IT (CRM, Abrechnung, Beschaffung...), Unterstützungs-IT (Personalverwaltung, Finanzen, Buchhaltung...), Big Data.

* Art der Technologie oder des Verfahrens zur Unterstützung der IT: Infrastruktur (Cloud), künstliche Intelligenz, Prozesse (Make-or-Buy, DevSecOps, SaaS).

Die Einführung von Open Source in einem bestimmten Bereich oder einer bestimmten Technologie Ihrer IT zeigt, dass Sie in diesem Bereich oder dieser Technologie aktiv werden wollen, weil Sie der Meinung sind, dass dieser Bereich oder diese Technologie Ihrer IT wichtig für die Wettbewerbsfähigkeit Ihres Unternehmens ist. Es ist wichtig, die Lage Ihres Unternehmens nicht nur im Vergleich zu Ihren Wettbewerbern, sondern auch im Vergleich zu anderen Branchen und wichtigen Beteiligten in Bezug auf Kundenerfahrung und Marktlösungen zu bewerten.

### Fortschrittsbeurteilung

1. Ebene 1: Situationsbeurteilung


* Ich habe festgestellt:

   - die IT-Bereiche, die für die Wettbewerbsfähigkeit meines Unternehmens wichtig sind und

   - die Open-Source-Technologien, die für die Entwicklung von Anwendungen in diesen Bereichen erforderlich sind. Und somit habe ich entschieden:

   - in welchen Bereichen ich die Entwicklung von Projekten selbst übernehmen will und

   - bei welchen Open-Source-Technologien ich eigenes Fachwissen aufbauen muss.

1. Level 2: Engagement


* Für einige ausgewählte Open-Source-Technologien, die im Unternehmen eingesetzt werden, wurden mehrere Entwickler geschult und werden von der Open-Source-Community als wertvolle Teilnehmer anerkannt. In einigen ausgewählten Bereichen wurden auf Open-Source-Technologien aufbauende Projekte gestartet.

1. Ebene 3: Verallgemeinerung


* Für alle Projekte wird bereits in der Anfangsphase des Projekts gezielt eine Open-Source-Alternative untersucht. Um den Projektteams die Untersuchung solcher Open-Source-Alternativen zu erleichtern, sind ein zentrales Budget und ein zentrales Architektenteam, das in der IT-Abteilung untergebracht ist, für die Unterstützung der Projekte vorgesehen.

**Leistungskennzahlen**

* Leistungskennzahl 1: Anteil der Projekte, für die eine Open-Source-Alternative untersucht wurde: (Anzahl der Projekte / Gesamtzahl der Projekte).

* Leistungskennzahl 2: Verhältnis, bei dem die Open-Source-Alternative gewählt wurde: (Anzahl der Projekte / Gesamtzahl der Projekte).

### Empfehlungen

Abgesehen von der Überschrift ist die digitale Transformation eine Denkweise, die einige grundlegende Veränderungen mit sich bringt und diese sollten auch (oder sogar hauptsächlich) von den obersten Ebenen der Organisation ausgehen. Führungskräfte müssen Initiativen und neue Ideen fördern, Risiken handhaben und möglicherweise bestehende Verfahren überarbeiten, um sie an neue Ansätze anzupassen.

Leidenschaft ist ein wichtiger Erfolgsfaktor. Eines der von wichtigen Vertretern in diesem Bereich entwickelten Mittel ist die Einrichtung von offenen Bereichen für neue Ideen, in denen jeder seine Ideen zur digitalen Transformation einbringen und frei daran arbeiten kann. Die Leitung sollte solche Vorhaben fördern.

### Hilfsmittel

* [Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).

* [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).

* [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
