## Open Source und die digitale Souveränität

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/35>.

### Beschreibung

Digitale Souveränität kann beschrieben werden als

> "Fähigkeit und Möglichkeit von Einzelpersonen und Einrichtungen, ihre Rolle(n) in der digitalen Welt digitalen Welt selbstständig, zielgerichtet und sicher wahrzunehmen." - Kompetenzzentrum für öffentliche IT, Deutschland


Um seine Geschäfte ordnungsgemäß führen zu können, ist jedes Unternehmen auf Partner, Dienstleistungen, Produkte und Werkzeuge angewiesen. Die Überprüfung der Bindungen und Zwänge dieser Abhängigkeiten ermöglicht es der Organisation, ihre Abhängigkeit von äußeren Gegebenheiten zu bewerten und zu kontrollieren und so ihre Unabhängigkeit und Widerstandsfähigkeit zu verbessern.

So ist beispielsweise die Abhängigkeit von einem bestimmten Anbieter ein starker Umstand, der die Abläufe und den Mehrwert eines Unternehmens beeinträchtigen kann und daher vermieden werden sollte. Open Source ist eine der Arten, diese Beschränkung zu überwinden. Open Source spielt eine wichtige Rolle bei der digitalen Souveränität, da es eine größere Auswahl an Lösungen, Anbietern und Integratoren sowie eine größere Kontrolle über die IT-Fahrpläne ermöglicht.

Es sei darauf hingewiesen, dass digitale Souveränität keine Frage des Vertrauens ist: Natürlich müssen wir unseren Partnern und Anbietern vertrauen, aber die Beziehung wird noch besser, wenn sie auf gegenseitigem Einverständnis und gegenseitiger Anerkennung beruht und nicht auf erzwungenen Verträgen und Zwängen.

Hier einige Vorteile einer besseren digitalen Souveränität:

* Verbesserung der Fähigkeit der Organisation, ihre eigenen Entscheidungen ohne Zwänge zu treffen.

* Verbesserung der Widerstandsfähigkeit des Unternehmens gegenüber außenstehenden Akteuren und Faktoren.

* Verbesserung der Verhandlungsposition im Umgang mit Partnern und Dienstleistern.

### Gelegenheitsbeurteilung

* Wie schwierig/teuer ist es, von einer Lösung abzurücken?

* Könnten die Lösungsanbieter unerwünschte Bedingungen für ihren Dienst auferlegen (z. B. Lizenzwechsel, Vertragsänderungen)?

* Könnten die Lösungsanbieter ihre Preise einseitig erhöhen, nur weil wir keine andere Wahl haben?

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine Bewertung der wichtigsten Abhängigkeiten für die Anbieter und Partner der Organisation.

- [ ] Es gibt einen Ausweichplan für diese ermittelten Abhängigkeiten.

- [ ] Es gibt eine erklärte Forderung nach digitaler Souveränität, wenn neue Lösungen untersucht werden.

### Empfehlungen

* Ermitteln der wichtigsten Abhängigkeitsgefahren durch Dienstanbieter und Dritte.

* Führen Sie eine Liste von quelloffenen Alternativen für kritische Dienste.

* Hinzufügen einer Forderung nach digitaler Souveränität bei der Auswahl neuer Werkzeuge und Dienste, die innerhalb der Einrichtung verwendet werden.

### Hilfsmittel

* [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) and [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), von der Open-Sourcerers Webseite.

* Ein hervorragender superuser.openstack.org-Artikel auf [The Role of Open Source in Digital Sovereignty](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). Ein ein kleiner Auszug:

   > Digitale Souveränität ist ein zentrales Anliegen des 21. Jahrhunderts, insbesondere für Europa. Open Source spielt eine wichtige Rolle bei der Verwirklichung digitaler Souveränität, indem es jedermann den Zugang zu den erforderlichen Technologien ermöglicht, aber auch indem es die für den Erfolg dieser Lösungen erforderliche Transparenz und Interoperabilität der Verwaltung bietet.

* Die Haltung der Europäischen Union zur digitalen Souveränität, aus der [Open Source Beobachtungsstelle (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observ atory-osor): Open Source, digitale Souveränität und Interoperabilität: Die Berliner Erklärung.

* UNICEFs Position über [Open Source for Digital Sovereignty](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
