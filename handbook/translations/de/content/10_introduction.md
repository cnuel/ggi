# Einleitung

In diesem Dokument wird eine Methodik zur Vorstellung einer professionellen Verwaltung von Open-Source-Software in einer Organisation vorgestellt. Es befasst sich mit der Notwendigkeit, Open-Source-Software richtig und fair zu nutzen, das Unternehmen vor technischen und rechtlichen Bedrohungen und der des geistigen Eigentums zu schützen und die Vorteile von Open Source zu maximieren. Unabhängig davon, wo eine Organisation bei diesen Themen steht, bietet dieses Dokument Anleitungen und Ideen, um voranzukommen und Ihre Reise zu einem Erfolg zu machen.

## Kontext

Die meisten großen Endnutzer und Systemintegratoren verwenden bereits Freie und Open-Source-Software (FOSS) entweder in ihren Informationssystemen oder in ihren Produkt- und Dienstleistungsbereichen. Die Einhaltung von Open-Source-Richtlinien ist zu einem immer wichtigeren Thema geworden und viele große Unternehmen haben Compliance-Beauftragte eingesetzt. Während jedoch die Klärung der Open-Source-Produktionskette eines Unternehmens - worum es bei der Einhaltung der Compliance geht - von grundlegender Bedeutung ist, *müssen* die Nutzer der Gemeinschaft etwas zurückgeben und zur Nachhaltigkeit des Open-Source-Ökosystems beitragen. Unserer Ansicht nach umfasst die Open-Source-Verwaltung das gesamte Ökosystem, die Zusammenarbeit mit lokalen Gemeinschaften, die Pflege einer gesunden Beziehung zu Open-Source-Anbietern von Open-Source-Software und Dienstleistungsspezialisten. Dies hebt die Einhaltung auf die nächste Stufe und ist, worum es bei einer *guten* Open-Source-Verwaltung geht.

Diese Initiative geht über Compliance und Haftung hinaus. Es geht um den Aufbau von Wissen in Gemeinschaften von Endnutzern (oft selbst Softwareentwickler) und Systemintegratoren, sowie um die Entwicklung von für beide Seiten vorteilhaften Beziehungen innerhalb des europäischen FOSS-Ökosystems.

OSS Good Governance ermöglicht es Organisationen aller Art -- kleinen und großen Unternehmen, Stadtverwaltungen, Universitäten, Verbänden, usw. -- die Vorteile von Open Source zu maximieren, indem sie hilft, Menschen, Prozesse, Technologie und Strategie aufeinander abzustimmen. In diesem Bereich, der Maximierung der Vorteile von Open Source, sind insbesondere in Europa alle noch dabei, zu lernen und Neuerungen einzuführen, ohne dass jemand weiß, wo er eigentlich bezüglich des Stands der Technik in diesem Bereich steht.

Diese Initiative hat zum Ziel Organisationen zu helfen, diese Ziele zu erreichen über:

* Einen strukturierten Katalog von **Aktivitäten**, einen Fahrplan für die Einrichtung einer professionellen Verwaltung von Open Source-Software.

* Ein **Verwaltungswerkzeug** zum Definieren, Überwachen, Berichten und Kommunizieren über dem Fortschritt,

* Ein **klarer und umsetzbarer Verbesserungsweg** mit kleinen, erschwinglichen Schritten zur Risikominderung, zur Schulung von Mitarbeitern, zur Anpassung von Prozessen und zur Kommunikation nach innerhalb und außerhalb der Organisation.

* **Leitfaden** und eine Reihe von **ausgewählten Referenzen** über Open-Source-Lizenzierung, bewährte Verfahren, Schulungen und das Engagement im Ökosystem, um das Open-Source-Bewusstsein und die -Kultur zu fördern, internes Wissen zu konsolidieren und Führung zu erweitern.

Dieser Leitfaden wurde unter Berücksichtigung der folgenden Anforderungen entwickelt:

* Alle Arten von Organisationen werden abgedeckt: von KMUs bis zu großen Unternehmen und gemeinnützigen Organisationen, von lokalen Behörden (z. B. Stadtverwaltungen) bis zu großen Institutionen (z. B. europäische oder staatliche Einrichtungen). Der Rahmen bietet Bausteine für eine Strategie und Hinweise für deren Umsetzung, aber *wie* die Aktivitäten durchgeführt werden, hängt ganz vom Programmkontext ab und liegt in der Verantwortung des Programmmanagers. Es kann sich als hilfreich erweisen, Beratungsdienste in Anspruch zu nehmen und sich mit Gleichgesinnten auszutauschen.

* Es werden keine Annahmen über das Niveau der technischen Kenntnisse innerhalb der Organisation oder des Tätigkeitsbereichs gemacht. Einige Organisationen müssen beispielsweise einen kompletten Lehrplan aufstellen, während andere den Teams lediglich Ad-hoc-Materialien vorschlagen.

Einige Aktivitäten werden nicht für alle Situationen relevant sein, aber der gesamte Rahmen bietet dennoch einen umfassenden Fahrplan und ebnet den Weg für maßgeschneiderte Strategien.

## Über die Good Governance Initiative

Bei OW2 ist eine Initiative eine gemeinsame Anstrengung, um Bedürfnisse des Marktes zu decken. OW2 schlägt einen methodischen Rahmen vor, um eine professionelle Verwaltung von Open-Source-Software in Organisationen einzuführen.

Die Good Governance Initiative basiert auf einem umfassenden Modell, das sich an der bekannten Hierarchie der menschlichen Bedürfnisse und Motivationen von Abraham Maslow orientiert, wie die folgende Abbildung zeigt.

![Maslow und die GGI](resources/images/ggi_maslow.png)

Die Good Governance Initiative liefert durch Ideen, Leitlinien und Aktivitäten eine Vorlage für die Einführung von Organisationseinheiten, die mit der professionellen Verwaltung von Open-Source-Software betraut sind, was auch als OSPO (Open Source Program Offices) bezeichnet wird. Die Methodik ist auch ein Verwaltungssystem für die Festlegung von Prioritäten, die Überwachung und den Austausch von Fortschritten.

Bei der Umsetzung der OSS Good Governance-Methodik werden die Organisationen ihre Fähigkeiten in verschiedenen Bereichen verbessern, u. a:

* **einsetzen** von Open-Source-Software, richtig und sicher im Unternehmen, um die Wiederverwendung und Wartbarkeit von Software sowie die Geschwindigkeit der Softwareentwicklung zu verbessern;

* **mindern** der rechtlichen und technischen Risiken im Zusammenhang mit externem Code und externer Zusammenarbeit;

* **ermitteln** der erforderlichen Schulungen für die Teams, von den Entwicklern bis zu den Teamleitern und Managern, damit alle die gleiche Vorstellung haben;

* **priorisieren** von Zielen und Aktivitäten, um eine effiziente Open-Source-Strategie zu entwickeln;

* **kommunizieren** auf effiziente Weise, innerhalb des Unternehmens und nach außen, um die Open-Source-Strategie optimal zu nutzen;

* **verbessern** der Wettbewerbsfähigkeit und Attraktivität des Unternehmens für Top-Talente im Bereich Open Source.

## Über die OSPO Alliance

Die **OSPO Alliance** wurde von einer Koalition führender europäischer Open Source Non-Profit-Organisationen ins Leben gerufen, darunter OW2, Eclipse Foundation, OpenForum Europe und Foundation for Public Code, mit dem Ziel, das Bewusstsein für Open Source in Europa und weltweit zu stärken und die strukturierte und professionelle Verwaltung von Open Source durch Unternehmen und Verwaltungen zu fördern.

Während sich die OW2 OSS Good Governance-Initiative auf die Entwicklung einer Verwaltungsmethodik konzentriert, verfolgt die OSPO Alliance das weiter gefasste Ziel, Unternehmen, insbesondere in nicht-technologischen Sektoren, und öffentlichen Einrichtungen dabei zu helfen, Open Source zu entdecken und zu verstehen, in ihren Aktivitäten davon zu profitieren und ihre eigenen OSPOs zu beherbergen.

Die OSPO Alliance hat die Website **OSPO.Zone** eingerichtet, die unter https://ospo.zone gehostet wird. Basierend auf der OW2 Good Governance Initiative ist OSPO.Zone ein Repository für umfassende Ressourcen für Unternehmen, öffentliche Einrichtungen, Forschungs- und akademische Organisationen. OSPO.Zone ermöglicht es der Allianz, mit OSPOs in ganz Europa und der Welt sowie mit unterstützenden Community-Organisationen in Kontakt zu treten. Die OSPO.Zone fördert bewährte Verfahren und leistet einen Beitrag zur Nachhaltigkeit des Open-Source-Ökosystems. Auf der Website [OSPO.Zone](https://ospo.zone) finden Sie einen schnellen Überblick über ergänzende Rahmenwerke für bewährte IT-Managementverfahren.

Die Website [OSPO Zone](https://ospo.zone) ist auch der Ort, an dem wir Rückmeldungen über die Initiative und ihren Inhalt (z. B. Aktivitäten, Wissensbestand) von der gesamten Gemeinschaft sammeln.

## Beitragende

Die folgenden tollen Leute haben zur Good Governance Initiative beigetragen:

* Frédéric Aatz (Microsoft France)

* Boris Baldassari (Castalia Solutions, Eclipse Foundation)

* Philippe Bareille (Ville de Paris)

* Gaël Blondelle (Eclipse Foundation)

* Vicky Brasseur (Wipro)

* Philippe Carré (Nokia)

* Pierre-Yves Gibello (OW2)

* Michael Jaeger (Siemens)

* Max Mehl (Free Software Foundation Europe)

* Hervé Pacault (Orange)

* Stefano Pampaloni (RIOS)

* Christian Paterson (OpenUp)

* Simon Phipps (Meshed Insights)

* Silvério Santos (Orange Business Services)

* Cédric Thomas, our master of ceremony (OW2)

* Nicolas Toussaint (Orange Business Services)

## Lizenz

Dieses Werk ist lizenziert unter der [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) Lizenz (CC-BY 4.0). Aus der Creative Commons-Webseite:

> Sie dürfen:

>
> * Teilen — das Material in jedwedem Format oder Medium vervielfältigen und weiterverbreiten

> * Bearbeiten — das Material remixen, verändern und darauf aufbauen

> und zwar für beliebige Zwecke, sogar kommerziell

>
> Sie müssen angemessene Urheber- und Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder angemessenen Art und Weise gemacht werden, allerdings nicht so, dass der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder Ihre Nutzung besonders. (Quelle: https://creativecommons.org/licenses/by/4.0/deed.de)


Jeder Inhalt ist unter Copyright: 2020-2021 OW2 & Teilnehmer der Good Governance Initiative.
