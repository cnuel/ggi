## Aufstellung einer Strategie für die Verwaltung von Open Source im Unternehmen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/16>.

### Beschreibung

Die Festlegung einer übergeordneten Strategie für die Open-Source-Governance innerhalb des Unternehmens gewährleistet die Einheitlichkeit und Sichtbarkeit der Ansätze sowohl für die eigene Nutzung als auch für fremde Beiträge und die Beteiligung. Sie macht die Kommunikation des Unternehmens wirksamer, indem sie eine klare und gefestigte Vorstellung und Führung bietet.

Die Umstellung auf Open Source bringt zahlreiche Vorteile, aber auch einige Pflichten und eine Veränderung der Unternehmenskultur mit sich. Sie kann sich auf Geschäftsmodelle auswirken und die Art und Weise beeinflussen, wie eine Organisation ihren Wert und ihr Angebot darstellt, sowie ihre Haltung gegenüber ihren Kunden und Wettbewerbern.

Diese Aktivität enthält die folgenden Aufgaben:

* Einsetzung eines OSS-Beauftragten, der von der (obersten) Führungsebene gefördert und unterstützt wird.

* Aufstellung und Veröffentlichung eines klaren Fahrplans für Open Source mit erklärten Zielen und erwarteten Vorteilen.

* Stellen Sie sicher, dass die gesamte oberste Führungsebene darüber Bescheid weiß und danach handelt.

* Förderung von OSS innerhalb des Unternehmens: Ermutigung der Mitarbeiter zur Nutzung von OSS, Förderung von eigenen Vorhaben und des Wissensstandes.

* Förderung von OSS außerhalb des Unternehmens: durch offizielle Erklärungen und Mitteilungen sowie sichtbare Beteiligung an OSS-Vorhaben.

Die Festlegung, Veröffentlichung und Durchsetzung einer klaren und einheitlichen Strategie trägt auch dazu bei, dass sich alle Mitarbeiter im Unternehmen dafür einsetzen und weitere Vorhaben von Teams erleichtert werden.

### Gelegenheitsbeurteilung

Es ist ein guter Zeitpunkt, um an dieser Aktivität zu arbeiten, wenn:

* Es keine abgestimmten Bemühungen des Führungspersonals gibt und Open Source immer noch als Ad-hoc-Lösung angesehen wird Lösung angesehen wird.

* Es bereits unternehmensinterne Vorhaben gibt, die aber nicht bis zu den oberen Ebenen der Führung durchdringen.

* Das Vorhaben ist bereits vor einiger Zeit ins Leben gerufen worden, stößt aber auf zahlreiche Hindernisse und bringt noch immer nicht die erwarteten Ergebnisse.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine klare Open-Source-Verwaltungs-Charta für das Unternehmen. Die Charta sollte enthalten:

   * was erreicht werden soll,

   * für wen wir das tun,

   * welche Befugnisse der/die Stratege(n) hat/haben und welche nicht.

- [ ] Ein Open-Source-Fahrplan ist weithin verfügbar und wird im gesamten Unternehmen anerkannt.

### Empfehlungen

* Einrichtung einer Gruppe von Personen und Verfahren zur Festlegung und Überwachung der Verwaltung von Open Source innerhalb des Unternehmens.

* Stellen Sie sicher, dass es ein klares Bekenntnis der obersten Führungsebene zu den Open-Source-Vorhaben gibt.

* Kommunizieren Sie die Open-Source-Strategie innerhalb des Unternehmens, machen Sie sie zu einem wichtigen Anliegen und zu einer echten Unternehmensverpflichtung.

* Stellen Sie sicher, dass der Fahrplan und die Strategie von allen verstanden werden, von den Entwicklungsteams bis zur Geschäftsleitung und dem Infrastrukturpersonal.

* Kommunizieren Sie über ihre Fortschritte, damit man weiß, wo die Organisation bei ihrem Einsatz steht. Veröffentlichen Sie regelmäßig Updates und Kennzahlen.

### Hilfsmittel

* [Checklist and references for Open Governance](https://opengovernance.dev/).

* [L'open source comme enjeu de souveraineté numérique, by Cédric Thomas, OW2 CEO, Workshop at Orange Labs, Paris, January 28, 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (french only).

* [Eine Reihe von Anleitungen zur Verwaltung von Open Source im Unternehmen, von der Linux Foundation](https://todogroup.org/guides/).

* [Ein schönes Beispiel für ein Open-Source-Strategiedokument der LF Energy-Gruppe](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)
