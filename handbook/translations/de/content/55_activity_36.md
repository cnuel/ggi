## Open Source ermöglicht Innovation

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/36>.

### Beschreibung

> Innovation ist die praktische Umsetzung von Ideen, die zur Einführung neuer Waren oder Dienstleistungen oder zur Verbesserung des Angebots an Waren oder Dienstleistungen führen.

>
> &mdash; <cite>Schumpeter, Joseph A.</cite>


Open Source kann ein Schlüsselfaktor für Innovation sein, durch Vielfalt, Zusammenarbeit und einen reibungslosen Austausch von Ideen. Menschen mit unterschiedlichem Hintergrund und aus verschiedenen Bereichen können unterschiedliche Blickwinkel einnehmen und neue, verbesserte oder sogar bahnbrechende Antworten auf bekannte Probleme geben. Man kann Innovation ermöglichen, indem man sich unterschiedliche Ansichten anhört und aktiv die offene Zusammenarbeit bei Projekten und Themen fördert.

In ähnlicher Weise ist die Beteiligung an der Ausarbeitung und Umsetzung offener Standards ein großartiger Förderer von bewährten Verfahren und Ideen zur Verbesserung der täglichen Arbeit des Unternehmens. Sie ermöglicht es dem Unternehmen auch, Innovationen dort voranzutreiben und zu beeinflussen, wo sie benötigt werden und verbessert seine globale Sichtbarkeit und seinen Ruf.

Durch Innovation ermöglicht Open Source nicht nur die Veränderung der Waren oder Dienstleistungen, die Ihr Unternehmen vermarktet, sondern auch das gesamte Ökosystem zu schaffen oder zu verändern, in dem Ihr Unternehmen gedeihen möchte.

Durch die Freigabe von Android als Open Source lädt Google beispielsweise Hunderttausende von Unternehmen ein, ihre eigenen Dienste auf der Grundlage dieser Open-Source-Technologie zu entwickeln. Google schafft damit ein ganzes Ökosystem, von dem alle Teilnehmer profitieren können. Natürlich sind nur sehr wenige Unternehmen mächtig genug, um aus eigener Kraft ein Ökosystem zu schaffen. Aber es gibt viele Beispiele für Bündnisse zwischen Unternehmen zur Schaffung eines solchen Ökosystems.

### Gelegenheitsbeurteilung

Es ist wichtig, die Position Ihres Unternehmens im Vergleich zu seinen Wettbewerbern, Partnern und Kunden zu bewerten, da es für ein Unternehmen oft riskant wäre, sich zu weit von den Standards und Technologien zu entfernen, die von seinen Kunden, Partnern und Wettbewerbern verwendet werden. Innovation bedeutet natürlich, anders zu sein, aber die Unterschiede sollten nicht zu groß sein, da Ihr Unternehmen sonst nicht von den Softwareentwicklungen der anderen Unternehmen des Ökosystems und von den Geschäftsimpulsen, die das Ökosystem bietet, profitieren würde.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Die Technologien -- und die Communities, die sie entwickeln --, die einen Einfluss auf das Unternehmen haben sind ermittelt worden.

- [ ] Der Fortschritt und die Veröffentlichungen dieser Open-Source-Communities werden überwacht -- ich bin sogar über ihre Strategie informiert, bevor die Veröffentlichungen öffentlich gemacht werden.

- [ ] Mitarbeiter des Unternehmens sind Mitglieder (einiger) dieser Open-Source-Communities und beeinflussen deren Fahrpläne und technische Entscheidungen, indem sie Codezeilen beisteuern und in den Leitungsgremien dieser Communities mitwirken.

### Empfehlungen

Von allen Technologien, die für den Betrieb Ihres Unternehmens notwendig sind, sollten Sie die ermitteln:

* die Technologien, die die gleichen sein könnten wie die Ihrer Konkurrenten,

* die Technologien, die ausschließlich für Ihr Unternehmen bestimmt sind.

Bleiben Sie bei neuen Technologien auf dem Laufenden. Open Source hat in den letzten zehn Jahren die Innovation vorangetrieben und viele leistungsstarke Werkzeuge stammen daher (denken Sie an Docker, Kubernetes, Apache Big Data-Projekte oder Linux). Niemand muss alles wissen, aber man sollte genug über den Stand der Technik wissen, um interessante neue Entwicklungen zu erkennen.

Erlauben Sie und ermutigen Sie andere, innovative Ideen einzubringen und diese voranzutreiben. Wenn möglich, geben Sie Mittel für diese Vorhaben aus und lassen Sie sie wachsen. Vertrauen Sie auf die Leidenschaft und den Willen der Menschen, neue Ideen und Trends zu entwickeln und zu fördern.

### Hilfsmittel

* [4 innovations we owe to open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).

* [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), von Professor Dirk Riehle.

* [Open source technology, enabling innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).

* [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).

* [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).

* [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
