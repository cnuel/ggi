## Mit Open-Source-Anbietern zusammenarbeiten

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/33>.

### Beschreibung

Sichern Sie sich Verträge mit Open-Source-Anbietern, die für Sie wichtige Software bereitstellen. Unternehmen und Einrichtungen, die Open-Source-Software herstellen, müssen sich weiterentwickeln, um die Wartung und Entwicklung neuer Merkmale zu gewährleisten. Ihr Fachwissen wird für das Projekt benötigt und die Nutzergemeinschaft ist auf Ihre anhaltende Tätigkeit und Ihre Beiträge angewiesen.

Die Zusammenarbeit mit Open-Source-Anbietern kann verschiedene Formen annehmen:

* Abonnement von Wartungsverträgen.

* Beauftragung lokaler Dienstleistungsunternehmen.

* Sponsoring von Entwicklungen.

* Bezahlen für kommerzielle Lizenzen.

Diese Aktivität setzt voraus, dass man Open-Source-Projekte als vollwertige Produkte betrachtet, für die es sich zu zahlen lohnt, ähnlich wie für proprietäre Produkte - auch wenn sie in der Regel weitaus billiger sind.

### Gelegenheitsbeurteilung

Das Ziel dieser Aktivität ist es, professionelle Unterstützung für Open-Source-Software zu gewährleisten, die in der Organisation verwendet werden. Dies hat mehrere Vorteile:

* Kontinuität des Dienstes durch rechtzeitige Fehlerkorrekturen.

* Serviceleistung durch bestmögliche Einrichtung.

* Klärung des rechtlichen/kommerziellen Stands der eingesetzten Software.

* Frühzeitiger Zugang zu Informationen.

* Stabile Finanzplanung.

Die Kosten hängen natürlich von den gewählten Wartungsverträgen ab. Ein weiterer Kostenpunkt könnte die Abkehr von Outsourcing an große Systemintegratoren zugunsten einer fein abgestimmten Auftragsvergabe an kompetente KMUs sein.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Der in der Organisation verwendete Open-Source-Code wird durch gewerbliche Unterstützung abgesichert.

- [ ] Für einige Open-Source-Projekte wurden Wartungsverträge abgeschlossen.

- [ ] Die Kosten für Open-Source-Wartungsverträge sind ein zulässiger Posten im IT-Haushalt.

### Empfehlungen

* Suchen Sie, wann immer es möglich ist, kompetente KMU vor Ort.

* Hüten Sie sich vor großen Systemintegratoren, die Fachwissen von Dritten weiterverkaufen (Weiterverkauf von Wartungsverträgen die eigentlich von erfahrenen Open-Source-KMU angeboten werden).

### Hilfsmittel

Einige Verknüpfungen zur Verdeutlichung der kommerziellen Realität von Open-Source-Software:

* [Die Sicht eines Investors auf die Entwicklung von Open-Source-Projekten von der Gemeinschaft zum Unternehmen](https://a16z.com/2019/10/04/commercializing-open-source/).

* [Eine schnelle Lektüre zum Verständnis von kommerziellem Open Source](https://www.webiny.com/blog/what-is-commercial-open-source).
