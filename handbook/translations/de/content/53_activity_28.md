## Sichtweise der Personalabteilung

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/28>.

### Beschreibung

Die Umstellung auf eine Open-Source-Kultur hat tiefgreifende Auswirkungen auf das Personalwesen:

* **Neue Prozesse und Verträge**: Die Verträge müssen angepasst werden, um fremde Beiträge zu ermöglichen und zu fördern. Dazu gehören Fragen des geistigen Eigentums und der Lizenzierung für Arbeiten innerhalb des Unternehmens, aber auch die Möglichkeit für Mitarbeiter oder Auftragnehmer, ihre eigenen Projekte durchzuführen.

* **Unterschiedliche Menschentypen**: Menschen, die mit Open Source arbeiten, haben oft andere Anreize und Mentalitäten als bei rein proprietären, betrieblichen Mitarbeitern. Verfahren und Denkweisen müssen sich an dieses auf die Gemeinschaft bezogene, ansehensorientiertes Muster anpassen, um neue Arten von Talenten anzuziehen und zu halten.

* Laufbahnentwicklung: Es muss ein Karrierepfad angeboten werden, der die fachlichen und sozialen Fähigkeiten der Mitarbeiter sowie die von Ihrem Unternehmen erwarteten Fähigkeiten fördert und wertschätzt (Zusammenarbeit, um die Bemühungen der Gemeinschaft voranzutreiben, Kommunikation, um als Sprecher für Ihr Unternehmen zu dienen, usw.). Die Personalabteilung spielt auf jeden Fall eine Schlüsselrolle bei der Förderung von Open Source als Kulturziel.

**Belegschaft**

Für einen Entwickler, der lange Zeit mit derselben proprietären Lösung gearbeitet hat, kann der Wechsel zu Open Source eine ziemliche Umstellung darstellen und eine Anpassung erfordern. Aber für die meisten Entwickler bringt Open-Source-Software nur Vorteile mit sich.

Entwickler, die heute die Schule oder Universität verlassen, haben alle schon immer mit Open Source gearbeitet. Innerhalb eines Unternehmens, verwendet die große Mehrheit der Entwickler Open-Source-Sprachen und importiert Open-Source-Bibliotheken oder Snippets jeden Tag. Es ist in der Tat viel einfacher, Zeilen von Open-Source-Code in ein Programm einzufügen, als das interne Sourcing-Verfahren auszulösen, das durch mehrfache Bestätigungen über die Führungsebene eskaliert.

Open Source macht die Arbeit eines Entwicklers interessanter, denn mit Open Source ist ein Entwickler immer auf der Suche nach dem, was seine Kollegen außerhalb des Unternehmens erfunden haben und bleibt so auf dem neuesten Stand der Technik.

Für ein Unternehmen muss es eine Personalstrategie geben, um 1) die vorhandene Belegschaft zu schulen oder umzuschulen und 2) das Unternehmen bei der Einstellung neuer Talente zu sensibilisieren und zu positionieren, was die Attraktivität des Unternehmens in Bezug auf Open Source ausmacht.

> Leute mit einer guten FLOSS-Mentalität zu bekommen, die den Code bereits verstehen und gut mit anderen zusammenarbeiten können, ist wunderbar. Die Alternative der Missionierung/Schulung/Praktikum ist lohnenswert, aber teurer und zeitaufwändiger.

>
> &mdash; <cite>CEO eines OSS-Softwareanbieters</cite>


Dies verdeutlicht, dass die Einstellung von Mitarbeitern mit Open-Source-Kompetenz eine Beschleunigung darstellt, die in der Personalstrategie bedacht werden sollte.

#### Verfahren

- Erstellen oder Überarbeiten von Stellenbeschreibungen (technische Fähigkeiten, Soft Skills, Fähigkeiten und Erfahrungen)

- Schulungsprogramme: Selbstschulung, formale Schulung, Führungscoaching, Zuordnung von Peers, Communities

- Festlegen oder Überprüfen des Karrierewegs: Fähigkeiten, Schlüsselergebnisse/Auswirkungen und Karriereschritte

### Gelegenheitsbeurteilung

1. Rahmen für Entwicklungsverfahren: Das Problem besteht wahrscheinlich nicht so sehr darin, die Entwickler dazu zu bringen, mehr Open Source zu verwenden, sondern, dafür zu sorgen, dass sie diese sicher und unter Einhaltung der Lizenzbedingungen der einzelnen Open-Source-Technologien nutzen, ohne dabei auf die üblichen Sicherheitsüberprüfungen zu verzichten (Open-Source-Codezeilen könnten bösartigen Code enthalten),

1. Überarbeitung der Zusammenarbeit: Bei den Entwicklungsverfahren besteht die Möglichkeit, die Agilität und die Zusammenarbeit auf andere Geschäftsbereiche in Ihrem Unternehmen auszuweiten. Inner Sourcing wird oft eingesetzt, um diese Verhaltensweisen zu fördern, obwohl dies nur eine halbherzige Art der Open-Source-Kultur sein kann,

1. Organisationskultur: Letztendlich geht es hier um die Kultur Ihrer Organisation: Open Source kann das Aushängeschild für Werte wie Offenheit, Zusammenarbeit, Ethik und Nachhaltigkeit sein.


### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt Schulungen, die sowohl die Vorteile als auch die Einschränkungen (Einhaltung der Lizenzbedingungen für geistiges Eigentum) im Zusammenhang mit Open Source aufzeigen.

- [ ] Jeder Entwickler, jeder Architekt, jeder Projektleiter (oder Product Owner/Business Owner) versteht die Vorteile und Einschränkungen (Einhaltung der Lizenzbedingungen für geistiges Eigentum) im Zusammenhang mit Open Source.

- [ ] Entwickler werden ermutigt, zu Open-Source-Communities beizutragen und Verantwortung für sie zu übernehmen und können dafür eine angemessene Schulung erhalten.

- [ ] Die Fähigkeiten und Kompetenzen spiegeln sich in den Stellenbeschreibungen und Karrierestufen der Organisation wider.

- [ ] Erfahrungen, die Entwickler im Bereich Open Source gesammelt haben (Beiträge zu Open-Source-Communities, Teilnahme am internen Regelbefolgungsverfahren, externe Ansprechpartner für das Unternehmen, usw.), werden bei der Personalbewertung berücksichtigt.

### Werkzeuge

* Qualifikationsmatrix.

* Öffentliche Schulungsprogramme (z. B. Open Source School).

* Beschaffung: GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita, ...

* Vertragsvorlagen (Loyalitätsklausel).

* Stellenbeschreibungen (Vorlagen) & Karriereschritte (Vorlagen).

### Empfehlungen

In den meisten Fällen kennen die Entwickler heutzutage bereits einige Open-Source-Prinzipien und sind bereit, mit und an Open-Source-Software zu arbeiten. Dennoch gibt es einige Maßnahmen, die die Leitung ergreifen sollte:

* Bevorzugung von OSS-Erfahrung bei der Einstellung, auch wenn die Aufgabe, für die der Entwickler eingestellt wird, nur mit proprietärer Technologie zu tun hat. Die Chancen stehen gut, dass der Entwickler im Zuge der digitalen Transformation eines Tages mit Open Source arbeiten muss.

* OSS-Schulungsprogramm: Jeder Entwickler, jeder Architekt, jeder Projektleiter (oder Product Owner/Business Owner) sollte Zugang zu Schulungsmaterialien (Videos oder persönliche Schulungen) haben, die die Vorteile von Open Source, aber auch die Einschränkungen in Bezug auf geistiges Eigentum und die Einhaltung von Lizenzen aufzeigen.

* Für Entwickler, die einen Beitrag zu Open-Source-Communities leisten und Teil der Leitungsgremien dieser Gemeinschaften sein wollen, sollten Schulungen angeboten werden (Linux-Zertifizierungen).

* Anerkennung des Beitrags des Mitarbeiters (Entwickler oder Architekt) zu Open-Source-Themen wie Beiträge zu Open-Source-Communities und Einhaltung der Lizenzbedingungen für geistiges Eigentum in den Personalbeurteilungsprozessen. Die meisten Themen sind allgemeiner Art und passen in technische Karrierewege, während einige spezifisch sein können oder sollten.

* Am besten gehütetes Geheimnis und Haltung des Unternehmens: Es müssen der Kommunikationsbereich (wie wichtig ist dies für Ihr Unternehmen, dass es sich in Ihrem Jahresbericht widerspiegeln könnte), wie wirkt es sich auf Ihre Kommunikationshaltung aus (ein Open-Source-Mitarbeiter könnte ein Ansprechpartner für Ihr Unternehmen sein, einschließlich Pressekontakte).

### Hilfsmittel

* Bezüglich der Möglichkeit, sich bei Veranstaltungen außerhalb des Unternehmens zu Wort zu melden, siehe Aktivität 31: "(Engagementziel) Öffentliche Darstellung der Verwendung von Open Source".
