## Verwalten von Fähigkeiten und Mitteln zur Entwicklung von Open-Source-Software

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/42>.

### Beschreibung

Diese Aktivität konzentriert sich auf Fähigkeiten und Mittel der **Softwareentwicklung**. Sie umfasst die Technologien und besonderen Entwicklungsfähigkeiten der Entwickler sowie die allgemeinen Entwicklungsprozesse, Verfahren und Werkzeuge.

Für Open-Source-Technologien gibt es eine große Zahl an Beschreibungen, Foren und Diskussionen, die sich aus dem Ökosystem ergeben sowie öffentliche Hilfsmittel. Um in vollem Umfang aus ihrem Open-Source-Ansatz Nutzen zu ziehen, muss man einen Fahrplan mit den aktuellen Beständen und den gewünschten Zielen aufstellen, um ein einheitliches Programm für Entwicklungsfähigkeiten, Verfahren und Werkzeugen innerhalb der Teams.

#### Anwendungsbereiche

Es muss festgelegt werden, in welchen Bereichen das Programm eingesetzt werden soll und wie es die Qualität und Leistungsfähigkeit des Codes und der Verfahren verbessern wird. So wird das Programm beispielsweise nicht den gleichen Nutzen haben, wenn nur ein einziger Entwickler an Open-Source-Teilen arbeitet oder wenn der gesamte Entwicklungszyklus so optimiert wird, dass er bewährte Open-Source-Praktiken einschließt.

Man muss den Bereich bestimmen, der für die Open-Source-Entwicklung in Frage kommt: technische Teile, Anwendungen, Modernisierungen oder Neuentwicklungen. Beispiele für Entwicklungsverfahren, die von Open Source profitieren können, sind:

- Cloud Verwaltung,

- Cloud-native Anwendungen, wie man mit diesen Technologien innovativ sein kann,

- DevOps, kontinuierliche Integration / kontinuierliche Bereitstellung.

#### Kategorien

* Erforderliche Fähigkeiten und Hilfsmittel für die Entwicklung von Open-Source-Software: geistiges Eigentum, Lizenzierung, Praktiken.

* Erforderliche Fähigkeiten und Hilfsmittel für die Entwicklung von Software mit Open-Source-Komponenten, -Sprachen und -Technologien.

* Erforderliche Fähigkeiten und Hilfsmittel für den Einsatz von Open-Source-Verfahren und -Abläufen.

### Gelegenheitsbeurteilung

Open-Source-Werkzeuge werden bei Entwicklern immer beliebter. Diese Aktivität befasst sich mit der Notwendigkeit, die Verbreitung von verschiedenartigen Werkzeugen innerhalb einer Entwicklungsgruppe zu vermeiden. Sie hilft bei der Festlegung einer Vorgehensweise in diesem Bereich. Sie hilft bei der Optimierung von Ausbildung und Erfahrungsaufbau. Ein Fähigkeitenverzeichnis wird für die Einstellung, Schulung und Nachfolgeplanung verwendet, für den Fall, dass ein wichtiger Mitarbeiter das Unternehmen verlässt.

Wir bräuchten eine Methodik für die Zuordnung von Fähigkeiten in der Open-Source-Softwareentwicklung.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine Beschreibung der Herstellungskette von Open Source (die "Software-Lieferkette").

- [ ] Es gibt einen Plan (oder eine Wunschliste) für die Straffung der Entwicklungsressourcen.

- [ ] Es gibt ein Fähigkeitenverzeichnis, das die Fähigkeiten, die Ausbildung und die Erfahrung der derzeitigen Entwickler zusammenfasst.

- [ ] Es gibt eine Wunschliste für Schulungen und ein Programm zur Behebung von Fähigkeitslücken

- [ ] Es gibt eine Liste fehlender bewährter Verfahren für die Open-Source-Entwicklung und einen Plan zu deren Anwendung.

### Empfehlungen

- Beginnen Sie mit einfachen Mitteln und bauen Sie die Untersuchung und den Fahrplan kontinuierlich aus.

- Legen Sie bei der Einstellung den Schwerpunkt auf Open-Source-Fähigkeiten und -Erfahrungen. Es ist immer einfacher, wenn die Leute bereits einen Open-Source-Hintergrund haben, als sie zu schulen und zu betreuen.

- Prüfen Sie Schulungsprogramme von Softwareanbietern und Open-Source-Schulen.

### Hilfsmittel

Weitere Informationen:

* Ein Einführung in [what is a Skills Inventory?](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) von Robert Tanner.

* Ein Artikel über Open Source Fähigkeiten: [5 Open Source Skills to Up Your Game and Your Resume](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Diese Aktivität kann technische Hilfsmittel und Fähigkeiten einschließen, wie z.B.:

- **Beliebte Sprachen** (wie etwa Java, PHP, Perl, Python),

- **Open-Source-Frameworks** (Spring, AngularJS, Symfony) and Testhilfsmittel,

- **Entwicklungsmethoden und bewährte Verfahren** für Agile, DevOps und Open Source.

Zugehörige Aktivitäten:

* [(Kulturziel) Personalsichtweise](https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/28)
