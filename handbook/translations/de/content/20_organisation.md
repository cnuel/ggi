# Organisation

## Fachwortschatz

Die OSS Good Governance Methodikvorlage ist auf vier Schlüsselkonzepte aufgebaut: Ziele, Anerkannte Aktivitäten, Angepasste Aktivitäts-Scorecards und Iteration.

* **Ziele**: Ein Ziel ist eine Reihe von Aktivitäten, die mit einem gemeinsamen Anliegen verbunden sind. Es gibt fünf Ziele: Verwendungsziel, Vertrauensziel, Kulturziel, Engagementziel und Strategieziel. Ziele können unabhängig voneinander und parallel erreicht und durch Aktivitäten iterativ verfeinert werden.

* **Anerkannte Aktivitäten**: Innerhalb eines Ziels befasst sich eine Aktivität mit einem einzelnen Anliegen oder Entwicklungsthema, wie z. B. die Verwaltung der Einhaltung von Rechtsvorschriften, das als inkrementeller Schritt zur Erreichung der Programmziele verwendet werden kann. Der vollständige Satz von Aktivitäten, wie er von der GGI definiert wurde, wird als anerkannte Aktivitäten bezeichnet.

* **Angepasste Aktivitäts-Scorecard (CAS)**: Um GGI in einer bestimmten Organisation umzusetzen, müssen die anerkannten Aktivitäten an die Besonderheiten des Kontexts angepasst werden, so dass eine Reihe von angepassten Aktivitäts-Scorecards erstellt werden. Die angepasste Aktivitäts-Scorecard beschreibt, wie die Aktivität im Kontext der Organisation umgesetzt wird und wie der Fortschritt überwacht wird.

* **Iteration**: Die OSS Good Governance ist ein Managementsystem und erfordert als solches eine regelmäßige Bewertung, Überprüfung und Überarbeitung. Denken Sie an das Buchhaltungssystem in einer Organisation, es ist ein fortlaufender Prozess mit mindestens einem jährlichen Kontrollpunkt, der Bilanz; in gleicher Weise erfordert der OSS-Good-Governance-Prozess mindestens eine jährliche Überprüfung, allerdings können die Überprüfungen je nach den Aktivitäten seltener oder häufiger erfolgen.

## Ziele

Die von der GGI definierten anerkannten Aktivitäten sind in Ziele gegliedert. Jedes Ziel befasst sich mit einem bestimmten Bereich des Fortschritts innerhalb des Prozesses. Von der Nutzung bis zur Strategie decken die Ziele Themen ab, die alle Interessengruppen betreffen, von den Entwicklungsteams bis hin zur Vorstandsebene.

* **Verwendungsziel**: Dieses Ziel umfasst die grundlegenden Schritte bei der Verwendung von Open-Source-Software. Die Aktivitäten im Zusammenhang mit dem "Verwendungsziel" decken die ersten Schritte eines Open-Source-Programms ab, indem ermittelt wird, wie effizient Open Source verwendet wird und was es der Organisation bringt. Dazu gehören Schulungen und Wissensmanagement, die Erstellung von Verzeichnissen der bereits im Unternehmen verwendeten Open-Source-Software und die Vorstellung einiger Open-Source-Konzepte, die während des gesamten Prozesses genutzt werden können.

* **Vertrauensziel**: Bei diesem Ziel geht es um die sichere Verwendung von Open Source. Das "Vertrauensziel" befasst sich mit der Einhaltung von Gesetzen, dem Management von Abhängigkeiten und Schwachstellen und zielt allgemein darauf ab, Vertrauen in die Art und Weise zu schaffen, wie die Organisation Open Source verwendet und verwaltet.

* **Kulturziel**: Das kulturelle Ziel umfasst Aktivitäten, die darauf abzielen, dass sich Teams mit Open Source wohlfühlen, sich individuell an gemeinschaftlichen Aktivitäten beteiligen und bewährte Open-Source-Verfahren verstehen und umsetzen. Dieses Ziel fördert das Gefühl der Zugehörigkeit zur Open-Source-Community bei den Einzelnen.

* **Engagementziel**: Dieses Ziel besteht darin, sich auf Unternehmensebene für das Open-Source-Ökosystem zu engagieren. Es werden personelle und finanzielle Ressourcen bereitgestellt, um einen Beitrag zu Open-Source-Projekten zu leisten. Damit bekräftigt das Unternehmen, dass es ein verantwortungsbewusster "Open-Source-Citizen" ist und erkennt seine Verantwortung an, die Nachhaltigkeit des Open-Source-Ökosystems zu gewährleisten.

* **Strategieziel**: Bei diesem Ziel geht es darum, Open Source auf den höchsten Ebenen der Unternehmensführung sichtbar und akzeptabel zu machen. Es geht darum, anzuerkennen, dass Open Source ein strategischer Wegbereiter für digitale Souveränität, Prozessinnovation und ganz allgemein eine Quelle der Attraktivität und des guten Willens ist.

## Anerkannte Aktivitäten

Die Anerkannten Aktivitäten stehen im Mittelpunkt der Vorlage der GGI. In ihrer ursprünglichen Version sieht die GGI-Methodik fünf Anerkannte Aktivitäten pro Ziel vor, also insgesamt 25. Die Anerkannten Aktivitäten werden anhand der folgenden vordefinierten Abschnitte beschrieben:

* *Beschreibung*: eine Zusammenfassung des Themas, das die Aktivität behandelt und die Schritte zu deren Abschluß.

* *Gelegenheitsbeurteilung*: beschreibt, warum und wann es maßgeblich ist, diese Aktivität umzusetzen.

* *Fortschrittsbeurteilung*: beschreibt, wie der Fortschritt der Aktivität messbar ist und dessen Erfolg beurteilt wird.

* *Werkzeuge*: eine Liste der Technologien und Werkzeuge, die helfen diese Aktivität umzusetzen.

* *Empfehlungen*: von den GGI Teilnehmern gesammelte Tipps und bewährte Verfahren.

* *Hilfsmittel*: Verknüpfungen und Verweise, um mehr über das Thema zu lesen, das durch die Aktivität abgedeckt wird.

### Beschreibung

Dieser Abschnitt enthält eine allgemeine Beschreibung der Aktivität, eine Zusammenfassung des Themas, um den Zweck der Aktivität im Kontext des Open-Source-Ansatzes im Rahmen eines Ziels festzulegen.

### Gelegenheitsbeurteilung

Um den iterativen Ansatz zu strukturieren, gibt es zu jeder Aktivität einen Abschnitt "Gelegenheitsbewertung", dem eine oder mehrere Fragen beigefügt sind. Die Gelegenheitsbewertung konzentriert sich auf die Frage, warum es wichtig ist, diese Aktivität durchzuführen und welche Bedürfnisse sie behandelt. Die Bewertung der Gelegenheit hilft bei der Definition des erwarteten Aufwands und der benötigten Ressourcen sowie bei der Bewertung der Kosten und der erwarteten Amortisierung.

### Fortschrittsbeurteilung

Dieser Schritt konzentriert sich auf die Festlegung von Zielen, Leitungskennzahlen und auf die Bereitstellung von *Überprüfungspunkten*, die helfen, den Fortschritt der Aktivität zu bewerten. Kontrollpunkte werden vorgeschlagen, sie können dazu beitragen, einen Fahrplan für den Good-Governance-Prozess, seine Prioritäten und die Messung der Fortschritte festzulegen.

### Werkzeuge

Hier sind Werkzeuge aufgelistet, die bei der Durchführung der Aktivität oder eines bestimmten Schrittes der Aktivitäten helfen können. Die Tools stellen keine verbindlichen Empfehlungen dar und erheben auch keinen Anspruch auf Vollständigkeit, sondern sind Vorschläge oder Kategorien, die auf der Grundlage des bestehenden Kontexts ausgearbeitet werden können.

### Empfehlungen

Dieser Abschnitt wird regelmäßig mit den Rückmeldungen der Benutzer auf den neuesten Stand gebracht und jede Art von Empfehlung kann bei der Verwaltung der Aktivität helfen.

### Hilfsmittel

Es werden Hilfsmittel vorgeschlagen, um den Ansatz mit Hintergrundstudien, Referenzdokumenten, Veranstaltungen oder Online-Inhalten zu bereichern und den entsprechenden Ansatz für die Aktivität zu entwickeln. Die Hilfsmittelaufzählung ist nicht erschöpfend, sie ist Ausgangspunkt oder Vorschlag, um die Bedeutung der Aktivität je nach eigenem Kontext zu erweitern.

## Angepasste Aktivitäts-Scorecard

Angepasste Aktivitäts-Scorecards (CAS) sind leicht ausführlicher als Anerkannte Aktivitäten. Eine CAS enthält Einzelheiten, die zielgerichtet für die Organisation sind, die GGI umsetzen. Wie die CAS verwendet wird ist im Methodologie-Abschnitt beschrieben.

## Iteration

Iteration gehört auch zum Methodologie-Abschnitt.
