## Förderung bewährter Verfahrensweisen bei der Open-Source-Entwicklung

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/25>.

### Beschreibung

Bei dieser Aktivität geht es darum, bewährte Open-Source-Verfahren innerhalb der Entwicklungsteams zu definieren, aktiv zu fördern und umzusetzen.

Als Ausgangspunkt könnten die folgenden Themen bedacht werden:

* Benutzer- und Entwicklerdokumentation.

* Ordnungsgemäße Verwaltung des Projekts in einem öffentlich zugänglichen Repository.

* Förderung und Umsetzung der kontrollierten Wiederverwendung.

* Bereitstellung einer vollständigen und aktuellen Produktdokumentation.

* Einrichtungsverwaltung: Git-Workflows, Zusammenarbeitsmuster.

* Release-Management: frühes und häufiges Freigeben, stabile Versionen im Vergleich zu Entwicklungsversionen, usw.

OSS-Projekte haben einen besonderen, basarähnlichen Arbeitsstil. Um diese Zusammenarbeit und Denkweise zu ermöglichen und zu fördern, werden einige Verhaltensweisen empfohlen, die eine gemeinschaftliche und verteilte Entwicklung und Beiträge von Drittentwicklern erleichtern...

**Dokumente der Community**

Stellen Sie sicher, dass alle Projekte innerhalb des Unternehmens die folgenden Dokumente vorschlagen:

* README -- kurze Beschreibung des Projekts, wie man miteinander umgeht, Verknüpfungen zu Hilfsmitteln.

* Contributing -- Einführung für diejenigen, die etwas beitragen wollen.

* Code Of Conduct -- Was als Verhalten innerhalb der Gemeinschaft akzeptabel ist -- und was nicht.

* LICENSE -- die übliche Lizenz im Repository.

**bewährte Verfahren von REUSE**

[REUSE](https://reuse.software) ist eine Initiative der [Free Software Foundation Europe](https://fsfe.org/) zur Verbesserung der Wiederverwendung von Software und zur Vereinfachung der Einhaltung von OSS und Lizenzen.

### Gelegenheitsbeurteilung

Obwohl es stark vom Kenntnisstand des Teams über OSS abhängt, ist es immer von Vorteil, die Mitarbeiter zu schulen und Verfahren zu schaffen die diese Verhaltensweisen durchsetzen, sind immer von Vorteil. Dies ist umso wichtiger, wenn:

* die potenziellen Nutzer und Mitwirkenden nicht bekannt sind,

* die Entwickler nicht an die Open-Source-Entwicklung gewöhnt sind.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Das Projekt legt eine Liste mit bewährten Open-Source-Verfahren fest, die einzuhalten sind.

- [ ] Das Projekt überwacht seine Ausrichtung an bewährten Verfahren.

- [ ] Das Entwicklungsteam hat ein Bewusstsein für die Einhaltung von bewährten OSS-Verfahren entwickelt.

- [ ] Neue bewährte Verfahren werden regelmäßig ausgewertet und es wird versucht, sie umzusetzen.

### Werkzeuge

* Das [REUSE-Hilfswerkzeug](https://github.com/fsfe/reuse-tool) hilft dabei, ein Repository mit den bewährten Verfahren von [REUSE](https://reuse.software) in Einklang zu bringen. Es kann in viele Entwicklungsprozesse einbezogen werden, um den aktuellen Zustand zu bestätigen.

* [ScanCode](https://scancode-toolkit.readthedocs.io) bietet die Möglichkeit, alle Gemeinschafts- und Rechtsdokumente im Repository aufzulisten: siehe [Funktionsbeschreibung](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).

* GitHub hat eine nette Funktion, um [nach fehlenden Gemeinschaftsdokumenten zu suchen](https://docs.github.com/articles/viewing-your-community-profile). Sie ist auf der Seite Repository > "Insights" > "Community" zu finden. [Hier](https://github.com/borisbaldassari/alambic/community) ist ein Beispiel.

### Empfehlungen

* Die Liste der bewährten Verfahren hängt vom jeweiligen Umfeld und Anwendungsbereich des Programms ab und sollte regelmäßig im Sinne einer kontinuierlichen Verbesserung neu bewertet werden. Die Maßnahmen sollten überwacht und regelmäßig bewertet werden, um Fortschritte zu verfolgen.

* Schulung von Menschen über die Wiederverwendung von OSS (als Konsumenten) und Ökosystemen (als Beitragende).

* Setzen Sie REUSE.software wie in Aktivität #14 um.

* Richten Sie ein Verfahren zum Umgang mit rechtlichen Risiken im Zusammenhang mit Wiederverwendung und Beiträgen ein.

* Ermutigen Sie die Mitarbeiter ausdrücklich, zu Fremdprojekten beizutragen.

* Stellen Sie eine Vorlage oder offizielle Richtlinien für die Projektstruktur bereit.

* Richten Sie automatische Kontrollen ein, um sicherzustellen, dass alle Projekte die Richtlinien einhalten.

### Hilfsmittel

* [OW2's list of open source best practices](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) aus der Methodik zur Bewertung der Marktreifegrade.

* [REUSEs offizielle Website](https://reuse.software) mit Spezifikationen, Tutorial und FAQ.

* [Gemeinschaftsrichtlinien von GitHub](https://opensource.guide/).

* Ein Beispiel für [bewährte Verfahren für die Einrichtungsverwaltung mit GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).
