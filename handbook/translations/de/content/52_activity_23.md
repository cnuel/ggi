## Verwalten von Softwareabhängigkeiten

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/23>.

### Beschreibung

Ein Programm zur *Ermittlung von Abhängigkeiten* sucht nach den tatsächlich in der Codebasis verwendeten Abhängigkeiten. Infolgedessen muss die Organisation eine Liste bekannter Abhängigkeiten für ihre Codebasis erstellen und pflegen und die Entwicklung der ermittelten Anbieter beobachten.

Die Erstellung und Pflege einer Liste der bekannten Abhängigkeiten ist eine Voraussetzung für die Umsetzung:

* Überprüfung von geistigem Eigentum und Lizenzen: Einige Lizenzen können nicht gemischt werden, auch nicht in Form von Abhängigkeiten. Man muss die Abhängigkeiten kennen, um die damit verbundenen rechtlichen Risiken abschätzen zu können.

* Schwachstellenverwaltung: Die gesamte Software ist so schwach wie ihr schwächstes Teil: siehe das Beispiel der Heartbleed-Schwachstelle. Man muss seine Abhängigkeiten kennen, um die damit verbundenen Sicherheitsrisiken einschätzen zu können.

* Lebenszyklus und Nachhaltigkeit: Eine aktive Gemeinschaft für das Projekt Abhängigkeiten ist ein gutes Zeichen für Fehlerkorrekturen, Verbesserungen und neue Funktionen.

* Sorgfältige Auswahl der verwendeten Abhängigkeiten nach "Reifekriterien" - das Ziel ist die Verwendung von Open-Source-Komponenten, die sicher sind, mit einer gesunden und gut gepflegten Codebasis und einer lebendigen, aktiven und reaktiven Gemeinschaft, die externe Beiträge akzeptiert, usw.

### Gelegenheitsbeurteilung

Die Ermittlung und Verfolgung von Abhängigkeiten ist ein notwendiger Schritt, um die mit der Wiederverwendung von Code verbundenen Risiken zu minimieren. Darüber hinaus ist die Einrichtung von Werkzeugen und Verfahren zur Verwaltung von Software-Abhängigkeiten eine Voraussetzung für die ordnungsgemäße Verwaltung von Qualität, Vorschriften und Sicherheit.

Bedenken Sie die folgenden Fragen:

* Wie hoch ist das Risiko für das Unternehmen (Kosten, Ruf, usw.), wenn die Software beschädigt ist, angegriffen oder verklagt wird?

* Wird die Codebasis als kritisch für die Mitarbeiter, die Organisation oder das Unternehmen betrachtet?

* Was passiert, wenn eine Komponente, von der eine Anwendung abhängt, ihr Repository ändert?

Der kleinste und erste Schritt ist die Umsetzung eines Werkzeugs zur Softwarekompositionsanalyse (SCA). Für eine vollwertige SCA oder eine Abhängigkeitszuordnung kann die Unterstützung durch Fachberatungsunternehmen erforderlich sein.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Abhängigkeiten werden im gesamten intern entwickelten Code ermittelt.

- [ ] Abhängigkeiten werden im gesamten externen Code, der innerhalb des Unternehmens ausgeführt wird, festgestellt.

- [ ] Ein einfach einzurichtendes Verfahren zur Untersuchung der Softwarezusammensetzung oder zur Ermittlung von Abhängigkeiten steht Projekten zur Verfügung, um ihren kontinuierlichen Integrationsprozess zu ergänzen.

- [ ] Es werden Werkzeuge zur Abhängigkeitsprüfung eingesetzt.

### Werkzeuge

* [OWASP Dependency-Check](https://github.com/jeremylong/DependencyCheck): dependency-Check ist ein Werkzeug zur Software Composition Analysis (SCA), das versucht, öffentlich bekannte Schwachstellen in den Abhängigkeiten eines Projekts zu erkennen.

* [OSS Review Toolkit](https://oss-review-toolkit.org/): eine Reihe von Werkzeugen zur Unterstützung bei der Überprüfung von Open-Source-Software-Abhängigkeiten.

* [Fossa](https://github.com/fossas/fossa-cli): schnelle, portable und zuverlässige Abhängigkeitsprüfung. Unterstützt Lizenz- und Schwachstellenprüfungen. Sprachunabhängig; integriert sich in mehr als 20 Build-Systeme.

* [Software 360](https://projects.eclipse.org/projects/technology.sw360).

* [Eclipse Dash license tool](https://github.com/eclipse/dash-licenses): übernimmt eine Liste von Abhängigkeiten und fordert [ClearlyDefined](https://clearlydefined.io) auf, deren Lizenzen zu prüfen.

### Empfehlungen

* Führen Sie regelmäßig Kontrollen der Abhängigkeiten und Anforderungen an das geistige Eigentum durch, um rechtliche Risiken zu minimieren.

* Idealerweise integrieren Sie die Abhängigkeitsverwaltung in den kontinuierlichen Integrationsprozess, damit Probleme (neue Abhängigkeiten, Lizenzunverträglichkeiten) so schnell wie möglich erkannt und behoben werden können.

* Behalten Sie den Überblick über Schwachstellen im Zusammenhang mit Abhängigkeiten und halten Sie Benutzer und Entwickler auf dem Laufenden.

* Informieren Sie über die mit einer falschen Lizenzierung verbundenen Risiken.

* Schlagen Sie eine einfache Lösung für Projekte vor, um die Lizenzprüfung in ihrer Codebasis einzurichten.

* Informieren Sie über deren Bedeutung und helfen Sie den Projekten, sie in ihre CI-Systeme einzubauen.

* Richten Sie sichtbaren Leitungskennzahlen für abhängigkeitsbezogene Risiken ein.

### Hilfsmittel

* Vorhandene Seite der Gruppe [OSS-licenced OSS licence compliance tools](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).

* [The FOSSology Project](https://www.linuxfoundation.org/wp-content/uploads/lfcorp/files/lf_foss_compliance_fossology.pdf). Eine aktuelle Einführung in die FOSSologie und die Einhaltung der FOSS-Richtlinien durch die Linux Foundation.

* [Free and Open Source Software licence Compliance: Tools for Software Composition Analysis](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), von Philippe Ombredanne, nexB Inc.

* [Software Sustainability Maturity Model](http://oss-watch.ac.uk/resources/ssmm).

* [CHAOS](https://chaoss.community/): Community Health Analytics Open Source Software.
