## Verzeichnis der Open-Source-Kompetenzen und -Mittel

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/17>.

### Beschreibung

In jeder Phase ist es aus der Sicht des Managements nützlich, eine Zuordnung, ein Verzeichnis der Open-Source-Mittel, der Anlagen, der Nutzung und ihres Zustands sowie des möglichen Bedarfs und der verfügbaren Lösungen zu erstellen. Dazu gehört auch die Bewertung des erforderlichen Aufwands und der Fähigkeiten, um die Lücke zu schließen.

Diese Aktivität zielt darauf ab, eine Momentaufnahme der Open-Source-Situation innerhalb in der Organisation und auf dem Markt aufzunehmen und die Brücke zwischen beiden zu beurteilen.

* Bestandsaufnahme der OSS-Nutzung in der Software-Entwicklungskette sowie in den Softwareprodukten und -komponenten, die in der Produktion verwendet werden.

* Stellen Sie Open-Source-Technologien (Lösungen, Rahmen, innovative Funktionen) fest, die Ihren Bedürfnissen entsprechen und zur Verbesserung Ihres Prozesses beitragen könnten.

Nicht enthalten

* Identifizieren und qualifizieren Sie verwandte OSS-Ökosysteme und -Gemeinschaften. (Kulturziel)

* Feststellung von Abhängigkeiten von OSS-Bibliotheken und -Komponenten. (Vertrauensziel)

* Ermittlung der erforderlichen technischen (z. B. Sprachen, Rahmen, ...) und sozialen (z. B. Zusammenarbeit, Kommunikation) benötigt werden. (gehört zu den nächsten Aktivitäten: OSS-Kompetenzentwicklung und Open-Source-Softwareentwicklungsfähigkeiten)

### Gelegenheitsbeurteilung

Eine Bestandsaufnahme der verfügbaren Open-Source-Mittel, die zur Optimierung der Investitionen zu optimieren und Prioritäten bei der Kompetenzentwicklung zu setzen.

Diese Tätigkeit schafft die Voraussetzungen für eine Verbesserung der Entwicklungsproduktivität angesichts der Effizienz und Popularität von OSS-Komponenten, Entwicklungsprinzipien und -werkzeugen, insbesondere bei der Entwicklung moderner Anwendungen und Infrastrukturen.

- Dies kann eine Vereinfachung des Portfolios an OSS-Mitteln erfordern.

- Dies kann eine Umschulung des Personals erfordern.

- Dies ermöglicht die Feststellung von Bedürfnissen und fließt in Ihren IT-Fahrplan ein.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine umsetzbare Liste von OSS-Ressourcen "Wir nutzen", "Wir integrieren", "Wir produzieren", "Wir hosten" und die zugehörigen Fähigkeiten

- [ ] Wir sind dabei, die Effizienz durch den Einsatz modernster Methoden und Werkzeuge zu verbessern

- [ ] Wir haben OSS-Mittel identifiziert, die bisher nicht berücksichtigt wurden (die sich möglicherweise eingeschlichen haben und haben wir Bestandteile, um eine Regelung in diesem Bereich zu bestimmen?)

- [ ] Wir fordern neue Projekte auf, bestehende OSS-Mittel zu übernehmen oder wiederzuverwenden. (Kulturziel?)

- [ ] Wir haben eine einigermaßen sichere Vorstellung und ein Verständnis für den Umfang der OSS Nutzung in unserer Organisation

### Werkzeuge

Es gibt viele verschiedene Möglichkeiten, ein solches Verzeichnis zu erstellen. Eine Möglichkeit wäre OSS-Ressourcen in vier Arten einzuteilen:

- OSS, die wir verwenden: Software, die wir entweder in der Produktion oder in der Entwicklung einsetzen

- OSS, die wir integrieren: zum Beispiel OSS-Bibliotheken, die wir in eine maßgeschneiderte Anwendung integrieren

- OSS, die wir herstellen: zum Beispiel eine Bibliothek, die wir auf GitHub veröffentlicht haben, oder ein OSS-Projekt, das wir entwickeln oder zu dem wir regelmäßig beitragen.

- OSS, die wir hosten: OSS, die wir betreiben, um einen internen Dienst wie CRM, GitLab, nexus, usw. anzubieten. Eine Beispieltabelle würde wie folgt aussehen:

| Wir verwenden | Wir integrieren | Wir stellen her | Wir hosten | Fähigkeiten |
| --- | --- | --- | --- | --- |
| Firefox, <br/>OpenOffice, <br />
Postgresql | Bibliothek slf4j | Bibliothek YY auf GH | GitLab, <br />Nexus | Java, <br />Python |

Die gleiche Feststellung sollte für Fähigkeiten gelten

- Fähigkeiten und Erfahrungen durch die vorhandenen Teams

- Fähigkeiten und Erfahrungen die intern entwickelt oder erworben werden können (Schulung, Beratung, Ausprobieren)

- Fähigkeiten und Erfahrungen, die auf dem Markt oder durch Partnerschaften / Verträge gesucht werden müssen

### Empfehlungen

* Halten Sie alles einfach.

* Es handelt sich um eine vergleichsweise anspruchsvolle Übung, nicht um eine detaillierte Bestandsaufnahme für die Buchhaltungsabteilung.

* Diese Aktivität ist zwar ein guter Ausgangspunkt, muss aber nicht zu 100 % abgeschlossen sein, bevor Sie mit anderen Aktivitäten beginnen.

* Behandeln Sie Fragen, Ressourcen und Fähigkeiten im Zusammenhang mit **Softwareentwicklung** in Aktivität #42.

* Das Verzeichnis sollte alle Bereiche der IT abdecken: Betriebssysteme, Middlewares, DBMS, Systemadministration, Entwicklungs- und Testwerkzeuge, usw.

* Beginnen Sie damit, verwandte Gemeinschaften ausfindig zu machen: Es ist einfacher, Unterstützung und Rückmeldung für das Projekt zu erhalten, wenn man Sie bereits kennt.

### Hilfsmittel

* Einen hervorragenden Lehrgang auf [Free (/Libre), and Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), von Professor Dirk Riehle.
