## Open-Source-Communities unterstützen

Verknüpfung zum GitLab-Ticket: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/30>.

### Beschreibung

Bei dieser Aktivität geht es darum, mit institutionellen Vertretern der Open-Source-Welt in Kontakt zu treten.

Sie wird erreicht durch:

* Beitritt zu OSS-Stiftungen (einschließlich der finanziellen Kosten für die Mitgliedschaft).

* Unterstützung und Befürwortung der Tätigkeiten der Stiftungen.

Diese Aktivität beinhaltet, dass den Entwicklungs- und IT-Teams Zeit und Mittel für die Teilnahme an Open-Source-Communities zur Verfügung gestellt werden.

### Gelegenheitsbeurteilung

Open-Source-Communities stehen an der Spitze der Entwicklung des Open-Source-Ökosystems. Das Engagement in Open-Source-Communities hat mehrere Vorteile:

* es ist hilfreich, informiert und auf dem neuesten Stand zu bleiben,

* es verbessert das Image der eigenen Organisation,

* die Mitgliedschaft bringt Vorteile mit sich,

* sie gibt dem Open-Source-IT-Team zusätzliche Struktur und Motivation.

Die Kosten umfassen:

* Mitgliedsbeiträge,

* Personalzeit und ein gewisses Reisekostenbudget für die Teilnahme an Community-Aktivitäten,

* Überwachung des Engagements für das geistige Eigentum.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Die Organisation ist ein eingetragenes Mitglied einer Open-Source-Stiftung.

- [ ] Die Organisation beteiligt sich an der Steuerung.

- [ ] Software, die von der Organisation entwickelt wurde, wird an die Code-Basis einer Stiftung übermittelt bzw. wurde in diese aufgenommen.

- [ ] Die Mitgliedschaft wird auf den Websites der Organisation und der Community anerkannt.

- [ ] Es wurde eine Kosten-Nutzen-Bewertung der Mitgliedschaft durchgeführt.

- [ ] Es wurde eine Kontaktstelle für die Community benannt.

### Empfehlungen

* Schließen Sie sich einer Community an, die Ihrer Größe und Ihren Mitteln entspricht, z.B. einer Community, die Ihnen Gehör schenkt und in der Sie einen anerkannten Beitrag leisten können.

### Hilfsmittel

* Auf dieser [hilfreichen Seite](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) der Linux Foundation erfahren Sie, warum und wie Sie einer Open-Source-Community beitreten sollten.
