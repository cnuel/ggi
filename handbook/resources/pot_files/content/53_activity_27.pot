#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/53_activity_27.md:block 1 (header)
msgid "Belong to the open source community"
msgstr ""

#: ../content/53_activity_27.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/27>."
msgstr ""

#: ../content/53_activity_27.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/53_activity_27.md:block 4 (paragraph)
msgid "This activity is about developing among developers a feeling of belonging to the greater open source community. As with every community, people and entities have to participate and contribute back to the whole. It reinforces the links between practitioners and brings sustainability and activity to the ecosystem. On a more technical side, it allows choosing the priorities and roadmap of projects, improves the level of general knowledge and technical awareness."
msgstr ""

#: ../content/53_activity_27.md:block 5 (paragraph)
msgid "This activity covers the following:"
msgstr ""

#: ../content/53_activity_27.md:block 6 (unordered list)
msgid "**Identify events** worth attending. Connecting people, learning about new technologies and building a network are key factors to get the full benefits of open-source."
msgstr ""

#: ../content/53_activity_27.md:block 6 (unordered list)
msgid "Consider **foundation memberships**. Open-source foundations and organisations are a key component of the open-source ecosystem. They provide technical and organisational resources to projects, and are a good neutral place for sponsors to discuss common issues and solutions, or work on standards."
msgstr ""

#: ../content/53_activity_27.md:block 6 (unordered list)
msgid "Watch **working groups**. Working groups are neutral collaborative workspaces where experts interact on a specific domain like IoT, modelling or science. They are a very efficient and cost-effective mechanism to tackle common, albeit domain-specific concerns together."
msgstr ""

#: ../content/53_activity_27.md:block 6 (unordered list)
msgid "**Budget participation**. At the end of the journey, money is the enabler. Plan required expenses, allow people paid time for these activities, anticipate next moves, so the program doesn't have to stop after a few months short of funding."
msgstr ""

#: ../content/53_activity_27.md:block 7 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/53_activity_27.md:block 8 (paragraph)
msgid "Open source works best when done in relation with the open source community at large. It facilitates bug fixing, solution sharing, etc."
msgstr ""

#: ../content/53_activity_27.md:block 9 (paragraph)
msgid "It is also a good way for companies to show their support to open-source values. Communicating about the company's involvement is important both for the company's reputation and for the open-source ecosystem."
msgstr ""

#: ../content/53_activity_27.md:block 10 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/53_activity_27.md:block 11 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/53_activity_27.md:block 12 (unordered list)
msgid "A list of events people could attend is drafted."
msgstr ""

#: ../content/53_activity_27.md:block 12 (unordered list)
msgid "There is a monitoring of public talks given by team members."
msgstr ""

#: ../content/53_activity_27.md:block 12 (unordered list)
msgid "People can submit event participation requests."
msgstr ""

#: ../content/53_activity_27.md:block 12 (unordered list)
msgid "People can submit projects for sponsorship."
msgstr ""

#: ../content/53_activity_27.md:block 13 (header)
msgid "Recommendations"
msgstr ""

#: ../content/53_activity_27.md:block 14 (unordered list)
msgid "Survey people to get to know what events they like or would be the most beneficial for their work."
msgstr ""

#: ../content/53_activity_27.md:block 14 (unordered list)
msgid "Set up in-house communications (newsletter, resource centre, invitations…) so people know about the initiatives and can participate."
msgstr ""

#: ../content/53_activity_27.md:block 14 (unordered list)
msgid "Make sure that these initiatives can benefit various types of people (developers, administrators, support…), not only C-level executives."
msgstr ""

#: ../content/53_activity_27.md:block 15 (header)
msgid "Resources"
msgstr ""

#: ../content/53_activity_27.md:block 16 (unordered list)
msgid "[What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) An article by Michael Sweeney on clearcode.cc."
msgstr ""

#: ../content/53_activity_27.md:block 16 (unordered list)
msgid "[Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) An article by Velichka Atanasova from VMWare."
msgstr ""

#: ../content/53_activity_27.md:block 16 (unordered list)
msgid "[Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) A good read by Robert Kowalski from CloudBees."
msgstr ""

#: ../content/53_activity_27.md:block 16 (unordered list)
msgid "[7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) An article from Simon Phipps for InfoWorld."
msgstr ""

#: ../content/53_activity_27.md:block 16 (unordered list)
msgid "[Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) An article by Donna Benjamin from RedHat."
msgstr ""

#~ msgid "content/53_activity_27.md"
#~ msgstr ""
