#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/51_activity_20.md:block 1 (header)
msgid "Open source enterprise software"
msgstr ""

#: ../content/51_activity_20.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/20>."
msgstr ""

#: ../content/51_activity_20.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/51_activity_20.md:block 4 (paragraph)
msgid "This activity is about proactively selecting OSS solutions, either vendor or community-supported, in business-oriented areas. It may also cover defining preference policies for the selection of open source business application software."
msgstr ""

#: ../content/51_activity_20.md:block 5 (paragraph)
msgid "While open source software is most often used by IT professionals -- operating system, middleware, DBMS, system administration, development tools -- it has yet to be recognized in areas where business professionals are the primary users."
msgstr ""

#: ../content/51_activity_20.md:block 6 (paragraph)
msgid "The activity concerns areas such as: Office suites, Collaboration environments, User management, Workflow management, Customer relationship management, Email, e-Commerce, etc."
msgstr ""

#: ../content/51_activity_20.md:block 7 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/51_activity_20.md:block 8 (paragraph)
msgid "As open source becomes mainstream it reaches out well beyond operating systems and development tools, it increasingly finds its way into the upper layers of the information systems, well into the business applications. It is relevant to identify what OSS applications are successfully used to meet the needs of the organisation and how they can become an organisation's cost-saving preferred choice."
msgstr ""

#: ../content/51_activity_20.md:block 9 (paragraph)
msgid "The activity may bring some retraining and migration costs."
msgstr ""

#: ../content/51_activity_20.md:block 10 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/51_activity_20.md:block 11 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid "There is a list of recommended OSS solutions to address pending needs in business applications."
msgstr ""

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid "A preference policy for the selection of open source business application software is drafted."
msgstr ""

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid "Proprietary business applications in use are being evaluated against OSS equivalents."
msgstr ""

#: ../content/51_activity_20.md:block 12 (unordered list)
msgid "Procurement process and calls for proposals specify open source preference (if legally doable)."
msgstr ""

#: ../content/51_activity_20.md:block 13 (header)
msgid "Tools"
msgstr ""

#: ../content/51_activity_20.md:block 14 (paragraph)
msgid "Tools to map out software and business applications?"
msgstr ""

#: ../content/51_activity_20.md:block 15 (paragraph)
msgid "At this stage, we cannot think of any tool relevant or concerned by this Activity."
msgstr ""

#: ../content/51_activity_20.md:block 16 (header)
msgid "Recommendations"
msgstr ""

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid "Talk to colleagues, learn from what other companies comparable to yours do."
msgstr ""

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid "Visit local industry events to find out about OSS solutions and professional support."
msgstr ""

#: ../content/51_activity_20.md:block 17 (unordered list)
msgid "Try out community editions and community support first before committing to paid support plans."
msgstr ""

#: ../content/51_activity_20.md:block 18 (header)
msgid "Resources"
msgstr ""

#: ../content/51_activity_20.md:block 19 (unordered list)
msgid "[What is enterprise open source?](https://www.redhat.com/en/blog/what-enterprise-open-source): a quick read about enterprise-ready open source."
msgstr ""

#: ../content/51_activity_20.md:block 19 (unordered list)
msgid "[101 Open Source Apps to Help your Business Thrive](https://digital.com/creating-an-llc/open-source-business/): An indicative list of business-oriented open source solutions."
msgstr ""

#~ msgid "content/51_activity_20.md"
#~ msgstr ""
