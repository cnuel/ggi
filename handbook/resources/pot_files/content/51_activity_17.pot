#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/51_activity_17.md:block 1 (header)
msgid "Inventory of open source skills and resources"
msgstr ""

#: ../content/51_activity_17.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/17>."
msgstr ""

#: ../content/51_activity_17.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/51_activity_17.md:block 4 (paragraph)
msgid "At any stage, from a management perspective, it is useful to have a mapping, an inventory of open source resources, assets, usage and their status, as well as potential needs and available solutions. It also includes assessing the required effort and skills to fill the gap."
msgstr ""

#: ../content/51_activity_17.md:block 5 (paragraph)
msgid "This activity aims to take a snapshot of the open source situation within the organisation and on the market and evaluate the bridge between them."
msgstr ""

#: ../content/51_activity_17.md:block 6 (unordered list)
msgid "Inventory of OSS usage in the software development chain as well as in the software products and components used in production."
msgstr ""

#: ../content/51_activity_17.md:block 6 (unordered list)
msgid "Identify open source technologies (solutions, frameworks, innovative features) that could fit your needs and help improve your process."
msgstr ""

#: ../content/51_activity_17.md:block 7 (paragraph)
msgid "Not included"
msgstr ""

#: ../content/51_activity_17.md:block 8 (unordered list)
msgid "Identify and qualify related OSS ecosystems and communities. (Culture Goal)"
msgstr ""

#: ../content/51_activity_17.md:block 8 (unordered list)
msgid "Identify dependencies on OSS libraries and components. (Trust Goal)"
msgstr ""

#: ../content/51_activity_17.md:block 8 (unordered list)
msgid "Identify the technical (e.g. languages, frameworks..) and soft (e.g. collaboration, communication) skills needed. (belongs to next Activities: OSS competency growth and Open source software development skills)"
msgstr ""

#: ../content/51_activity_17.md:block 9 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/51_activity_17.md:block 10 (paragraph)
msgid "An inventory of available open source resources that will help optimise investment and prioritise skills development."
msgstr ""

#: ../content/51_activity_17.md:block 11 (paragraph)
msgid "This activity creates the conditions for improving development productivity given the efficiency and popularity of OSS components, development principles and tools, particularly in the development of modern applications and infrastructures."
msgstr ""

#: ../content/51_activity_17.md:block 12 (unordered list)
msgid "This may require simplifying the portfolio of OSS resources."
msgstr ""

#: ../content/51_activity_17.md:block 12 (unordered list)
msgid "This may require retraining personnel."
msgstr ""

#: ../content/51_activity_17.md:block 12 (unordered list)
msgid "This enables the identification of needs and feeds your IT roadmap."
msgstr ""

#: ../content/51_activity_17.md:block 13 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/51_activity_17.md:block 14 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/51_activity_17.md:block 15 (unordered list)
msgid "There is a workable list of OSS resources \"We use\", \"We integrate\", \"We produce\", \"We host\", and the related Skills"
msgstr ""

#: ../content/51_activity_17.md:block 15 (unordered list)
msgid "We are on a path to improve efficiency by using state of the art methods and tools."
msgstr ""

#: ../content/51_activity_17.md:block 15 (unordered list)
msgid "We have identified OSS resources unaccounted for until now (that may have been creeping in, and do we have elements to define policy in this domain?)"
msgstr ""

#: ../content/51_activity_17.md:block 15 (unordered list)
msgid "We request new projects to endorse or reuse existing OSS resources. (Culture Goal?)"
msgstr ""

#: ../content/51_activity_17.md:block 15 (unordered list)
msgid "We have a reasonably safe perception and understanding of the scope of OSS usage in our organisation."
msgstr ""

#: ../content/51_activity_17.md:block 16 (header)
msgid "Tools"
msgstr ""

#: ../content/51_activity_17.md:block 17 (paragraph)
msgid "There are many different ways to establish such inventory. One way would be to classify OSS resources into four categories:"
msgstr ""

#: ../content/51_activity_17.md:block 18 (unordered list)
msgid "OSS we use: software we use either in production or in development"
msgstr ""

#: ../content/51_activity_17.md:block 18 (unordered list)
msgid "OSS we integrate: for example, OSS libraries we integrate into a custom-made application"
msgstr ""

#: ../content/51_activity_17.md:block 18 (unordered list)
msgid "OSS we produce: for example, a library we have published on GitHub or an OSS project we develop or regularly contribute to."
msgstr ""

#: ../content/51_activity_17.md:block 18 (unordered list)
msgid "OSS we host: OSS we run to offer an in-house service such as a CRM, GitLab, nexus, etc. An example table would look like the following:"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "We use"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "We integrate"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "We produce"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "We host"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "Skills"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "Firefox, <br />OpenOffice, <br />Postgresql"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "Library slf4j"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "Library YY on GH"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "GitLab, <br />Nexus"
msgstr ""

#: ../content/51_activity_17.md:block 19 (table)
msgid "Java, <br />Python"
msgstr ""

#: ../content/51_activity_17.md:block 20 (paragraph)
msgid "The same identification should apply to skills"
msgstr ""

#: ../content/51_activity_17.md:block 21 (unordered list)
msgid "Skills & experiences available through the existing teams"
msgstr ""

#: ../content/51_activity_17.md:block 21 (unordered list)
msgid "Skills & experiences that could be developed or acquired internally (training, coaching, experiment)"
msgstr ""

#: ../content/51_activity_17.md:block 21 (unordered list)
msgid "Skills & experiences that need to be sought out on the market or through partnership / contracting"
msgstr ""

#: ../content/51_activity_17.md:block 22 (header)
msgid "Recommendations"
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "Keep things simple."
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "It's a relatively high-level exercise, not a detailed inventory for the accounting department."
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "While this activity is a good starting point, you do not need to have it 100% completed before launching other activities."
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "Handle issues, resources and skills related to **software development** in Activity #42."
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "The inventory should cover all IT categories: operating systems, middlewares, DBMS, system administration, development and testing tools, etc."
msgstr ""

#: ../content/51_activity_17.md:block 23 (unordered list)
msgid "Start identifying related communities: it's easier to get support and feedback from the project when they already know you."
msgstr ""

#: ../content/51_activity_17.md:block 24 (header)
msgid "Resources"
msgstr ""

#: ../content/51_activity_17.md:block 25 (unordered list)
msgid "An excellent course on [Free (/Libre), and Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), by Professor Dirk Riehle."
msgstr ""

#~ msgid "content/51_activity_17.md"
#~ msgstr ""
