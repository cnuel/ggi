#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/52_activity_22.md:block 1 (header)
msgid "Manage software vulnerabilities"
msgstr ""

#: ../content/52_activity_22.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/22>."
msgstr ""

#: ../content/52_activity_22.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/52_activity_22.md:block 4 (paragraph)
msgid "One's code is as secure as its least secure part. Recent cases (e.g. heartbleed[^heartbleed], equifax[^equifax]) have demonstrated the importance of checking vulnerabilities in parts of the code that are not directly developed by the entity. Consequences of exposures range from data leaks (with tremendous reputational impact) to ransomware attacks and business-threatening unavailability of services."
msgstr ""

#: ../content/52_activity_22.md:block 5 (paragraph)
msgid "Open source software is known to have better vulnerability management than proprietary software, mainly because:"
msgstr ""

#: ../content/52_activity_22.md:block 6 (unordered list)
msgid "More eyes are looking to find and fix problems on open code and processes."
msgstr ""

#: ../content/52_activity_22.md:block 6 (unordered list)
msgid "Open source projects fix vulnerabilities and release patches and new versions a lot faster."
msgstr ""

#: ../content/52_activity_22.md:block 7 (paragraph)
msgid "For example, a [study by WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) on proprietary software showed that 95% of the vulnerabilities found in their open source components had already released a fix at the time of the analysis. The issue, therefore, is to **better manage vulnerabilities both in the codebase and its dependencies**, no matter if they are closed or open source."
msgstr ""

#: ../content/52_activity_22.md:block 8 (paragraph)
msgid "In order to mitigate these risks, one has to set up an assessment program of its software assets and a vulnerability-checking process executed regularly. Implement tools that alert impacted teams, manage known vulnerabilities, and prevent threats from software dependencies."
msgstr ""

#: ../content/52_activity_22.md:block 9 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/52_activity_22.md:block 10 (paragraph)
msgid "Any company that uses software has to watch its vulnerabilities in:"
msgstr ""

#: ../content/52_activity_22.md:block 11 (unordered list)
msgid "its infrastructure (e.g. Cloud infrastructure, network infrastructure, data stores),"
msgstr ""

#: ../content/52_activity_22.md:block 11 (unordered list)
msgid "its business applications (HR, CRM tools, internal and customers-related data management),"
msgstr ""

#: ../content/52_activity_22.md:block 11 (unordered list)
msgid "its in-house code: e.g. the company's website, internal development projects, etc.,"
msgstr ""

#: ../content/52_activity_22.md:block 11 (unordered list)
msgid "and all direct and indirect software and services dependencies."
msgstr ""

#: ../content/52_activity_22.md:block 12 (paragraph)
msgid "The ROI of vulnerabilities is little known until something bad happens. One has to consider the consequences of a major data breach or unavailability of services to estimate the true cost of vulnerabilities."
msgstr ""

#: ../content/52_activity_22.md:block 13 (paragraph)
msgid "Similarly, a culture of secrecy and hiding for security-related issues inside the company has to be avoided at all costs. Instead, information about the state of vulnerability needs to be shared and discussed to find the best answers from the right people, from developers to c-level executives."
msgstr ""

#: ../content/52_activity_22.md:block 14 (paragraph)
msgid "The benefits of preventing cyber-attacks by carefully managing software vulnerabilities are manyfold:"
msgstr ""

#: ../content/52_activity_22.md:block 15 (unordered list)
msgid "Avoid reputational risks,"
msgstr ""

#: ../content/52_activity_22.md:block 15 (unordered list)
msgid "Avoid exploitation loss (DDoS, Ransomware, Time to rebuild an alternative IT system after an attack),"
msgstr ""

#: ../content/52_activity_22.md:block 15 (unordered list)
msgid "Comply with data protection regulations."
msgstr ""

#: ../content/52_activity_22.md:block 16 (paragraph)
msgid "Managing OSS software vulnerabilities is just a part of the larger cybersecurity process that globally addresses the security of the systems and services in the organisation."
msgstr ""

#: ../content/52_activity_22.md:block 17 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/52_activity_22.md:block 18 (paragraph)
msgid "There should be a dedicated person or team to monitor vulnerabilities and easy-to-use processes for developers to rely on. Vulnerabilities assessment is a standard part of the continuous integration process, and people are able to monitor the current state of risk in a dedicated dashboard."
msgstr ""

#: ../content/52_activity_22.md:block 19 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/52_activity_22.md:block 20 (unordered list)
msgid "Activity is covered when all in-house software and services are assessed and monitored for known vulnerabilities."
msgstr ""

#: ../content/52_activity_22.md:block 20 (unordered list)
msgid "Activity is covered when a dedicated tool and process is implemented in the software production chain to prevent the introduction of issues in the daily development routines."
msgstr ""

#: ../content/52_activity_22.md:block 20 (unordered list)
msgid "A person or team is responsible for evaluating CVE/vulnerability risk against exposure."
msgstr ""

#: ../content/52_activity_22.md:block 20 (unordered list)
msgid "A person or team is responsible for dispatching CVE/vulnerability to concerned people (SysOps, DevOps, developers, etc.)."
msgstr ""

#: ../content/52_activity_22.md:block 21 (header)
msgid "Tools"
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "GitHub tools"
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "GitHub provides guidelines and tools to secure code hosted on the platform. See [GitHub docs](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) for more information."
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "GitHub provides [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) to identify vulnerabilities in dependencies automatically."
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "[Eclipse Steady](https://eclipse.github.io/steady/) is a free, open source tool that analyses Java and Python projects for vulnerabilities and helps developers mitigate them."
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "[OWASP dependency-check](https://owasp.org/www-project-dependency-check/): an open source vulnerability scanner."
msgstr ""

#: ../content/52_activity_22.md:block 22 (unordered list)
msgid "[OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): an open source orchestrator able to collect security advisories for used dependencies from configured vulnerability data services."
msgstr ""

#: ../content/52_activity_22.md:block 23 (header)
msgid "Resources"
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "The [MITRE's vulnerability database](https://cve.mitre.org/) of CVEs. See also [NIST's security database](https://nvd.nist.gov/) of NVDs, and satellite resources like [CVE Details](https://www.cvedetails.com/)."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "Check also this new initiative from Google: the [open source Vulnerabilities](https://osv.dev/)."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "The OWASP working group publishes a list of vulnerabilities scanners [on their website](https://owasp.org/www-community/Vulnerability_Scanning_Tools), both from the commercial and open sources worlds."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "J. Williams and A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "[Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pages 3175–3215(2020)."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
msgid "[A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. There is also a [toolkit in development to implement the aforementioned dataset](https://sap.github.io/project-kb/)."
msgstr ""

#: ../content/52_activity_22.md:block 24 (unordered list)
#, fuzzy
msgid "[^heartbleed]: https://fr.wikipedia.org/wiki/Heartbleed"
msgstr "[^heartbleed]: https://fr.wikipedia.org/wiki/Heartbleed"

#: ../content/52_activity_22.md:block 24 (unordered list)
#, fuzzy
msgid "[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/"
msgstr "[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/"

#~ msgid "content/52_activity_22.md"
#~ msgstr ""
