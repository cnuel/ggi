#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/55_activity_16.md:block 1 (header)
msgid "Setup a strategy for corporate open source governance"
msgstr ""

#: ../content/55_activity_16.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/16>."
msgstr ""

#: ../content/55_activity_16.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/55_activity_16.md:block 4 (paragraph)
msgid "Defining a high-level strategy for open source governance within the company ensures the consistency and visibility of approaches towards both in-house usage and external contributions and involvement. It makes the company's communication more effective by offering a clear and established vision and leadership."
msgstr ""

#: ../content/55_activity_16.md:block 5 (paragraph)
msgid "The shift towards open source brings with it numerous benefits, as well as some duties and a change in the company's culture. It can impact business models and influence the manner in which an organisation presents its value and offer, and in its position towards its customers and competitors."
msgstr ""

#: ../content/55_activity_16.md:block 6 (paragraph)
msgid "This activity includes the following tasks:"
msgstr ""

#: ../content/55_activity_16.md:block 7 (unordered list)
msgid "Set up an OSS Officer, with (top) management sponsoring and backing."
msgstr ""

#: ../content/55_activity_16.md:block 7 (unordered list)
msgid "Set up and publish a clear roadmap for open source, with stated objectives and expected benefits."
msgstr ""

#: ../content/55_activity_16.md:block 7 (unordered list)
msgid "Make sure that all top-level management knows about it and acts in accordance with it."
msgstr ""

#: ../content/55_activity_16.md:block 7 (unordered list)
msgid "Promote OSS inside the company: encourage people to use it, foster in-house initiatives and level of knowledge."
msgstr ""

#: ../content/55_activity_16.md:block 7 (unordered list)
msgid "Promote OSS outside the company: through official statements and communication, and visible involvement in OSS initiatives."
msgstr ""

#: ../content/55_activity_16.md:block 8 (paragraph)
msgid "Defining, publishing and enforcing a clear and consistent strategy also helps buy-in from all people within the company and eases further initiatives from teams."
msgstr ""

#: ../content/55_activity_16.md:block 9 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/55_activity_16.md:block 10 (paragraph)
msgid "It's a good time to work on this activity if:"
msgstr ""

#: ../content/55_activity_16.md:block 11 (unordered list)
msgid "There is no coordinated effort from management, and open source is still seen as an ad-hoc solution."
msgstr ""

#: ../content/55_activity_16.md:block 11 (unordered list)
msgid "There are already in-house initiatives, but they don't penetrate up to the upper levels of management."
msgstr ""

#: ../content/55_activity_16.md:block 11 (unordered list)
msgid "The initiative was started some time ago but faces many obstacles, and still doesn't yield the expected results."
msgstr ""

#: ../content/55_activity_16.md:block 12 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/55_activity_16.md:block 13 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/55_activity_16.md:block 14 (unordered list)
msgid "There is a clear open source governance charter for the company. The charter should contain:"
msgstr ""

#: ../content/55_activity_16.md:block 14 (unordered list)
msgid "what to achieve,"
msgstr ""

#: ../content/55_activity_16.md:block 14 (unordered list)
msgid "who we do this for,"
msgstr ""

#: ../content/55_activity_16.md:block 14 (unordered list)
msgid "what the power of the strategist(s) is and what not."
msgstr ""

#: ../content/55_activity_16.md:block 14 (unordered list)
msgid "An open source roadmap is widely available and accepted throughout the company."
msgstr ""

#: ../content/55_activity_16.md:block 15 (header)
msgid "Recommendations"
msgstr ""

#: ../content/55_activity_16.md:block 16 (unordered list)
msgid "Set up a group of people and processes to define and monitor open source governance within the company."
msgstr ""

#: ../content/55_activity_16.md:block 16 (unordered list)
msgid "Ensure there is a clear commitment from the top-level management to the open source initiatives."
msgstr ""

#: ../content/55_activity_16.md:block 16 (unordered list)
msgid "Communicate about the open source strategy within the organisation, make it a major concern and a true corporate commitment."
msgstr ""

#: ../content/55_activity_16.md:block 16 (unordered list)
msgid "Ensure that the roadmap and strategy is well understood by everybody, from development teams to management and infrastructure staff."
msgstr ""

#: ../content/55_activity_16.md:block 16 (unordered list)
msgid "Communicate on its progress, so people know where the organisation is regarding its commitment. Publish regular updates and indicators."
msgstr ""

#: ../content/55_activity_16.md:block 17 (header)
msgid "Resources"
msgstr ""

#: ../content/55_activity_16.md:block 18 (unordered list)
msgid "[Checklist and references for Open Governance](https://opengovernance.dev/)."
msgstr ""

#: ../content/55_activity_16.md:block 18 (unordered list)
msgid "[L'open source comme enjeu de souveraineté numérique, by Cédric Thomas, OW2 CEO, Workshop at Orange Labs, Paris, January 28, 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (french only)."
msgstr ""

#: ../content/55_activity_16.md:block 18 (unordered list)
msgid "[A series of guides to manage open source within the enterprise, by the Linux Foundation](https://todogroup.org/guides/)."
msgstr ""

#: ../content/55_activity_16.md:block 18 (unordered list)
msgid "[A fine example of open source strategy document, by the LF Energy group](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)"
msgstr ""

#~ msgid "content/55_activity_16.md"
#~ msgstr ""
