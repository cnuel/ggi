#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/54_activity_33.md:block 1 (header)
msgid "Engage with open source vendors"
msgstr ""

#: ../content/54_activity_33.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/33>."
msgstr ""

#: ../content/54_activity_33.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/54_activity_33.md:block 4 (paragraph)
msgid "Secure contracts with open source vendors that provide software critical to you. Companies and entities that produce open source software need to thrive to provide maintenance and development of new features. Their specific expertise is required on the project, and the community of users relies on their continued business and contributions."
msgstr ""

#: ../content/54_activity_33.md:block 5 (paragraph)
msgid "Engaging with open source vendors takes several forms:"
msgstr ""

#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Subscribing support plans."
msgstr ""

#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Contracting local service companies."
msgstr ""

#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Sponsoring developments."
msgstr ""

#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Paying for a commercial licence."
msgstr ""

#: ../content/54_activity_33.md:block 7 (paragraph)
msgid "This activity implies considering open source projects as fully-featured products worth paying for, much like any proprietary products -- although usually far less expensive."
msgstr ""

#: ../content/54_activity_33.md:block 8 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/54_activity_33.md:block 9 (paragraph)
msgid "The objective of this activity is to ensure professional support of open source software used in the organisation. It has several benefits:"
msgstr ""

#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Continuity of service through timely bug fixes."
msgstr ""

#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Service performance through optimised installation."
msgstr ""

#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Clarification of the legal/commercial status of the software used."
msgstr ""

#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Access to early information."
msgstr ""

#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Stable budget forecast."
msgstr ""

#: ../content/54_activity_33.md:block 11 (paragraph)
msgid "The cost is obviously that of the support plans selected. Another cost might be to depart from bulk outsourcing to large systems integrators in favour of fine-grained contracting with expert SMEs."
msgstr ""

#: ../content/54_activity_33.md:block 12 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/54_activity_33.md:block 13 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr ""

#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Open source used in the organisation is backed by commercial support."
msgstr ""

#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Support plans for some open source projects have been contracted."
msgstr ""

#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Cost of open source support plans is a legitimate entry in the IT budget."
msgstr ""

#: ../content/54_activity_33.md:block 15 (header)
msgid "Recommendations"
msgstr ""

#: ../content/54_activity_33.md:block 16 (unordered list)
msgid "Whenever possible, find local expert SMEs."
msgstr ""

#: ../content/54_activity_33.md:block 16 (unordered list)
msgid "Beware of large systems integrators reselling third-party expertise (reselling support plans that expert open source SMEs actually provide)."
msgstr ""

#: ../content/54_activity_33.md:block 17 (header)
msgid "Resources"
msgstr ""

#: ../content/54_activity_33.md:block 18 (paragraph)
msgid "A couple of links illustrating the commercial reality of open source software:"
msgstr ""

#: ../content/54_activity_33.md:block 19 (unordered list)
msgid "[An investor's view of the community to business evolution of open source projects](https://a16z.com/2019/10/04/commercializing-open-source/)."
msgstr ""

#: ../content/54_activity_33.md:block 19 (unordered list)
msgid "[A quick read to understand commercial open source](https://www.webiny.com/blog/what-is-commercial-open-source)."
msgstr ""

#~ msgid "content/54_activity_33.md"
#~ msgstr ""
