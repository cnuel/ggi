#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/51_activity_19.md:block 1 (header)
msgid "Open source supervision"
msgstr ""

#: ../content/51_activity_19.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/19>."
msgstr ""

#: ../content/51_activity_19.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/51_activity_19.md:block 4 (paragraph)
msgid "This activity is about controlling the use of open source and ensuring open source software is proactively managed. This concerns several perspectives, be it to use OSS tools and business solutions, or to include OSS as components in own developments or modify a version of a software adapting it to its own needs, etc. It is also about identifying areas where open source has become a (sometimes covert) de facto solution and assessing its suitability."
msgstr ""

#: ../content/51_activity_19.md:block 5 (paragraph)
msgid "It may be necessary to clarify the following:"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "Is the required functionality provided?"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "Is there additional functionality provided that is not needed but is increasing complexity in the BUILD and RUN phases?"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "What does the licence require, what are the legal constraints?"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "How much does the decision make your organisation supplier-independent?"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "Does a support option, ready for your business needs, exist, and how much does it cost?"
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "TCO (Total Cost of Ownership)."
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "Does the management know about open source's advantages, e.g. beyond \"saving licence cost\"? Being comfortable with open source helps get the maximum benefit from working with project communities and vendors."
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "See if it makes sense to share development costs by giving one's developments to the community and all its implications, like licence compliance."
msgstr ""

#: ../content/51_activity_19.md:block 6 (unordered list)
msgid "Check for availability of community support or professional support."
msgstr ""

#: ../content/51_activity_19.md:block 7 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/51_activity_19.md:block 8 (paragraph)
msgid "Defining a decision process specifically directed at open source is a way to maximise its benefits."
msgstr ""

#: ../content/51_activity_19.md:block 9 (unordered list)
msgid "It avoids the uncontrolled creeping usage and hidden costs of OSS technologies."
msgstr ""

#: ../content/51_activity_19.md:block 9 (unordered list)
msgid "It leads to informed and OSS-aware strategic and organisational decisions."
msgstr ""

#: ../content/51_activity_19.md:block 10 (paragraph)
msgid "Costs: the activity may challenge and reconsider sub-optimal de facto use of open source as inefficient, risky, etc."
msgstr ""

#: ../content/51_activity_19.md:block 11 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/51_activity_19.md:block 12 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "OSS has become a comfortable option when selecting OSS is not seen as an exception or a dangerous choice."
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "OSS has become a \"mainstream\" option."
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "Key players are sufficiently convinced the open source solution has strategic advantages worth investing in."
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "It can be demonstrated that the TCO of the solution based on open source gives your organisation a higher value than the alternative."
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "There is an evaluation of how supplier independence saves money or potentially can save money in the future."
msgstr ""

#: ../content/51_activity_19.md:block 13 (unordered list)
msgid "There is an evaluation that solution independence reduces risks to be too expensive to change the solution (no closed data formats possible)."
msgstr ""

#: ../content/51_activity_19.md:block 14 (header)
msgid "Tools"
msgstr ""

#: ../content/51_activity_19.md:block 15 (paragraph)
msgid "At this stage, we cannot think of any tool relevant or concerned by this activity."
msgstr ""

#: ../content/51_activity_19.md:block 16 (header)
msgid "Recommendations"
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Proactively managing the use of open-source requires basic levels of awareness and understanding of open source fundamentals because they should be considered in any OSS decision."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Compare the needed functionality instead of looking for an alternative for a known closed source solution."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Make sure to have support and further development."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Regard the effects of the solution's licence on your organisation."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Convince all key players about the value of the advantages of open source, beyond \"saving licence cost\"."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "Be honest, do not exaggerate the open source solution's effect."
msgstr ""

#: ../content/51_activity_19.md:block 17 (unordered list)
msgid "In the decision-making process it is equally important to assess different open source solutions in order to avoid disappointment through wrong expectations, to make it clear what the organisation is required to do and all the advantages the openness of the solutions brings. This must be identified so the organisation can assess it for its own context."
msgstr ""

#: ../content/51_activity_19.md:block 18 (header)
msgid "Resources"
msgstr ""

#: ../content/51_activity_19.md:block 19 (unordered list)
msgid "[Top 5 Benefits of Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Sponsored blog, but still interesting, quick read."
msgstr ""

#: ../content/51_activity_19.md:block 19 (unordered list)
msgid "[Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): an IBM-sponsored look at OSS support costs."
msgstr ""

#~ msgid "content/51_activity_19.md"
#~ msgstr ""
