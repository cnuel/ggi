#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/52_activity_44.md:block 1 (header)
msgid "Run code reviews"
msgstr ""

#: ../content/52_activity_44.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/44>."
msgstr ""

#: ../content/52_activity_44.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/52_activity_44.md:block 4 (paragraph)
msgid "Code review is a routine task involving manual and/or automated review of an application's source code before releasing a product or delivering a project to the customer. In the case of open-source software, code review is more than just about catching errors opportunistically; it is an integrated approach to collaborative development carried out at the team level."
msgstr ""

#: ../content/52_activity_44.md:block 5 (paragraph)
msgid "Code reviews should apply to code developed in-house as well as to code reused from external sources, as it improves general confidence in code and reinforces ownership. It is also an excellent way to enhance global skills and knowledge within the team and foster team collaboration."
msgstr ""

#: ../content/52_activity_44.md:block 6 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/52_activity_44.md:block 7 (paragraph)
msgid "Code reviews are valuable whenever the organisation develops software or reuses external pieces of software. While being a standard step in the software engineering process, code reviews in the context of open-source bring specific benefits such as:"
msgstr ""

#: ../content/52_activity_44.md:block 8 (unordered list)
msgid "When publishing internal source code, verify adequate quality guidelines are respected."
msgstr ""

#: ../content/52_activity_44.md:block 8 (unordered list)
msgid "When contributing to an existing open source project, verify that guidelines of the targeted project are respected."
msgstr ""

#: ../content/52_activity_44.md:block 8 (unordered list)
msgid "The publicly available documentation is updated accordingly."
msgstr ""

#: ../content/52_activity_44.md:block 9 (paragraph)
msgid "It is also an excellent opportunity to share and enforce some of your company legal compliance policy rules, such as:"
msgstr ""

#: ../content/52_activity_44.md:block 10 (unordered list)
msgid "Never remove existing licence headers or copyrights found in reused open-source code."
msgstr ""

#: ../content/52_activity_44.md:block 10 (unordered list)
msgid "Do not copy & paste source code from Stack Overflow without prior permission from the legal team."
msgstr ""

#: ../content/52_activity_44.md:block 10 (unordered list)
msgid "Include the correct copyright line when required."
msgstr ""

#: ../content/52_activity_44.md:block 11 (paragraph)
msgid "Code reviews will bring trust and confidence to code. If people are not sure about the quality or potential risks of using a software product, they should conduct peer- and code- reviews."
msgstr ""

#: ../content/52_activity_44.md:block 12 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/52_activity_44.md:block 13 (paragraph)
msgid "The following **verification points** demonstrate progress in this Activity:"
msgstr ""

#: ../content/52_activity_44.md:block 14 (unordered list)
msgid "Open source code review is recognised as a necessary step."
msgstr ""

#: ../content/52_activity_44.md:block 14 (unordered list)
msgid "Open source code reviews are planned (either regularly or at critical moments)."
msgstr ""

#: ../content/52_activity_44.md:block 14 (unordered list)
msgid "A process for conducting open-source code reviews has been collectively defined and accepted."
msgstr ""

#: ../content/52_activity_44.md:block 14 (unordered list)
msgid "Open-source code reviews are a standard part of the development process."
msgstr ""

#: ../content/52_activity_44.md:block 15 (header)
msgid "Recommendations"
msgstr ""

#: ../content/52_activity_44.md:block 16 (unordered list)
msgid "Code review is a collective task that works better in a good collaborative environment."
msgstr ""

#: ../content/52_activity_44.md:block 16 (unordered list)
msgid "Do not hesitate to use existing tools and patterns from the open-source world, where code reviews have been a standard for years (decades)."
msgstr ""

#: ../content/52_activity_44.md:block 17 (header)
msgid "Resources"
msgstr ""

#: ../content/52_activity_44.md:block 18 (unordered list)
msgid "[What is Code Review?](https://openpracticelibrary.com/practice/code-review/): a didactic read on code review found on Red Hat's Open Practice Library."
msgstr ""

#: ../content/52_activity_44.md:block 18 (unordered list)
msgid "[Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): another interesting perspective on what code review is about."
msgstr ""

#~ msgid "content/52_activity_44.md"
#~ msgstr ""
