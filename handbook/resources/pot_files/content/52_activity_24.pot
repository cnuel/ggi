#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/52_activity_24.md:block 1 (header)
msgid "Manage key indicators"
msgstr ""

#: ../content/52_activity_24.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/24>."
msgstr ""

#: ../content/52_activity_24.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/52_activity_24.md:block 4 (paragraph)
msgid "This activity collects and monitors a set of indicators that inform day-to-day managerial decisions and strategic options concerning professionally managed open source software."
msgstr ""

#: ../content/52_activity_24.md:block 5 (paragraph)
msgid "Key metrics related to open source software form the backdrop of how well governance programmes are rolled out. The activity covers selecting a few indicators, publishing them to the teams and management, and sending regular updates on the initiative, e.g. via a newsletter or corporate news."
msgstr ""

#: ../content/52_activity_24.md:block 6 (paragraph)
msgid "This activity requires:"
msgstr ""

#: ../content/52_activity_24.md:block 7 (unordered list)
msgid "stakeholders to discuss and define the objectives of the program,"
msgstr ""

#: ../content/52_activity_24.md:block 7 (unordered list)
msgid "the implementation of a measurement and data collection tool connected to the development infrastructure,"
msgstr ""

#: ../content/52_activity_24.md:block 7 (unordered list)
msgid "the publication of at least one dashboard for the stakeholders and for all the people involved in the initiative."
msgstr ""

#: ../content/52_activity_24.md:block 8 (paragraph)
msgid "Indicators are based on data that must be collected from relevant sources. Fortunately, there are plenty of sources for open source software engineering. Examples include:"
msgstr ""

#: ../content/52_activity_24.md:block 9 (unordered list)
msgid "the development environment, the CI/CD production chain,"
msgstr ""

#: ../content/52_activity_24.md:block 9 (unordered list)
msgid "the HR department,"
msgstr ""

#: ../content/52_activity_24.md:block 9 (unordered list)
msgid "the testing and software composition analysis tools,"
msgstr ""

#: ../content/52_activity_24.md:block 9 (unordered list)
msgid "the repositories."
msgstr ""

#: ../content/52_activity_24.md:block 10 (paragraph)
msgid "Examples of indicators include:"
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Number of resolved dependencies, displayed by licence type."
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Number of outdated/vulnerable dependencies."
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Number of licencing/ip issues detected."
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Contributions made to external projects."
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Bug open time."
msgstr ""

#: ../content/52_activity_24.md:block 11 (unordered list)
msgid "Number of contributors on a component, number of commits, etc."
msgstr ""

#: ../content/52_activity_24.md:block 12 (paragraph)
msgid "This activity is about defining these requirements and measurement needs, and implementing a dashboard that shows in a simple and efficient manner the main indicators of the program."
msgstr ""

#: ../content/52_activity_24.md:block 13 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/52_activity_24.md:block 14 (paragraph)
msgid "Key indicators help understand and better manage the resources devoted to open source software, and measure the results in order to communicate effectively and reap the full benefits of the investment. By communicating broadly, more people can follow the initiative and will feel involved, ultimately making it an organisation-level concern and goal."
msgstr ""

#: ../content/52_activity_24.md:block 15 (paragraph)
msgid "While each activity has evaluation criteria that help answer questions about progress achieved, there is still a need for monitoring done with numbers and quantitative indicators."
msgstr ""

#: ../content/52_activity_24.md:block 16 (paragraph)
msgid "Whether in a small startup or a large global company, key metrics help keep teams focused and monitor performance. Metrics are crucial because they support decision-making and are the basis for monitoring decisions already taken."
msgstr ""

#: ../content/52_activity_24.md:block 17 (paragraph)
msgid "With simple and practical numbers and graphics, the whole organisation members will be able to follow and synchronise efforts regarding open source, making it a shared concern and action. This also allows the various actors to better enter the course, contribute to the project and get the overall benefits."
msgstr ""

#: ../content/52_activity_24.md:block 18 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/52_activity_24.md:block 19 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr ""

#: ../content/52_activity_24.md:block 20 (unordered list)
msgid "A list of metrics and how to collect them has been established."
msgstr ""

#: ../content/52_activity_24.md:block 20 (unordered list)
msgid "Tools to collect, store, process and display indicators are used."
msgstr ""

#: ../content/52_activity_24.md:block 20 (unordered list)
msgid "There is a generalised dashboard available to all participants that shows the progress made on the initiative."
msgstr ""

#: ../content/52_activity_24.md:block 21 (header)
msgid "Tools"
msgstr ""

#: ../content/52_activity_24.md:block 22 (unordered list)
msgid "[GrimoireLab](https://chaoss.github.io/grimoirelab) from Bitergia."
msgstr ""

#: ../content/52_activity_24.md:block 22 (unordered list)
msgid "[Alambic](https://alambic.io) from Castalia Solutions."
msgstr ""

#: ../content/52_activity_24.md:block 22 (unordered list)
msgid "Generic BI tools (elasticsearch, grafana, R/Python visualisations…) are a good fit too, when the proper connectors are setup according to the defined goals."
msgstr ""

#: ../content/52_activity_24.md:block 23 (header)
msgid "Recommendations"
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "Write down the objectives and roadmap of the open source Governance."
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "Communicate in-house about the actions and status of the initiative."
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "Involve people in the definition of KPIs, in order to make sure that"
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "they are well understood,"
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "they provide a complete view of the needs and"
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "they are considered and followed."
msgstr ""

#: ../content/52_activity_24.md:block 24 (unordered list)
msgid "Build at least one dashboard that can be displayed for everybody (e.g. on a screen in the room), with essential indicators to show the progress and overall situation."
msgstr ""

#: ../content/52_activity_24.md:block 25 (header)
msgid "Resources"
msgstr ""

#: ../content/52_activity_24.md:block 26 (unordered list)
msgid "The [CHAOSS community](https://chaoss.community/) has many good references and resources related to open source indicators."
msgstr ""

#: ../content/52_activity_24.md:block 26 (unordered list)
msgid "Check out metrics for [Project Attributes](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) from the OW2 Market Readiness Levels [methodology](https://www.ow2.org/view/MRL/Overview)."
msgstr ""

#: ../content/52_activity_24.md:block 26 (unordered list)
msgid "[A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) by Liz Laffan is an interesting reading about openness in open source projects."
msgstr ""

#: ../content/52_activity_24.md:block 26 (unordered list)
msgid "[Governance Indicators: A Users’ Guide](https://www.un.org/ruleoflaw/files/Governance%20Indicators_A%20Users%20Guide.pdf) is the UN's guide about governance indicators. Although it is applied to democracy, corruption and transparency of nations, the basics of measurement and indicators as applied to governance are well worth a read."
msgstr ""

#~ msgid "content/52_activity_24.md"
#~ msgstr ""
