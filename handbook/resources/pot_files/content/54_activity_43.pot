#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/54_activity_43.md:block 1 (header)
msgid "Open source procurement policy"
msgstr ""

#: ../content/54_activity_43.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/43>."
msgstr ""

#: ../content/54_activity_43.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/54_activity_43.md:block 4 (paragraph)
msgid "This activity is about implementing a process to select, acquire, purchase open source software and services. It is also about considering the actual cost of open source software and provisioning for it. OSS may be \"free\" at first sight, but it is not without internal and external costs such as integration, training, maintenance and support."
msgstr ""

#: ../content/54_activity_43.md:block 5 (paragraph)
msgid "Such policy requires that both open source and proprietary solutions are symmetrically considered when evaluating value for money as the optimum combination of the total cost of ownership and quality. Therefore, the IT Procurement department should actively and fairly consider open source options, while at the same time ensuring proprietary solutions are considered on an equal footing in purchasing decisions."
msgstr ""

#: ../content/54_activity_43.md:block 6 (paragraph)
msgid "Open source preference can be explicitly stated based on the intrinsic flexibility of the open source option when there is no significant overall cost difference between proprietary and open source solutions."
msgstr ""

#: ../content/54_activity_43.md:block 7 (paragraph)
msgid "Procurement departments must understand that companies offering support for OSS typically lack the commercial resources to participate in procurement competitions, and adapt their open source procurement policies and processes accordingly."
msgstr ""

#: ../content/54_activity_43.md:block 8 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/54_activity_43.md:block 9 (paragraph)
msgid "Several reasons justify the efforts to set up specific open source procurement policies:"
msgstr ""

#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Supply of commercial open source software and services is growing and cannot be ignored, and requires the implementation of dedicated procurement policies and processes."
msgstr ""

#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "There is a growing supply of highly competitive commercial open source business solutions for corporate information systems."
msgstr ""

#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Even after adopting a free-of-charge OSS component and integrating it into an application, internal or external resources must be provided to maintain that source code."
msgstr ""

#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Total Cost of Ownership (TCO) is often (although not necessarily) lower for FOSS solutions: no licence fees to pay when purchasing/upgrading, open market for service providers, option to provide some or all of the solution yourself."
msgstr ""

#: ../content/54_activity_43.md:block 11 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/54_activity_43.md:block 12 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr ""

#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "New call for proposals proactively request open source submissions."
msgstr ""

#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "Procurement department has a way to evaluate open source vs proprietary solutions."
msgstr ""

#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "A simplified procurement process for open source software and services has been implemented and documented."
msgstr ""

#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "An approval process drawing from cross-functional expertise has been defined and documented."
msgstr ""

#: ../content/54_activity_43.md:block 14 (header)
msgid "Recommendations"
msgstr ""

#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "\"Be sure to tap into the expertise of your IT, DevOps, cybersecurity, risk management, and procurement teams when creating the process.\" (from [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/))."
msgstr ""

#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "Competition law may require that \"open source\" not be specifically mentioned."
msgstr ""

#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "Select technology upfront then go to RFP for customisation and support services."
msgstr ""

#: ../content/54_activity_43.md:block 16 (header)
msgid "Resources"
msgstr ""

#: ../content/54_activity_43.md:block 17 (unordered list)
msgid "[Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): not new, but still a great read by our colleagues at OSS-watch in the UK. Check out the [slides](http://oss-watch.ac.uk/files/procurement.odp)."
msgstr ""

#: ../content/54_activity_43.md:block 17 (unordered list)
msgid "[5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): a recent piece on open source procurement with useful hints."
msgstr ""

#~ msgid "content/54_activity_43.md"
#~ msgstr ""
