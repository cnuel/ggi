# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR OW2 and others
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: ossgovernance@ow2.org\n"
"POT-Creation-Date: 2022-07-14 13:12+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#.  Translators: please keep all { and }, all commas and the pdf...= intact in your translations
#. type: Plain text
#: ../resources/latex/customisations.tex:22
msgid ""
"{ pdftitle={The GGI Body of Knowledge}, pdfauthor={The Good Governance "
"Initiative}, pdfsubject={Ideas and guidance to build an open-source "
"strategy.}, pdfkeywords={open-source, good governance, community} }"
msgstr ""

#. \fancyhead[CO,CE]{This is fancy}
#.  Translators: please keep the \copyright command as is in your translations
#. type: fancyfoot{#2}
#: ../resources/latex/customisations.tex:29
msgid "Copyright \\copyright 2020-2021 OW2 and others."
msgstr ""

#. type: title{#1}
#: ../resources/latex/customisations.tex:33
msgid "The GGI Handbook"
msgstr ""

#. type: author{#1}
#: ../resources/latex/customisations.tex:33
msgid "The GGI Working Group"
msgstr ""
