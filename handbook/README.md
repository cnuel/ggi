# Building the BoK

## Pre-requisites

The following packages are required to build the BoK. The commands provided are for debian family systems, but should be available on any modern *nix system.

- `xelatex` to build the PDF. On Debian you can install `texlive-xetex`.
- `pandoc` (version >=  2.x): `apt install pandoc`
- `python3`
- `python3 pip` module: `apt install python3-pip`
- `translate-toolkit` module: `apt install translate-toolkit`
- `po4a (po for all)` module: `apt install po4a`

Some specific language-related tooling may be necessary, for example `texlive-lang-german` for German hyphenation.

For translation-specific requirements, please see also the TRANSLATING file in the `translations` directory.

## Building the main version

To build the handbook from the markdown files stored in the content directory, simply go to the `handbook/` directory and execute:

```
$ bash scripts/convert_handbook.sh -w web_out/
```

It will create a virtual environment, install all required dependencies, produce the PDF version of the handbook (with nice pdf features like links and toc) and optionally (`-w|--write-web=path`) produce the HTML export in the specified directory.

## Building the paperback version

To build the *paperback* version of the handbook from the markdown files stored in the content directory, simply go to the `handbook/` directory and execute:

```
$ bash scripts/paperback_convert_handbook.sh
```

Note that this version is specificly designed for *printed* versions of the handbook. 

## Building the translations

The content files used for the translations of the handbook are stored in `translations/[lang]/`. To build the handbook for a specific language, go to the language directory and execute the build script with the `-l|--language [LANG]` option:

```
cd translations/de
bash ../../scripts/convert_handbook.sh -l de
```

This will build the handbook pdf in the current, language-specific directory.
