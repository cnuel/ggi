## Open source enabling innovation

Activity ID: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_36.md).

### Description 

> Innovation is the practical implementation of ideas that result in the introduction of new goods or services or improvement in offering goods or services.
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

Open source can be a key factor for innovation through diversity, collaboration and a fluent exchange of ideas. People from different backgrounds and domains may have different perspectives and provide new, improved or even disruptive answers to known problems. One can enable innovation by listening to different views and actively promoting open collaboration on projects and topics.

Similarly, participating in the elaboration and implementation of open standards is a great promoter of good practices and ideas to improve the company's daily work. It also allows the entity to drive and influence innovation to where and what it needs, and enhances its global visibility and reputation.

Through innovation, open source makes it possible not only to transform the goods or services that your company markets, but also to create or modify the whole ecosystem in which your company wants to thrive.  

As an example, by releasing Android as open source, Google is inviting hundreds of thousands of companies to build up their own services based upon this open source technology. Google is thus creating a whole ecosystem from which all participants could benefit. Of course, very few companies are powerful enough to create an ecosystem by their own decision. But there are many examples of alliances between companies to create such an ecosystem.  


### Opportunity Assessment 

It is important to assess the position of your company compared with its competitors and its partners and customers because it would often be risky for a company to drift too far away from the standards and technologies used by its customers, partners and competitors. Innovation obviously means being different, but what differs should not represent too large a scope; otherwise, your company would not benefit from the software developments made by the other companies of the ecosystem and from the business momentum the ecosystem provides.   


### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] The technologies -- and open source communities that develop them -- that have an impact on the business have been identified.
- [ ] The progress and publications of these open source communities are monitored -- I am even aware of their strategy before the releases are made public.
- [ ] Employees of the organisation are members of (some of) these open source communities and influence their roadmaps and technical choices by contributing lines of codes and participating in the governance bodies of these communities.


### Recommendations 

Out of all the technologies that are necessary to run your business, you should identify:
* the technologies that could be the same as your competitors,
* the technologies that should be specific to your company.

Stay up-to-date on emerging technologies. Open source has been driving innovation for the last decade, and many day-to-day powerful tools come from there (think of Docker, Kubernetes, Apache Big Data projects, or Linux). No need to know everything about everything, but one should know enough of the state of the art to identify interesting new trends.

Allow, and encourage, people to submit innovative ideas, and to bring them forward. If possible, spend resources on these initiatives and make them grow. Rely on people's passion and will to create and foster emerging ideas and trends.

### Resources 

* [4 innovations we owe to open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
* [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), from Professor Dirk Riehle.
* [Open source technology, enabling innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).
* [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
* [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
* [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
