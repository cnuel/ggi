# Introduction

This document introduces a methodology to implement professional management of open source software in an organisation. It addresses the need to use open source software properly and fairly, safeguard the company from technical, legal and IP threats, and maximise the advantages of open source. Wherever an organisation stands on these topics this document proposes guidance and ideas to move forward and make your journey a success. 

## Context

Most large end-users and systems integrators already use Free and Open-Source Software (FOSS) either in their information systems or product and service divisions. Open source compliance has become an ever-growing concern, and many large companies have established compliance officers. However, while sanitising a company's open-source production chain – which is what compliance is about – is fundamental, users *must* give back to communities and contribute to the sustainability of the open-source ecosystem. We see open source governance encompassing the whole ecosystem, engaging with local communities, nurturing a healthy relationship with open source software vendors and service specialists. This takes compliance to the next level, and this is what open source _good_ governance is about. 

This initiative goes beyond compliance and liability. It is about building awareness in communities of end-users (often software developers themselves) and systems integrators, and developing mutually beneficial relationships within the European FOSS ecosystem.

OSS Good Governance enables organisations of all types --  companies, small and large, city councils, universities, associations, etc. -- maximise the benefits derived from open source by helping them align people, processes, technology and strategy. And in this area, that of maximising the advantages of open source, especially in Europe, everyone is still learning and innovating, with nobody knowing where they actually stand regarding state of the art in the domain.

This initiative aims to help organisations achieve these goals with:
* A structured catalog of **activities**, a roadmap for the implementation of professional management of open source software.
* A **management tool** to define, monitor, report and communicate about progress.
* A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm.
* **Guidance** and a range of **curated references** about open-source licensing, best practices, training, and ecosystem engagement to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership.

This guide has been developed with the following requirements in mind:
* Any type of organisation is covered: from SMEs to large companies and not-for-profit organisations, from local authorities (e.g. town councils) to large institutions (e.g. European or governmental institutions). The framework provides building blocks for a strategy and hints for its realisation, but *how* the activities are executed depends entirely on the program's context and is up to the program manager. It may prove helpful to look for consulting services and to exchange with peers.
* No assumption is made about the level of technical knowledge within the organisation or the domain of activity. For example, some organisations will need to set up a complete training curriculum, while others might simply propose ad-hoc material to the teams.

Some activities will not be relevant to all situations, but the whole framework still provides a comprehensive roadmap and paves the way for tailored strategies.


## About the Good Governance Initiative

At OW2, an initiative is a joint effort to address a market need. The [OW2 OSS Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) proposes a methodological framework to implement professional management of open source software within organisations.

The Good Governance initiative is based on a comprehensive model inspired by the popular Abraham Maslow's hierarchy of human needs and motivations, as illustrated by the picture below. 

![Maslow and the GGI](resources/images/ggi_maslow.png)

Through ideas, guidelines and activities the Good Governance initiative provides a blueprint for the implementation of organisational entities tasked with professional management of open source software, what is also called OSPO (for Open Source Program Offices). The methodology is also a management system to define priorities, and monitor and share progress.

As they implement the OSS Good Governance methodology, organisations will enhance their skills in a number of directions, including:

* **using** open source software properly and safely within the company to improve software reuse and maintainability and software development velocity;
* **mitigating** the legal and technical risks associated with external code and collaboration;
* **identifying** required training for teams, from developers to team leaders and managers, so everybody shares the same vision;
* **prioritizing** goals and activities, to develop an efficient open source strategy;
* **communicating** efficiently within the company and to the external world to make the most off the open source strategy;
* **improving** the organisation's competitiveness and attractiveness for top open source talents.


## About the OSPO Alliance

The **OSPO Alliance** was launched by a coalition of leading European open source non-profit organizations, including OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code, with a mission to grow awareness for open source in Europe and globally and to promote the structured and professional management of open source by companies and administrations.

While the OW2 OSS Good Governance initiative is focused on developing a management methodology, the OSPO Alliance has the broader goal to help companies, particularly in non-technology sectors, and public institutions discover and understand open source, start benefiting from it across their activities and grow to host their own OSPOs. 

The OSPO Alliance has established the **OSPO.Zone** website hosted at https://ospo.zone. Based on the OW2 Good Governance Initiative, OSPO.Zone is a repository for a comprehensive set of resources for corporations, public institutions, and research and academic organizations. OSPO.Zone enables the Alliance to connect with OSPOs across Europe and the world as well as with supportive community organizations. It encourages best practices and fosters contribution to the sustainability of the open source ecosystem. Check out the [OSPO Zone](https://ospo.zone) website for a quick overview of complementary frameworks of IT management best practices.

The [OSPO Zone](https://ospo.zone) website is also the place where we collect feedback about the initiative and its content (e.g. activities, body of knowledge) from the community at large.

## Contributors

The following great people have contributed to the Good Governance Initiative:

* Frédéric Aatz (Microsoft France)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (Ville de Paris)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Max Mehl (Free Software Foundation Europe)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business Services)
* Cédric Thomas, our master of ceremony (OW2)
* Nicolas Toussaint (Orange Business Services)


## Licence

This work is licenced under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) licence (CC-BY 4.0). From the Creative Commons website:

> You are free to:
> * Share it — copy and redistribute the material in any medium or format
> * Adapt it — remix, transform, and build upon the material
>
> for any purpose, even commercially.
>
> As long as you give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

All content is Copyright: 2020-2021 OW2 & The Good Governance Initiative participants.

