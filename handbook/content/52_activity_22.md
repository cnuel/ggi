## Manage software vulnerabilities

Activity ID: [GGI-A-22](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_22.md).

### Description 

One's code is as secure as its least secure part. Recent cases (e.g. heartbleed[^heartbleed], equifax[^equifax]) have demonstrated the importance of checking vulnerabilities in parts of the code that are not directly developed by the entity. Consequences of exposures range from data leaks (with tremendous reputational impact) to ransomware attacks and business-threatening unavailability of services.

Open source software is known to have better vulnerability management than proprietary software, mainly because: 
* More eyes are looking to find and fix problems on open code and processes.
* Open source projects fix vulnerabilities and release patches and new versions a lot faster.

For example, a [study by WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) on proprietary software showed that 95% of the vulnerabilities found in their open source components had already released a fix at the time of the analysis. The issue, therefore, is to **better manage vulnerabilities both in the codebase and its dependencies**, no matter if they are closed or open source.

In order to mitigate these risks, one has to set up an assessment program of its software assets and a vulnerability-checking process executed regularly. Implement tools that alert impacted teams, manage known vulnerabilities, and prevent threats from software dependencies.


### Opportunity Assessment 

Any company that uses software has to watch its vulnerabilities in:
* its infrastructure (e.g. Cloud infrastructure, network infrastructure, data stores),
* its business applications (HR, CRM tools, internal and customers-related data management), 
* its in-house code: e.g. the company's website, internal development projects, etc.,
* and all direct and indirect software and services dependencies.

The ROI of vulnerabilities is little known until something bad happens. One has to consider the consequences of a major data breach or unavailability of services to estimate the true cost of vulnerabilities. 

Similarly, a culture of secrecy and hiding for security-related issues inside the company has to be avoided at all costs. Instead, information about the state of vulnerability needs to be shared and discussed to find the best answers from the right people, from developers to c-level executives.

The benefits of preventing cyber-attacks by carefully managing software vulnerabilities are manyfold:
* Avoid reputational risks,
* Avoid exploitation loss (DDoS, Ransomware, Time to rebuild an alternative IT system after an attack),
* Comply with data protection regulations.

Managing OSS software vulnerabilities is just a part of the larger cybersecurity process that globally addresses the security of the systems and services in the organisation. 


### Progress Assessment 

There should be a dedicated person or team to monitor vulnerabilities and easy-to-use processes for developers to rely on. Vulnerabilities assessment is a standard part of the continuous integration process, and people are able to monitor the current state of risk in a dedicated dashboard.

The following **verification points** demonstrate progress in this Activity:

- [ ] Activity is covered when all in-house software and services are assessed and monitored for known vulnerabilities.
- [ ] Activity is covered when a dedicated tool and process is implemented in the software production chain to prevent the introduction of issues in the daily development routines.
- [ ] A person or team is responsible for evaluating CVE/vulnerability risk against exposure.
- [ ] A person or team is responsible for dispatching CVE/vulnerability to concerned people (SysOps, DevOps, developers, etc.).


### Tools 

* GitHub tools
  - GitHub provides guidelines and tools to secure code hosted on the platform. See [GitHub docs](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) for more information.
  - GitHub provides [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) to identify vulnerabilities in dependencies automatically.
* [Eclipse Steady](https://eclipse.github.io/steady/) is a free, open source tool that analyses Java and Python projects for vulnerabilities and helps developers mitigate them.
* [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): an open source vulnerability scanner.
* [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): an open source orchestrator able to collect
security advisories for used dependencies from configured vulnerability data services.


### Resources 

* The [MITRE's vulnerability database](https://cve.mitre.org/) of CVEs. See also [NIST's security database](https://nvd.nist.gov/) of NVDs, and satellite resources like [CVE Details](https://www.cvedetails.com/). 
* Check also this new initiative from Google: the [open source Vulnerabilities](https://osv.dev/).
* The OWASP working group publishes a list of vulnerabilities scanners [on their website](https://owasp.org/www-community/Vulnerability_Scanning_Tools), both from the commercial and open sources worlds.
* J. Williams and A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012.
* [Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pages 3175–3215(2020).
* [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. There is also a [toolkit in development to implement the aforementioned dataset](https://sap.github.io/project-kb/).


[^heartbleed]: https://fr.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
