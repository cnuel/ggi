## Engage with open source projects

Activity ID: [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_29.md).

### Description 

This activity is about committing significant contributions to some OSS projects that are important to you. Contributions are scaled up and committed at the organisation level (not personal level as in #26). They can take several forms, from direct funding to resources allocation (e.g. people, servers, infrastructure, communication, etc.), as long as they benefit the project or ecosystem sustainably and efficiently. 

This activity is a follow-up on activity #26 and brings open source projects' contributions to the level of the organisation, making them more visible, powerful, and beneficial. In this activity, the contributions are supposed to bring a substantial, long-term improvement to the OSS project: e.g., a developer or team who develops a much-wanted new feature, infrastructure assets, servers for a new service, take-over of the maintenance of a widely used branch.

The idea is to set aside a percentage of resources to sponsor open source developers that write and maintain libraries or projects that we use. 

This activity implies to have a mapping of open source software used, and an evaluation of their criticality to decide which one to support.


### Opportunity Assessment 

> If every company using open source contributed at least a little, we would have a healthy ecosystem. https://news.ycombinator.com/item?id=25432248

Supporting projects helps ensure their sustainability and provides access to information, even maybe help influence and prioritise some developments (although this should not be the main reason for supporting projects).

Potential benefits of this activity: ensuring bug reports are prioritised and developments are integrated into the stable version.
Possible costs associated with the activity: committing time to projects, cash commitment.

### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] Beneficiary project identified.
- [ ] Support option decided, such as direct monetary contribution or code contribution.
- [ ] Task leader appointed.
- [ ] Some contribution has happened.
- [ ] Result of contribution has been evaluated.

Verification points borrowed from OpenChain [self certification](https://certification.openchainproject.org/) questionnaire:
- [ ] We have a policy for contribution to open source projects on behalf of the organisation.
- [ ] We have a documented procedure governing open source contributions.
- [ ] We have a documented procedure for making all Software Staff aware of the open source contribution policy.


### Tools 

Some organisations offer mechanisms for funding open source projects (it could be convenient if your target project is in their portfolios).
* [Open Collective](https://opencollective.com/).
* [Software Freedom Conservancy](https://sfconservancy.org/).
* [Tidelift](https://tidelift.com/).

### Recommendations 

* Concentrate on projects that are critical for the organisation: these are the projects you most want to help with your contributions.
* Target community projects.
* This activity requires a minimum familiarity with a target project.


### Resources 

* [How to support open source projects now](https://sourceforge.net/blog/support-open-source-projects-now/):  A short page with ideas on funding open source projects.
* [Sustain OSS: a space for conversations about sustaining open source](https://sustainoss.org)
