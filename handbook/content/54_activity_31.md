## Publicly assert use of open source

Activity ID: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_31.md).

### Description 

This Activity is about acknowledging the use of OSS in an information system, in applications and in new products.
* Providing success stories.
* Presenting at events.
* Funding participation to events.


### Opportunity Assessment 

It is now generally accepted that most information systems run on OSS, and that new applications are for the most part made by reusing OSS. 

The main benefit of this activity is to create a level playing field between OSS and proprietary software, to make sure OSS is paid equal attention to and managed just as professionally as proprietary software. 

A side benefit is that it greatly helps raise the profile of the OSS ecosystem and, since OSS users are identified as "innovators" it also enhances the attractiveness of the organisation.


### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] Commercial open source vendors are granted authorization to use the organisation's name as customer reference.
- [ ] Contributors are allowed to do so and express themselves under the organisation's name.
- [ ] Use of OSS is openly mentioned in the IT department annual report.
- [ ] There is no obstacle to the organisation explaining their use of OSS in the media (interviews, OSS and industry events, etc.).


### Recommendations 

* The objective of this activity is not for the organisation to become an OSS activism body, but to make sure there is no obstacle to the public recognising its use of OSS.


### Resources 

* Example of [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) publicly asserting their use of OpenStack
