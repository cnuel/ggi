## Setup a strategy for corporate open source governance

Activity ID: [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_16.md).

### Description 

Defining a high-level strategy for open source governance within the company ensures the consistency and visibility of approaches towards both in-house usage and external contributions and involvement. It makes the company's communication more effective by offering a clear and established vision and leadership.

The shift towards open source brings with it numerous benefits, as well as some duties and a change in the company's culture. It can impact business models and influence the manner in which an organisation presents its value and offer, and in its position towards its customers and competitors. 

This activity includes the following tasks:

* Set up an OSS Officer, with (top) management sponsoring and backing.
* Set up and publish a clear roadmap for open source, with stated objectives and expected benefits.
* Make sure that all top-level management knows about it and acts in accordance with it.
* Promote OSS inside the company: encourage people to use it, foster in-house initiatives and level of knowledge.
* Promote OSS outside the company: through official statements and communication, and visible involvement in OSS initiatives.

Defining, publishing and enforcing a clear and consistent strategy also helps buy-in from all people within the company and eases further initiatives from teams. 

### Opportunity Assessment 

It's a good time to work on this activity if:

* There is no coordinated effort from management, and open source is still seen as an ad-hoc solution. 
* There are already in-house initiatives, but they don't penetrate up to the upper levels of management.
* The initiative was started some time ago but faces many obstacles, and still doesn't yield the expected results.

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] There is a clear open source governance charter for the company.
  The charter should contain:
  * what to achieve,
  * who we do this for,
  * what the power of the strategist(s) is and what not.
- [ ] An open source roadmap is widely available and accepted throughout the company.

### Recommendations 

* Set up a group of people and processes to define and monitor open source governance within the company.
* Ensure there is a clear commitment from the top-level management to the open source initiatives.
* Communicate about the open source strategy within the organisation, make it a major concern and a true corporate commitment.
* Ensure that the roadmap and strategy is well understood by everybody, from development teams to management and infrastructure staff.
* Communicate on its progress, so people know where the organisation is regarding its commitment. Publish regular updates and indicators.

### Resources 

* [Checklist and references for Open Governance](https://opengovernance.dev/).
* [L'open source comme enjeu de souveraineté numérique, by Cédric Thomas, OW2 CEO, Workshop at Orange Labs, Paris, January 28, 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (french only).
* [A series of guides to manage open source within the enterprise, by the Linux Foundation](https://todogroup.org/guides/).
* [A fine example of open source strategy document, by the LF Energy group](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)
