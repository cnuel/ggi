# Organisation

## Terminology

The OSS Good Governance methodology blueprint is structured around four key concepts: Goals, Canonical Activities, Customised Activity Scorecards and Iteration.

* **Goals**: A Goal is a set of activities associated with a common area of concern, there are five Goals: Usage Goal, Trust Goal, Culture Goal, Engagement Goal and Strategy Goal. Goals can be achieved independently, in parallel, and iteratively refined through Activities.
* **Canonical Activities**: within a Goal, an Activity addresses a single concern or topic of development -- such as Managing legal compliance -- that can be used as an incremental step towards the program's objectives. The complete set of Activities as defined by the GGI are called the Canonical Activities. 
* **Customised Activity Scorecard (CAS)**: To implement GGI in a given organisation, the Canonical Activities must be adapted to the specifics of the context, thus building a set of Customised Activity Scorecards. The Customised Activity Scorecard describes how the activity will be implemented in the context of the organisation and how progress will be monitored.
* **Iteration**: The OSS Good Governance is a management system and as such requires periodic assessment, review and revision. Think of the accounting system in an organisation, it's an on-going process with at least one annual check point, the balance sheet; in the same way, the OSS Good Governance process requires at least an annual review, however reviews can be partial or more frequent depending on the Activities.

## Goals

The Canonical Activities defined by the GGI are organised in Goals. Each Goal addresses a specific area of progress within the process. From Usage to Strategy, Goals cover issues related to all stakeholders, from development teams up to C-level management.
* **Usage** Goal: This Goal covers the basic steps in using open source software. Activities related to the Usage Goal cover the first steps through an open source program, by identifying how efficiently open source is used and what it brings to the organisation. It includes training and knowledge management, producing inventories of existing open source already used in-house, and presents some open source concepts that can be used throughout the process.
* **Trust** Goal: This Goal is concerned about using open source securely. The Trust Goal deals with legal compliance, dependency and vulnerability management and generally aims to build confidence in how the organisation uses and manages open source.  
* **Culture** Goal: The cultural objective includes activities aimed at making teams comfortable with open source, individually participating in collaborative activities, understanding and implementing open source best practices. This objective fosters a sense of belonging to the open source community among individuals.
* **Engagement** Goal: This goal is to engage with the open source ecosystem at the corporate level. Human and financial resources are budgeted to contribute back to open source projects. Here, the organisation asserts that it is a responsible open source citizen and acknowledges its responsibility to ensure the sustainability of the open source ecosystem.
* **Strategy** goal: This Goal is about making open source visible and acceptable at the highest levels of corporate management.  It is about recognising that open source is a strategic enabler of digital sovereignty, process innovation and, in general, a source of attractiveness and goodwill.


## Canonical Activities

The Canonical Activities are at the centre of the GGI blueprint. In its initial version, the GGI Methodology provides five Canonical Activities per goal, 25 in total. Canonical Activities are described using the following predefined sections:
* _Description_: a summary of the topic that the activity addresses and the steps to completion.
* _Opportunity Assessment_: describes why and when it is relevant to undertake this activity.
* _Progress Assessment_: describes how to measure progress on the activity and to assess its success.
* _Tools_: a list of technologies or tools that can help achieve this activity.
* _Recommendations_: hints and best practices collected from GGI participants.
* _Resources_: links and references to read more about the topic covered by the activity.

### Description
This section provides a high-level description of the Activity, a summary of the topic to set the purpose of the activity in the context of the open source approach within a goal.

### Opportunity Assessment
To help structure an iterative approach, each activity has an "Opportunity Assessment" section, with one or more questions attached to it. The opportunity assessment focuses on why it is relevant to undertake this activity, what needs it addresses. Assessing the opportunity will help define what are the efforts expected, resources needed, and help evaluate costs and expected ROI. 

### Progress Assessment
This step focuses on defining objectives, KPIs, and on providing _verification points_ that help evaluate progress in the Activity. The verification points are suggested, they can help define a roadmap for the Good Governance process, its priorities and how progress will be measured.

### Tools
Here are listed Tools that can help in delivering the activity or instrument a specific step of the activities. Tools are not a mandatory recommendation, nor pretend to be exhaustive, but are suggestions or categories to be elaborated upon based on existing context. 

### Recommendations
This section is regularly updated with feedbacks from users and all sorts of recommendations that can help manage the Activity.

### Resources
Resources are proposed to feed the approach with background studies, reference documents, events or online content to enrich and develop the related approach on the activity. Resources are not exhaustive, they are starting points or suggestions to expand on the semantics of the activity according to one's own context.


## Customised Activity Scorecards
Customised Activity Scorecards (CAS) are slightly more detailed than Canonical Activities. A CAS includes details specific to the organisation implementing GGI. Using the CAS is described in the Methodology section.

## Iteration
Iteration also belongs to the Methodology section.
