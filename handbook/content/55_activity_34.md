## C-Level awareness

Activity ID: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_34.md).

### Description 

The open source initiative of the organisation will yield its strategic benefits only if it is enforced at its highest levels by integrating the open source DNA into the company's strategy and internal working. Such commitment cannot happen if higher-level executives and top management are not themselves a part of it. The training and open source mindset must also be extended to those who shape the policies, decisions and overall strategy, both inside and outside the company.

This commitment ensures that practical improvements, mindset changes and new initiatives are met with consistent, benevolent and sustainable support from the hierarchy, bringing in more fervent participation from workers. It shapes how external actors see the organisation, bringing in reputational and ecosystem benefits. It is also a means to establish the initiative and its benefits in the mid and long term.


### Opportunity Assessment 

This activity becomes essential if/when:

* The organisation has set global goals relevant to open source management, but struggles to achieve them. It is unlikely that the initiative can achieve anything without good knowledge and a clear commitment from higher-level executives.
* The initiative has already started and is making progress, but the higher levels of the hierarchy do not follow it up properly.

Hopefully, it should become evident that anything but ad hoc usage of open source requires a consistent and well-thought approach, given the range of teams and cultural change it can bring.

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] There is a mandated governance office/officer empowered to set a uniform open source strategy across the company and ensure that the scope is clear.
- [ ] There is a clear, binding commitment from the hierarchy to the OSS strategy.
- [ ] There is transparent communication by the hierarchy about its commitment to the program. 
- [ ] The hierarchy is available to discuss open source software. It can be solicited and challenged on its promises.
- [ ] There is an appropriate budget and funding for the initiative.

### Recommendations 

Examples of actions associated with this activity include:
* Conduct training to demystify OSS to C-level management.
* Obtain explicit, practical endorsement for OSS usage and strategy.
* Explicitly mention and endorse the OSS program in internal communications.
* Explicitly mention and endorse the OSS program in public communications.

Open source is a _strategic enabler_ that embarks _enterprise culture_. What does this mean?
* Open source can be leveraged as a mechanism to disrupt suppliers and reduce software acquisition costs.
    * Should open source come under the purview of _Software Asset Managers_ or _purchasing departments_?
* Open source licences enshrine the freedoms that deliver the benefits of open source, but they also carry _obligations_. If not met appropriately, responsibilities can create legal, commercial and image risks to an organisation.
    * Will licence conditions grant visibility into areas of code that should remain confidential?
    * Will it impact my organisation's patent portfolio?
    * How should project teams be trained and supported on this subject?
* Contributing back to external open source projects is where the biggest value of open source lies.
    * How should my company encourage (and track) this?
    * How should developers use GitHub, GitLab, Slack, Discord, Telegram, or any of the other tools open source projects habitually use?
    * Can open source impact the company's HR policies? 
* Of course, it's not all about contributing back, what about my own open source projects?
    * Am I ready to do _open_ innovation?
    * How will my projects manage _incoming_ contributions?
    * Should I spend the effort to nurture a community for a given project?
    * How should I lead the community, what role should community members have?
    * Am I ready to cede roadmap decisions to a community?
    * Can open source be a valuable tool to reduce silo-isation between company teams?
    * Do I need to handle open source transfer from one company entity to another?
