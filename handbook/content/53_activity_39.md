## Upstream first

Activity ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_39.md).

### Description 

This activity is concerned with developing awareness with regard to the benefits of contributing back and enforcing the upstream first principle.

With the upstream first approach all development on an open source project is to be made with the level of quality and openness required to be submitted to a project's core developers and published by them. 


### Opportunity Assessment 

Writing code with upstream first in mind results in:
* better quality code,
* code that is ready to be submitted upstream,
* code that is merged in the core software,
* code that will be compatible with future version,
* recognition by the project community and better and more profitable cooperation.

> Upstream First is more than just "being kind". It means you have a say in the project. It means predictability. It means you are in control. It means you act rather than react. It means you understand open source. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))


### Progress Assessment 

The following **verification points** demonstrate progress in this Activity: Upstream first is implemented?
- [ ] Significant increase in the number of pull/merge requests submitted to third party projects.
- [ ] A list of third party projects for which upstream first must be applied has been drafted.


### Recommendations 

* Identify developers with most experience at interacting with upstream developers.
* Facilitate interaction between developers and core developers (events, hackathons, etc.)


### Resources 

* A clear explanation of the Upstream First principle and why it fits in the Culture Goal: https://maximilianmichels.com/2021/upstream-first/. 
> Upstream First means that whenever you solve a problem in your copy of the upstream code which others could benefit from, you contribute these changes back upstream, i.e. you send a patch or open a pull request to the upstream repository.
* [What is Upstream and Downstream in Software Development?](https://reflectoring.io/upstream-downstream/) A crystal clear explanation.
* A paper by Dave Neary: [Upstream first: Building products from open source software](https://inform.tmforum.org/features-and-analysis/2017/05/upstream-first-building-products-open-source-software/).
* Explained from the Chromium OS design documents: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
* Red Hat on upstream and the advantages of [upstream first](https://www.redhat.com/en/blog/what-open-source-upstream).
