## Manage key indicators

Activity ID: [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_24.md).

### Description 

This activity collects and monitors a set of indicators that inform day-to-day managerial decisions and strategic options concerning professionally managed open source software.

Key metrics related to open source software form the backdrop of how well governance programmes are rolled out. The activity covers selecting a few indicators, publishing them to the teams and management, and sending regular updates on the initiative, e.g. via a newsletter or corporate news.

This activity requires:
* stakeholders to discuss and define the objectives of the program,
* the implementation of a measurement and data collection tool connected to the development infrastructure, 
* the publication of at least one dashboard for the stakeholders and for all the people involved in the initiative. 

Indicators are based on data that must be collected from relevant sources. Fortunately, there are plenty of sources for open source software engineering. Examples include: 
* the development environment, the CI/CD production chain,
* the HR department, 
* the testing and software composition analysis tools,
* the repositories.

Examples of indicators include:

* Number of resolved dependencies, displayed by licence type.
* Number of outdated/vulnerable dependencies.
* Number of licencing/ip issues detected.
* Contributions made to external projects.
* Bug open time.
* Number of contributors on a component, number of commits, etc.

This activity is about defining these requirements and measurement needs, and implementing a dashboard that shows in a simple and efficient manner the main indicators of the program. 


### Opportunity Assessment 

Key indicators help understand and better manage the resources devoted to open source software, and measure the results in order to communicate effectively and reap the full benefits of the investment. By communicating broadly, more people can follow the initiative and will feel involved, ultimately making it an organisation-level concern and goal.

While each activity has evaluation criteria that help answer questions about progress achieved, there is still a need for monitoring done with numbers and quantitative indicators.

Whether in a small startup or a large global company, key metrics help keep teams focused and monitor performance.  Metrics are crucial because they support decision-making and are the basis for monitoring decisions already taken.

With simple and practical numbers and graphics, the whole organisation members will be able to follow and synchronise efforts regarding open source, making it a shared concern and action. This also allows the various actors to better enter the course, contribute to the project and get the overall benefits.


### Progress Assessment 

The following **verification points** demonstrate progress in this activity:

- [ ] A list of metrics and how to collect them has been established.
- [ ] Tools to collect, store, process and display indicators are used.
- [ ] There is a generalised dashboard available to all participants that shows the progress made on the initiative.


### Tools 

* [GrimoireLab](https://chaoss.github.io/grimoirelab) from Bitergia.
* [Alambic](https://alambic.io) from Castalia Solutions.
* Generic BI tools (elasticsearch, grafana, R/Python visualisations…) are a good fit too, when the proper connectors are setup according to the defined goals.

### Recommendations 

* Write down the objectives and roadmap of the open source Governance.
* Communicate in-house about the actions and status of the initiative.
* Involve people in the definition of KPIs, in order to make sure that 
  - they are well understood, 
  - they provide a complete view of the needs and 
  - they are considered and followed. 
* Build at least one dashboard that can be displayed for everybody (e.g. on a screen in the room), with essential indicators to show the progress and overall situation.


### Resources 

* The [CHAOSS community](https://chaoss.community/) has many good references and resources related to open source indicators.
* Check out metrics for [Project Attributes](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) from the OW2 Market Readiness Levels [methodology](https://www.ow2.org/view/MRL/Overview). 
* [A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) by Liz Laffan is an interesting reading about openness in open source projects.
* [Governance Indicators: A Users’ Guide](https://www.un.org/ruleoflaw/files/Governance%20Indicators_A%20Users%20Guide.pdf) is the UN's guide about governance indicators. Although it is applied to democracy, corruption and transparency of nations, the basics of measurement and indicators as applied to governance are well worth a read.
