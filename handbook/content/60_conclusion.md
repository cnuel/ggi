# Conclusion


## Roadmap

As a road to the Zen of open source good governance, we plan to work on the following features after the first release:
* **Define Roles** for the activities, so people can pick and start working on items according to their mission and capabilities. This will help to give the right tasks to the right people, and enable a new perspective on the program.
* **Improve and expand on the methodology** section thanks to the feedback gathered from the community. While working on the implementation of the Good Governance Initiative, open source officers will gain new insights and gather experience that will help us build a better model and methodology for the body of knowledge.
* **Improve Canonical Activities** thanks to the feedback gathered from the community, and **add new activities** to the current set. More areas of interest for open source good governance will soon emerge as people start using, and contributing to, the body of knowledge.
* **Consider a straight-forward process** to clone and **implement** the method in organisations, with an *install-like* feature. There should be an easy way for people willing to use the body of knowledge to set up their own private space, with everything needed to sort out activities and define sprints, fill up scorecards, and communicate transparently on progress. This could include developing an application to support the implementation of the methodology.
* **Develop** use cases, templates, testimonials aligned with typical scenarios such as SME, Big City, University, etc

We will set up a forum on the OSPO.zone to collect user feedback, ideas, support discussions, etc.


## Contributing

We are open to contributions, and all our activities are open and public. If you would like to participate, the best way is to register on the mailing list and join the conversation: <http://mail.ow2.org/wws/subscribe/ossgovernance>.

The Good Governance Initiative is a working group hosted at the OW2 forge:

* The [GGI resource centre](https://www.ow2.org/view/OSS_Governance/) contains a lot of resources and information from the beginning of the initiative.
* Activities are [elaborated and reviewed as issues](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/449) on the GitLab instance.
* Discussions happen on the [public GGI mailing list](https://mail.ow2.org/wws/info/ossgovernance), and we hold regular meetings. Minutes are available on the mailing list, and meetings are open to everyone.

To contribute to the GGI resource centre and GitLab activities, please follow these instructions:

* Create your OW2 user account at https://www.ow2.org/view/services/registration
* Login once at https://www.ow2.org/
* To edit the [Resource Centre](https://www.ow2.org/view/OSS_Governance/): send us your username, and we'll grant you appropriate access.
* To access the GitLab group: login once to https://gitlab.ow2.org with your OW2 credentials, then let us know when it's done, and we'll grant you access to the GGI group in GitLab.
* For access to Rocket.Chat you need to:
  - Login at least once on https://gitlab.ow2.org using your OW2 credentials.
  - Open Rocket.Chat and choose a Rocket.Chat username when prompted.
  - Go to the #general channel and ask for GGI channel access there.
  - Once access is granted, you should be able to access #good-governance channel.


## Contact

The preferred way to get in touch with the OW2 Good Governance Initiative is to post on the mailing list at <http://mail.ow2.org/wws/subscribe/ossgovernance>.

For administrative enquiries, you can reach the GGI initiative at https://www.ow2.org/view/About/Management_Office.


# Appendix: Customised Activity Scorecard template

The latest version of the Customised Activity Scorecard template is available in the `resources` section of the [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) at OW2.
