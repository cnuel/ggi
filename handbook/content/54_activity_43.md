## Open source procurement policy

Activity ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_43.md).

### Description 

This activity is about implementing a process to select, acquire, purchase open source software and services. It is also about considering the actual cost of open source software and provisioning for it. OSS may be "free" at first sight, but it is not without internal and external costs such as integration, training, maintenance and support. 

Such policy requires that both open source and proprietary solutions are symmetrically considered when evaluating value for money as the optimum combination of the total cost of ownership and quality. Therefore, the IT Procurement department should actively and fairly consider open source options, while at the same time ensuring proprietary solutions are considered on an equal footing in purchasing decisions.

Open source preference can be explicitly stated based on the intrinsic flexibility of the open source option when there is no significant overall cost difference between proprietary and open source solutions.

Procurement departments must understand that companies offering support for OSS typically lack the commercial resources to participate in procurement competitions, and adapt their open source procurement policies and processes accordingly.


### Opportunity Assessment 

Several reasons justify the efforts to set up specific open source procurement policies:
* Supply of commercial open source software and services is growing and cannot be ignored, and requires the implementation of dedicated procurement policies and processes.
* There is a growing supply of highly competitive commercial open source business solutions for corporate information systems.
* Even after adopting a free-of-charge OSS component and integrating it into an application, internal or external resources must be provided to maintain that source code. 
* Total Cost of Ownership (TCO) is often (although not necessarily) lower for FOSS solutions: no licence fees to pay when purchasing/upgrading, open market for service providers, option to provide some or all of the solution yourself.


### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] New call for proposals proactively request open source submissions.
- [ ] Procurement department has a way to evaluate open source vs proprietary solutions.
- [ ] A simplified procurement process for open source software and services has been implemented and documented.
- [ ] An approval process drawing from cross-functional expertise has been defined and documented.


### Recommendations 

* "Be sure to tap into the expertise of your IT, DevOps, cybersecurity, risk management, and procurement teams when creating the process." (from [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/)).
* Competition law may require that "open source" not be specifically mentioned. 
* Select technology upfront then go to RFP for customisation and support services.


### Resources 

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): not new, but still a great read by our colleagues at OSS-watch in the UK. Check out the [slides](http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): a recent piece on open source procurement with useful hints.
