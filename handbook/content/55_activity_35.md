## Open source and digital sovereignty

Activity ID: [GGI-A-35](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_35.md).

### Description 

Digital sovereignty can be defined as the

> “Ability and opportunity of individuals and institutions to execute their role(s) in the digital world independently, intentionally and safely.”
> &mdash; Competence Centre for Public IT, Germany

In order to properly conduct its business, any entity has to rely on some other partners, services, products and tools. Reviewing the ties and constraints of these dependencies enable the organisation to assess and control its dependence towards external factors, thus improving its autonomy and resilience. 

As an example, vendor lock-in is a strong factor of dependence that may impede the organisation's processes and added value and as such, it should be avoided. Open source is one of the ways out of this lock. Open source plays a significant role in digital sovereignty, allowing a greater choice between solutions, providers and integrators, and greater control over IT roadmaps.

It should be noted that digital sovereignty is not a trust issue: we obviously need to trust our partners and providers, but the relationship gets even better when it's based on mutual consent and recognition, rather than forced contracts and strains.

Here are some advantages of a better digital sovereignty:
* Improve the ability of the organisation to make its own choices without constraints.
* Improve the resilience of the company regarding external actors and factors.
* Improve negotiating position when dealing with partners and service providers.

### Opportunity Assessment 

* How difficult/expensive is it to move away from a solution?
* Could the solution providers impose unwanted conditions on their service (e.g. licence change, contracts updates)?
* Could the solution providers unilaterally increase their prices, simply because we do not have a choice?

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] There is an assessment of critical dependencies for the organisation's providers and partners.
- [ ] There is a backup plan for these identified dependencies.
- [ ] There is a stated requirement for digital sovereignty when new solutions are investigated.

### Recommendations 

* Identify key dependency risks from service providers and 3rd party entities.
* Maintain a list of open-source alternatives to critical services.
* Add a requirement when selecting new tools and services used within the entity, stating the need for digital sovereignty.


### Resources 

* [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) and [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), from the Open-Sourcerers website.
* An excellent superuser.openstack.org article on [The Role of Open Source in Digital Sovereignty](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). Here is a short extract:
  > Digital Sovereignty is a key concern for the 21st century, especially for Europe. Open source has a major role to play in enabling digital sovereignty, by allowing everyone to access the necessary technology, but also by providing the governance transparency and interoperability necessary for those solutions to succeed.
* The European Union's take on digital sovereignty, from the [Open Source Observatory (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Open Source, digital sovereignty and interoperability: The Berlin Declaration.
* The UNICEF's position on [Open Source for Digital Sovereignty](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
