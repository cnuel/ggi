## Manage software dependencies

Activity ID: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_23.md).

### Description 

A *dependency identification* program looks for the dependencies actually used within the codebase. As a result, the organisation must establish and maintain a list of known dependencies for its code base and watch the evolution of the identified providers. 

Establishing and maintaining a list of known dependencies is an enabler for, and a prerequisite to:
* IP and licence checking: some licences cannot be mixed, even as a dependency. One has to know its dependencies to assess its associated legal risks.
* Vulnerabilities management: the entire piece of software is as weak as its smallest part: see the example of the [Heartbleed flaw](https://en.wikipedia.org/wiki/Heartbleed). One has to know its dependencies to assess its associated security risks.
* Lifecycle and sustainability: an active community on the dependency project is a bright sign for bug corrections, optimisations, and new features.
* Thoughtful selection of used dependencies, according to "maturity" criteria - the goal being to use open source components that are safe, with a sane and well-maintained codebase, and a living, active and reactive community that will accept external contributions, etc.

### Opportunity Assessment 

Identifying and tracking dependencies is a required step to mitigate the risks associated with any code reuse. In addition, implementing tools and processes to manage software dependencies is a prerequisite to properly manage quality, compliance, and security.

Consider the following questions:  
* What is the company's risk (cost, reputation, etc.) if the software is corrupted, attacked or sued?
* Is the code base considered critical for people, the organisation, or business?
* What if a component upon which an application depends changes its repository?

The minimal and first step is to implement a software composition analysis (SCA) tool. Support by specialised consulting firms may be required for a full-fledged SCA or dependency mapping.

### Progress Assessment 

The following **verification points** demonstrate progress in this activity:
- [ ] Dependencies are identified in all in-house developed code.
- [ ] Dependencies are identified in all external code executed within the company.
- [ ] An easy-to-setup software composition analysis or dependency identification procedure is available for projects to add to their Continuous Integration process.
- [ ] Dependency analysis tools are used.

### Tools 

* [OWASP Dependency check](https://github.com/jeremylong/DependencyCheck): dependency-Check is a Software Composition Analysis (SCA) tool that attempts to detect publicly disclosed vulnerabilities contained within a project’s dependencies.
* [OSS Review Toolkit](https://oss-review-toolkit.org/): a suite of tools to assist with reviewing Open Source Software dependencies. 
* [Fossa](https://github.com/fossas/fossa-cli): fast, portable and reliable dependency analysis. Supports licence & vulnerability scanning. Language-agnostic; integrates with 20+ build systems.
* [Software 360](https://projects.eclipse.org/projects/technology.sw360).
* [Eclipse Dash license tool](https://github.com/eclipse/dash-licenses): takes a list of dependencies and requests [ClearlyDefined](https://clearlydefined.io) to check their licences.


### Recommendations 

* Conduct regular audits about the dependencies and IP requirements to mitigate legal risks.
* Ideally, integrate dependencies management in the Continuous integration process so that issues (new dependency, licence incompatibility) are identified and fixed as soon as possible.
* Keep track of dependency-related vulnerabilities, keep users and developers informed.
* Inform people about the risks associated with wrong licencing.
* Propose an easy solution for projects to set up licence checking on their codebase.
* Communicate on its importance and help projects to add it to their CI systems.
* Set up a visible KPI for dependency-related risks.

### Resources 

* Existing [OSS-licenced OSS licence compliance tools](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/) group page.
* [The FOSSology Project](https://www.linuxfoundation.org/wp-content/uploads/lfcorp/files/lf_foss_compliance_fossology.pdf). An up-to-date introduction to FOSSology and FOSS compliance by the Linux Foundation.
* [Free and Open Source Software licence Compliance: Tools for Software Composition Analysis](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), by Philippe Ombredanne, nexB Inc.
* [Software Sustainability Maturity Model](http://oss-watch.ac.uk/resources/ssmm).
* [CHAOS](https://chaoss.community/): Community Health Analytics Open Source Software.
