## Support open source communities

Activity ID: [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_30.md).

### Description 

This activity is about engaging with institutional representatives of the open source world. 

It is achieved through:
* Joining OSS foundations (including the financial cost of membership).
* Supporting, advocating foundations activities.

This activity involves allocating the development and IT teams some time and budget to participate in open source communities.

### Opportunity Assessment 

Open source communities are at the forefront of the evolution of the open source ecosystem. Engaging with open source communities has several advantages: 
* it helps keep informed and up to date,
* it enhances the profile of the organisation,
* membership comes with benefits,
* it provides additional structure and motivation to the open source IT team.

Costs include:
* membership fees,
* personnel time and some travel budget allocated to participate in community activities,
* monitoring of IP commitment.

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] The organisation is a signed member of an open source foundation.
- [ ] The organisation participates in the governance.
- [ ] Software developed by the organisation is submitted to / has been added to the code base of a foundation.
- [ ] Membership is acknowledged on the websites of both the organisation and the community.
- [ ] Performed cost/benefit assessment of the membership.
- [ ] A contact point for the community has been appointed.


### Recommendations 

* Join a community compatible with your size and resources, i.e. a community that can hear your voice and where you can be a recognized contributor.


### Resources 

* Check out this [useful page](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) from the Linux Foundation on the why and how to join an open source community.
