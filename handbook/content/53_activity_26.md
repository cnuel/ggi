## Contribute to open source projects

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_26.md).

### Description 

Contributing to open source projects that are freely used is one of the key principles of good governance. The point is to avoid being a simple passive consumer and give back to the projects. When people add a feature or fix a bug for their own purpose, they should make it generic enough to contribute to the project. Developers must be allowed time for contributions. 

This activity covers the following scope:
* Working with upstream open source projects.
* Reporting bugs and feature requests.
* Contributing code and bug fixes.
* Participating in community mailing lists.
* Sharing experience.


### Opportunity Assessment 

The main benefits of this activity are:

* It increases the general knowledge and commitment to open source within the company, as people start contributing and get involved in open source projects. They get a feeling of public utility and improve their personal reputation.
* The company increases its visibility and reputation as contributions make their way through the contributed project. This shows that the company is actually involved in open source, contributes back, and promotes fairness and transparency. 

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] There is a clear and official path for people willing to contribute.
- [ ] Developers are encouraged to contribute back to open source projects they use.
- [ ] A process is in place to ensure legal compliance and security of contributions by developers.


* KPI: Volume of external contributions (code, mailing lists, issues..) by individual, team, or entity.


### Tools 

It may be useful to follow contributions, both to keep track of what is contributed and to be able to communicate on the company's effort. Dashboards and activity tracking software can be used for this purpose. Check:
* Bitergia's [GrimoireLab](https://chaoss.github.io/grimoirelab/)
* [Alambic](https://alambic.io)
* [ScanCode](https://scancode-toolkit.readthedocs.io)

### Recommendations 

Encourage people within the entity to contribute to external projects, by:
* Allowing them time to write generic, well-tested bug fixes and features, and to contribute them back to the community.
* Providing training to people about contributing back to open source communities. This is both about technical skills (improving your team's knowledge) and community (belonging to the open source communities, code of conduct, etc.).
* Provide training on legal, IP, technical issues, and set up a contact within the company to help with these topics if people have doubts. 
* Provide incentives for published work.
* Note that contributions from the company/entity will reflect its code quality and involvement, so make sure your development team provides code that is good enough. 

### Resources 

* The [CHAOSS](https://chaoss.community/) initiative from the Linux Foundation has some tools and pointers about how to track contributions in development.
