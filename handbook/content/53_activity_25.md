## Promote open source development best practices

Activity ID: [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_25.md).

### Description 

This activity is about defining, actively promoting and implementing open source best practices within the development teams.

As a starting point the following topics might be considered for attention:
* User and developer documentation.
* Proper organisation of the project on a publicly accessible repository.
* Promote and implement controlled reuse.
* Providing a complete and up-to-date product documentation.
* Configuration Management: git workflows, collaborative patterns.
* Release management: release early & release often, stable vs development versions, etc.

OSS projects have a special, [bazaar-like](http://www.catb.org/~esr/writings/cathedral-bazaar/) modus operandi. In order to allow and foster this collaboration and mindset, some practices are recommended that facilitate collaborative and decentralised development and contributions from third-party developers…

**Community documents**

Make sure that all projects within the company propose the following documents:
* README -- quick description of the project, how to interact, links to resources.
* Contributing -- introduction for people willing to contribute.
* Code Of Conduct -- What is acceptable -- or not -- as behaviour within the community.
* LICENSE -- the default licence of the repository.

**REUSE best practices**

[REUSE](https://reuse.software) is an initiative from the [Free Software Foundation Europe](https://fsfe.org/) to improve reuse of software and streamline OSS and licence compliance.

### Opportunity Assessment 

Although it heavily depends on the OSS common-knowledge among the team, training people and creating processes that enforce these practices is always beneficial. It is even more important when:
* potential users and contributors are not known,
* developers are not used to open source development.

### Progress Assessment 

The following **verification points** demonstrate progress in this Activity:
- [ ] Project sets a list of open source best practices to comply with.
- [ ] Project monitors its alignment with best practices.
- [ ] Development team has built awareness about complying with OSS best practices.
- [ ] New best practices are regularly evaluated, and an effort is made to implement them.

### Tools

* The [REUSE helper tool](https://github.com/fsfe/reuse-tool) assists with making a repository conformant with the [REUSE](https://reuse.software) best practices. It can be included in many development processes to confirm the current status.
* [ScanCode](https://scancode-toolkit.readthedocs.io) has the ability to list all community and legal documents in the repository: see [feature description](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
* GitHub has a nice feature to [check for missing community documents](https://docs.github.com/articles/viewing-your-community-profile). It can be found in the Repository page > "Insights" > "Community". [Here](https://github.com/borisbaldassari/alambic/community) is an example.

### Recommendations 

* The list of best practices depends on the context and domain of the program and should be re-evaluated regularly in a continuous improvement manner. Practices should be monitored and regularly assessed to track down progress.
* Train people about OSS reuse (as consumers) and ecosystems (as contributors).
* Implement REUSE.software as in activity #14.
* Set up a process to manage legal risks associated with reuse and contributions.
* Explicitly encourage people to contribute to external projects.
* Provide a template or official guidelines for project structure.
* Set up automated checks to make sure that all projects comply with the guidelines.

### Resources 

* [OW2's list of open source best practices](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) from the Market Readiness Levels assessment methodology.
* [REUSE's official website](https://reuse.software) with specification, tutorial, and FAQ.
* [GitHub's community guidelines](https://opensource.guide/).
* An example of [configuration management best practices using GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).
