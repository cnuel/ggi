## Open source competency growth

Activity ID: [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Description 

This activity is about planning and initiating technical abilities and early experience with OSS once an inventory has been conducted (#17). It is also the opportunity to start establishing a basic, lightweight skills development roadmap.
- Identify what the required skills and training are.
- Set up a pilot project to kick start the approach, learn from doing, establish a first achievement milestone.
- Capitalise on lessons learned and build a body of knowledge.
- Start identifying and documenting the next steps for broader adoption. 
- Elaborate a strategy over the next few months or a year to engage management and financial support.

The scope of the activity:
* Linux, Apache, Debian, administration skills.
* Open source databases MariaDB, MySQL, PostgreSQL, etc.
* Open source virtualisation and cloud technologies.
* LAMP stack and its alternatives.

### Opportunity Assessment 

Like any IT technology, and probably even more, open source brings innovation. Open source grows fast and changes quickly. It requires organisations to keep up to date.

This activity helps identify areas where training could help people become more efficient and feel more secure using open source. It helps make employee development decisions. Seeding basic open source skills allows evaluating the opportunity to:
- Extend IT solutions with existing market technologies developed by the ecosystem.
- Develop new ways of collaboration inside and outside the organisation.
- Acquire competencies in new and innovative technologies.

### Progress Assessment 

The following **verification points** demonstrate progress in this activity: 
- [ ] A skills matrix is developed.
- [ ] The scope of OSS technologies used is proactively defined, i.e. avoiding uncontrolled use of OSS technologies.
- [ ] A satisfactory level of expertise is acquired for these technologies.
- [ ] Teams have received an "open source Basics" training to get started.

### Tools 

A key tool here is called Activity (or Competency) Matrix (or Mapping).

This activity can be conducted by:
* using online tutorials (many free of charge on the Internet), 
* participating in developers conferences, 
* receive vendor training, etc.


### Recommendations 

* Using and developing open source components in a safe and efficient manner requires an open, collaborative mindset that needs to be recognised and propagated both from the top (management) and the bottom (developers).
* Make sure that the approach is actively supported and promoted by the management. Nothing will happen if there is no commitment from the hierarchy.
* Involve people (developers, stakeholders) in the process: organise round tables and listen to ideas.
* Allow time and resources for people to discover, try and play with these new concepts. If possible, make it fun -- gamification and rewards are good incentives.

A pilot project with the following steps could serve as a catalyst:

- Identify the technology or framework to start with.
- Find online training, tutorial, and sample code to experiment.
- Build a prototype of the end solution.
- Identify some experts to challenge and coach on implementation.


### Resources 

* [What is a Competency Matrix](https://blog.kenjo.io/what-is-a-competency-matrix): a quick introductory read.
* [How to Make a Skills Matrix for your Team](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): a template with commentaries.
* [MOOC on Free (libre) culture](https://librecours.net/parcours/upload-lc000/) (French Only): this is a 6 part course on the free culture, introduction to Copyrights, Intellectual Property, open source licensing
